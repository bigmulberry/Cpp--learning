#include "Student.h"
Student::Student(char* t_name, int t_age, int t_num, char t_sex):sex(t_sex),num(t_num),age(t_age)//参数初始化列表
{
	int n_len = strlen(t_name);
	p_name = new char[n_len + 1];
	memset(p_name, 0, n_len + 1);
	strcpy_s(name, t_name);
}

Student::~Student()
{
	
	//在构造函数中new的空间，须在析构函数中进行delete
	if (p_name)
	{
		delete[] p_name;
	}

	cout << "Student::~Student()" << endl;
}
