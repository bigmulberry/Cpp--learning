#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include "Student.h"
#include <string>

//int main()
//{
//	int sum=0, value=0;
//	while (cin >> value)
//	{
//		sum += value;
//	}
//	cout << "sum=" << sum << endl;
//	return 0;
//}

//小作业：
//实现一个函数，该函数的声明如下：
//bool string_upper_diy(char str[], int str_len, bool b_odd_pos = true);
//功能为对字符串指定位置的字符变换为大写。
//str 参数为字符串的指针；
//str_len 为字符串的长度；
//b_odd_pos 为true的时候，就要将 str 字符串中的奇数位置的字符变为大写，同时将非奇数位置的字符变为小写；
//b_odd_pos 为false的时候，就要将 str 字符串中的奇数位置的字符变为小写，同时将非奇数位置的字符变为大写；
//奇数指的是：1, 3, 5, 7, 9, 11, 13, 15, 17, 19...以此类推；


//bool string_upper_diy(char str[], int str_len, bool b_odd_pos = true)
//{
//	if (str_len <= 0)
//		return false;
//	for (int i = 0; i < str_len; i++)
//	{
//		char& ch = str[i];
//		if (b_odd_pos)//奇数位大写
//		{
//			if (i % 2 == 1)
//			{
//				if (islower(ch))
//					ch -= ('a' - 'A');
//			}
//			else
//			{
//				if (isupper(ch))
//					ch += ('a' - 'A');
//			}
//		}
//		else//偶数位大写
//		{
//			if (i % 2 == 0)
//			{
//				if (islower(ch))
//					ch -= ('a' - 'A');
//			}
//			else
//			{
//				if (isupper(ch))
//					ch += ('a' - 'A');
//			}
//		}
//
//	}
//	return true;
//}
//
//
//int main()
//{
//	char str[50];
//	strcpy_s(str, "abcdefgh");
//	string_upper_diy(str, strlen(str));
//	cout << str << endl;
//	string_upper_diy(str, strlen(str), 0);
//	cout << str << endl;
//	return 0;
//}


//类的构造函数与析构函数


//析构函数也是一个在类中跟构造函数类似的特殊功能的成员函数。
//只不过它的作用是与构造函数相反，是在对象的生命周期结束的时候会被自动调用的。
//在C++中析构函数的名字跟类名相同，并且前面带上一个取反的符号~，表达的意思也就是跟构造函数的过程相反。
//
//默认情况下，如果类的设计者没有自己定义析构函数，那么编译器会自动为该类生成一个默认的析构函数，
//只不过函数体是空的，也就是什么都没做。所以，如果需要在对象被删除的时候做一些操作的话，那么就得自己定义析构函数


//以下几种情况会自动调用析构函数：
//①、如果在一个函数中定义了一个局部变量的对象，那么当这个函数执行结束时也就是该变量对象生命周期结束的时候，
//    所以析构函数会被自动调用；
//②、全局变量或者static类型的变量，他们的生命周期一般是在程序退出的时候，这时候该对象的析构函数才会被调用；
//③、如果是用new操作符动态的创建了一个对象，只有当用delete进行释放该对象的时候，析构函数才会被调用；

//析构函数的作用：
//先拿构造函数来说话，构造函数是新建对象吗？回答：不是，而是在对象被创建出来之后自动被调用的，
//用来初始化相关信息的函数。同理，析构函数也不是用来删除对象的，而是当对象被删除的时候自动会被调用的，
//用来做一些对象被删除之前的清理工作。只要对象的生命周期结束，那么程序就会自动执行析构函数来完成这个工作的。

//析构函数的特点：
//析构函数不返回任何值，没有函数类型，也没有任何函数的参数。
//由于上面这些特点，所以析构函数不能被重载，所以说一个类可以有多个构造函数，但只能有一个析构函数。

int main()
{
	char buffer[] = "zhangsan";
	//Student s1(buffer, 'm', 325, 15);

	//new出来的对象 delete才会调用析构
	Student* ps2 = new Student(buffer, 16, 326, 'm');
	delete ps2;//在delete对象时，如果对象中的某个变量是new出来的也需要先delete，不然会造成内存泄漏！！
	//new1（对象）——》new2（变量1）——》delete2（变量1）——》delete1（对象）
	return 0;
}


