#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//关于构造临时对象的优化
//********************************************************************

class A
{
public:

	//构造
	A(int a = 0) :_a(a)
	{
		cout << "A()" << endl;
	}

	A(const A& a) :_a(a._a)
	{
		cout << "A(const A& a)" << endl;
	}

	~A()
	{
		cout << "~A()" << endl;
	}
private:
	int _a;
};

void f1(A a)
{

}

A f2()
{
	static A aa;
	return aa;
}

int main()
{
	A a1;
	f1(a1);
	//对象的传参，将会在传参时调用拷贝构造函数，但是临时对象传参的过程可能被编译器优化，见下方例子↓
	
	//例1
	//匿名对象传参
	//编译器优化前：
	//1.A()的匿名对象调用构造函数，然后调用拷贝构造函数传参
	f1(A());
	//事实上，省略了拷贝构造函数这一步，
	//直接在函数的传参中调用了构造函数（直接构造在了调用的函数内部）

	//例2 
	//隐式类型转换
	//按照道理来说，2将会先构造成(匿名)临时对象，然后再拷贝构造给a2
	A a2 = 2;//隐式类型的转换，A tmp(2)，再用tmp拷贝a2
	//但是编译器优化了，直接构造

	//例3
	//返回值来拷贝构造
	//按照道理来说，f2()的返回值将通过拷贝构造临时对象返回，然后再拷贝构造给a4
	A a4 = f2();
	//但是编译器优化了，缺省了传值返回的临时对象的拷贝构造，而是直接拷贝构造了a4，相当于两步并作了一步

	//下面代码是没有优化的，因为这里没有拷贝构造a5，而是赋值运算符重载，不能两步并作一步
	A a5;
	a5 = f2();
	//所以尽量使用临时变量接受返回值直接构造对象


	//（拷贝）构造临时对象->（拷贝）构造     ==》优化==》         直接（拷贝）构造
	return 0;
}