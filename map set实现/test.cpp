#define _CRT_SECURE_NO_WARNINGS 1
#include "map.h"
#include "set.h"

int main()
{
	mystd::map<int, int> m;
	pair<int, int> mp[] = { pair<int,int>(1,100)
						,pair<int,int>(5,20)
						,pair<int,int>(4,18)
						,pair<int,int>(7,24)
						,pair<int,int>(3,78) };
	for(auto e:mp)
		m.insert(e);

	mystd::map<int, int>::reverse_iterator mit = m.rbegin();

	while (mit != m.rend())
	{
		cout << mit->first << ':'<<mit->second << endl;
		++mit;
	}

	mit = m.find(1);
	cout << mit->second << endl;

	//for (auto e : mp)
	//{
	//	m.erase(e.first);
	//	cout << m.Isbalance() << endl;
	//}

	mystd::set<int> s;
	int arr[] = { 1,5,4,7,3};
	for(auto e:arr)
		s.insert(e);
	

	mystd::set<int>::reverse_iterator sit = s.rbegin();

	while (sit != s.rend())
	{
		cout << *sit << endl;
		++sit;
	}

	cout << "-------" << endl;
	for (auto e : arr)
	{
		s.erase(e);
	}
	return 0;
}