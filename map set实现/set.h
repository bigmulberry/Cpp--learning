#pragma once
#pragma once
#include "RBTree.h"

namespace mystd
{
	template<class K>
	class set
	{
		
	public:
		struct SetKOfValue
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};

	public:
		typedef typename RBTree<K, K, SetKOfValue>::iterator iterator;//typename 确保其为类型而非变量
		typedef typename RBTree<K, K, SetKOfValue>::const_iterator const_iterator;
		typedef typename RBTree<K, K, SetKOfValue>::reverse_iterator reverse_iterator;
		pair<iterator,bool> insert(const K& data)
		{
			return  _t.Insert(data);
		}

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}
		reverse_iterator rbegin()
		{
			return _t.rbegin();
		}

		reverse_iterator rend()
		{
			return _t.rend();
		}

		bool erase(const K& key)
		{
			return _t.Erase(key);
		}

		iterator find(const K& key)
		{
			return _t.Find(key);
		}

	private:
		RBTree<K,K,SetKOfValue> _t;
	};
}