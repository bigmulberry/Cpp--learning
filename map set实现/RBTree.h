#pragma once
#pragma once
#include <iostream>
using namespace std;

enum Colour
{
	RED,
	BLACK
};

template<class Value>
struct RBTreeNode
{
	RBTreeNode<Value>* _left;
	RBTreeNode<Value>* _right;
	RBTreeNode<Value>* _parent;
	Value _data;
	Colour _colour;

	RBTreeNode(const Value& data) :
		_left(nullptr),
		_right(nullptr),
		_parent(nullptr),
		_data(data),
		_colour(RED)
	{}

};

template<class Value,class Ref,class Ptr>
struct RBTreeIterator
{
	//将提供给反向迭代器 作为类型
	typedef Ref Reference;
	typedef Ptr Pointer;

	typedef RBTreeNode<Value> Node;
	typedef RBTreeIterator<Value, Ref, Ptr> Self;
	Node* _node;

	RBTreeIterator(Node* node) :_node(node)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s)const
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)const
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		if (_node->_right)
		{
			Node* cur = _node->_right;
			while (cur->_left)
			{
				cur = cur->_left;
			}
			_node = cur;
		}
		else
		{
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent && cur==parent->_right)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}

	Self& operator--()
	{
		if (_node ->_left)
		{
			Node* cur = _node->_left;
			while (cur->_right)
			{
				cur = cur->_right;
			}
			_node = cur;
		}
		else
		{
			Node* cur = _node;
			Node* parent = _node->_parent;
			while (parent && cur == parent->_left)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	
};


// 反向迭代器——使用正向迭代器封装
template <class Iterator>
struct RBTreeReverseIterator
{
public:
	typedef typename Iterator::Reference Ref;
	typedef typename Iterator::Pointer Ptr;
	typedef RBTreeReverseIterator<Iterator> Self;

	Iterator _it;

	RBTreeReverseIterator(Iterator it) :_it(it._node)
	{}

	Ref operator*()
	{
		return *_it;
	}

	Ptr operator->()
	{
		return _it.operator->();
	}

	bool operator!=(const Self& s)const
	{
		return _it != s._it;
	}

	bool operator==(const Self& s)const
	{
		return _it == s._it;
	}

	Self& operator++()
	{
		--_it;
		return *this;
	}
	Self& operator--()
	{
		++_it;
		return *this;
	}
};



template<class K, class Value,class KeyOfValue>
class RBTree
{
public:
	typedef RBTreeNode<Value> Node;
	typedef RBTreeIterator<Value, Value&, Value*> iterator;
	typedef RBTreeIterator<Value, const Value&,const Value*> const_iterator;
	typedef RBTreeReverseIterator<iterator> reverse_iterator;

public:
	RBTree() :_root(nullptr)
	{}

	iterator begin()
	{
		Node* cur = _root;
		while (cur && cur->_left)
		{
			cur = cur->_left;
		}
		return iterator(cur);
	}

	const_iterator begin()const
	{
		Node* cur = _root;
		while (cur && cur->_left)
		{
			cur = cur->_left;
		}
		return const_iterator(cur);
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	const_iterator end()const
	{
		return const_iterator(nullptr);
	}

	reverse_iterator rbegin()
	{
		Node* cur = _root;
		while (cur && cur->_right)
		{
			cur = cur->_right;
		}
		return reverse_iterator(cur);
	}

	reverse_iterator rend()
	{
		return reverse_iterator(nullptr);
	}

	~RBTree()
	{
		Destroy(_root);
		_root = nullptr;
	}



	iterator Find(const K& key)
	{
		Node* cur = _root;
		KeyOfValue kot;
		while (cur)
		{
			if (kot(cur->_data) > key)
			{
				cur = cur->_left;
			}
			else if (kot(cur->_data) < key)
			{
				cur = cur->_right;
			}
			else
			{
				break;
			}
		}
		return iterator(cur);
	}


	pair<iterator, bool> Insert(const Value& data)
	{
		KeyOfValue kot;//仿函数类实例化
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_colour = BLACK;
			return make_pair(iterator(_root), true);
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			parent = cur;
			if (kot(cur->_data) > kot(data))//提取出Value类型中的key值，再进行比较
			{
				cur = cur->_left;
			}
			else if (kot(cur->_data) < kot(data))
			{
				cur = cur->_right;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}

		Node* newnode = new Node(data);
		newnode->_parent = parent;
		if (kot(parent->_data) >kot(data))
		{
			parent->_left = newnode;
		}
		else
		{
			parent->_right = newnode;
		}
		cur = newnode;

		//开始调整
		while (parent && parent->_colour == RED)//最差情况，处理到根节点为止
		{
			Node* grandfather = parent->_parent;
			Node* uncle = nullptr;
			if (grandfather->_right == parent)     //parent为grandfather右树
			{
				uncle = grandfather->_left;
				if (uncle && uncle->_colour == RED)//情况1，仅需变色，且考虑往上追溯
				{
					parent->_colour = BLACK;
					uncle->_colour = BLACK;
					grandfather->_colour = RED;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else                      //情况2+3 uncle为黑或者不存在，需要旋转+变色
				{
					//这里不会对uncle进行旋转或是变色处理，我们只关心是折线还是直线情况。
					if (cur == parent->_left)//折线情况
					{
						//对parent做右单旋
						RotateR(parent);
						swap(cur, parent);
					}
					//统一处理为直线情况
					RotateL(grandfather);
					parent->_colour = BLACK;//parent（当前子树的根节点）变为黑色
					grandfather->_colour = RED;//grandfather变为红色，以保证路径的黑色结点数量不变

					//无需再往上调整了，下次的while会因为parent为黑直接结束循环，不用在此break
				}
			}
			else                               //parent为grandfather左树
			{
				uncle = grandfather->_right;
				if (uncle && uncle->_colour == RED)//情况1，仅需变色，且考虑往上追溯
				{
					parent->_colour = BLACK;
					uncle->_colour = BLACK;
					grandfather->_colour = RED;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;

				}
				else                            //情况2+3 uncle为黑或者不存在，需要旋转+变色
				{
					//这里不会对uncle进行旋转或是变色处理，我们只关心是折线还是直线情况。
					if (cur == parent->_right)
					{
						//对parent做左单旋
						RotateL(parent);
						swap(cur, parent);
					}
					//统一处理为直线情况
					RotateR(grandfather);
					parent->_colour = BLACK;
					grandfather->_colour = RED;

					//无需再往上调整了，下次的while会因为parent为黑直接结束循环，不用在此break
				}
			}
		}//while
		_root->_colour = BLACK;//如果grandfather为根节点，那么让其变黑色

		return make_pair(iterator(newnode), true);
	}

	//删除结点
	bool Erase(const K& key)
	{
		KeyOfValue kot;
		Node* parent = nullptr;
		Node* cur = _root;

		//实际待删除结点
		Node* delPos = nullptr;
		Node* delParentPos = nullptr;

		//寻找待删除结点
		while (cur)
		{
			if (kot(cur->_data) > key)//往左找
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (kot(cur->_data) < key)//往右找
			{
				parent = cur;
				cur = cur->_right;
			}
			else//找到删除结点
			{
				if (cur->_left == nullptr)//左为空，右未必为空
				{
					if (cur == _root)
					{
						_root = _root->_right;
						if (_root)
						{
							_root->_parent = nullptr;
							_root->_colour = BLACK;//根节点注意时刻保持黑色
						}
						delete cur;
						return true;
					}
					else
					{
						delPos = cur;
						delParentPos = parent;
					}
				}
				else if (cur->_right == nullptr)//左必定不为空
				{
					if (cur == _root)
					{
						_root = _root->_left;
						_root->_parent = nullptr;
						_root->_colour = BLACK;

						delete cur;
						return true;
					}
					else
					{
						delParentPos = parent;
						delPos = cur;
					}
				}
				else    //cur左右均不为空 需找替换结点
				{
					Node* minR = cur->_right;
					Node* minRParent = cur;
					while (minR->_left)
					{
						minRParent = minR;
						minR = minR->_left;
					}

					//替换
					//注意：如果是map类，由于_data类型是pair<const K,Value>,所以是无法替换的，
					//将_data类型改为pair<K,Value>则可以替换
					cur->_data=minR->_data;

					delPos = minR;
					delParentPos = minRParent;
				}
				break;//进行红黑树调整和实际删除
			}
		}
		if (delPos == nullptr)//没有找到删除结点
		{
			return false;
		}

		//由于红黑树会向上调整，所以需要备份实际删除的结点
		cur = delPos;
		parent = delParentPos;

		//红黑树调整 
		//当cur为红结点直接删即可，这里只需考虑cur为黑的情况
		if (cur->_colour == BLACK)
		{
			//cur的度为1 孩子结点必为红色
			if (cur->_left)
			{
				cur->_left->_colour = BLACK;
			}
			else if (cur->_right)
			{
				cur->_right->_colour = BLACK;
			}
			else//cur为叶子结点
			{
				while (cur != _root)
				{
					if (cur == parent->_left)//待删除结点是父结点的左孩子
					{
						Node* brother = parent->_right;
						//情况3.a 黑兄弟远红侄
						if (brother->_colour == BLACK && brother->_right && brother->_right->_colour == RED)
						{
							RotateL(parent);//左旋父
							brother->_colour = parent->_colour;//祖染父色
							parent->_colour = BLACK;//父叔黑
							brother->_right->_colour = BLACK;
							//此情况完毕后，结束修复
							break;
						}
						//情况3.b 黑兄弟近红侄
						else if (brother->_colour == BLACK && brother->_left && brother->_left->_colour == RED)
						{
							brother->_colour = RED;//兄弟和侄子的颜色交换
							brother->_left->_colour = BLACK;
							RotateR(brother);//右旋兄
						}
						//情况3.c 黑兄弟双黑侄
						else if (brother->_colour == BLACK
							&& (brother->_left == nullptr || brother->_left->_colour == BLACK)
							&& (brother->_right == nullptr || brother->_right->_colour == BLACK))
						{
							brother->_colour = RED;
							if (parent == _root || parent->_colour == RED)
							{
								parent->_colour = BLACK;
								break;
							}
							//向上调整
							cur = parent;
							parent = cur->_parent;
						}
						//红兄弟
						else if (brother->_colour == RED)
						{
							parent->_colour = RED;
							brother->_colour = BLACK;
							RotateL(parent);//左旋父
						}
					}
					else                    //待删除结点是父结点的右孩子
					{
						Node* brother = parent->_left;
						//情况3.a 黑兄弟远红侄
						if (brother->_colour == BLACK && brother->_left && brother->_left->_colour == RED)
						{
							RotateR(parent);//右旋父
							brother->_colour = parent->_colour;//祖染父色
							parent->_colour = BLACK;//父叔黑
							brother->_left->_colour = BLACK;
							//此情况完毕后，结束修复
							break;
						}
						//情况3.b 黑兄弟近红侄
						else if (brother->_colour == BLACK && brother->_right && brother->_right->_colour == RED)
						{
							brother->_colour = RED;//兄弟和侄子的颜色交换
							brother->_right->_colour = BLACK;
							RotateL(brother);//左旋兄
						}
						//情况3.c 黑兄弟双黑侄
						else if (brother->_colour == BLACK
							&& (brother->_left == nullptr || brother->_left->_colour == BLACK)
							&& (brother->_right == nullptr || brother->_right->_colour == BLACK))
						{
							brother->_colour = RED;
							if (parent == _root || parent->_colour == RED)
							{
								parent->_colour = BLACK;
								break;
							}
							//向上调整
							cur = parent;
							parent = cur->_parent;
						}
						//红兄弟
						else if (brother->_colour == RED)
						{
							parent->_colour = RED;
							brother->_colour = BLACK;
							RotateR(parent);//右旋父
						}
					}
				}
			}
		}

		//实际删除
		if (delPos->_left == nullptr)//左为空和全空的情况
		{
			if (delPos == delParentPos->_left)
			{
				delParentPos->_left = delPos->_right;
				if (delPos->_right)
				{
					delPos->_right->_parent = delParentPos;
				}
			}
			else
			{
				delParentPos->_right = delPos->_right;
				if (delPos->_right)
				{
					delPos->_right->_parent = delParentPos;
				}
			}
		}
		else//左不为空右为空
		{
			if (delPos == delParentPos->_left)
			{
				delParentPos->_left = delPos->_left;
				if (delPos->_left)
				{
					delPos->_left->_parent = delParentPos;
				}
			}
			else
			{
				delParentPos->_right = delPos->_left;
				if (delPos->_left)
				{
					delPos->_left->_parent = delParentPos;
				}
			}
		}
		delete delPos;
		return true;
	}

	//遍历
	void InOrder()
	{
		_InOrder(_root);
	}

	//平衡性
	bool isBalance()
	{
		if (_root == nullptr)
		{
			return true;
		}
		if (_root->_colour == RED)
		{
			return false;
		}

		//计算一条路径的黑节点数量，并以此为基准
		int blacknums = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_colour == BLACK)
				blacknums++;
			cur = cur->_left;
		}
		int count = 0;
		return _isBalance(_root, blacknums, count);
	}




private:

	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_data.first << ":" << root->_data.second << endl;
		_InOrder(root->_right);
	}

	bool _isBalance(Node* const& root, int blacknums, int count)
	{
		if (root == nullptr)
		{
			if (count != blacknums)
			{
				cout << "黑节点数量不一致" << endl;
				return false;
			}

			return true;
		}

		//检查是否存在连续的红节点
		if (root->_colour == RED && root->_parent->_colour == RED)
		{
			cout << "存在连续红结点" << endl;
			return false;
		}


		if (root->_colour == BLACK)
		{
			count++;
		}

		return _isBalance(root->_left, blacknums, count) && _isBalance(root->_right, blacknums, count);
	}

	void RotateL(Node* parent)
	{
		Node* Pparent = parent->_parent;
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		subR->_left = parent;
		parent->_parent = subR;

		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}

		subR->_parent = Pparent;
		if (Pparent)
		{
			if (Pparent->_right == parent)
			{
				Pparent->_right = subR;
			}
			else
			{
				Pparent->_left = subR;
			}
		}
		else
		{
			_root = subR;
		}


	}

	void RotateR(Node* parent)
	{
		Node* Pparent = parent->_parent;
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		// parent 与 subL的关系建立
		subL->_right = parent;
		parent->_parent = subL;

		// parent 与 subLR 的关系建立
		if (subLR != nullptr)
		{
			subLR->_parent = parent;
		}
		parent->_left = subLR;

		// Pparent 与 subL的关系建立
		if (Pparent != nullptr)
		{
			subL->_parent = Pparent;
			if (Pparent->_left == parent)
			{
				Pparent->_left = subL;
			}
			else
			{
				Pparent->_right = subL;
			}
		}
		else
		{
			_root = subL;
			subL->_parent = nullptr;
		}

	}

	void Destroy(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		Destroy(root->_left);
		Destroy(root->_right);
		delete(root);
	}

private:
	Node* _root;

};