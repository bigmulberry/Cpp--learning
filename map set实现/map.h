#pragma once
#include "RBTree.h"

namespace mystd
{
	template<class K,class Value>
	class map
	{
		
	public:
		//内部类 仿函数 拿到Value中的key值
		struct MapKOfValue
		{
			//map 传给红黑树的value类型是pair，为了比较大小，需要提取出key值
			const K& operator()(const pair<const K, Value>& kv)
			{
				return kv.first;
			}
		};

	public:
		// typename 告诉编译器这只是一个名字，暂时不用对模板进行实例化
		typedef typename RBTree<K, pair<const K, Value>, MapKOfValue>::iterator iterator;//typename 确保后面为类型名而非变量
		typedef	typename RBTree<K, pair<const K, Value>, MapKOfValue>::const_iterator const_iterator;
		typedef	typename RBTree<K, pair<const K, Value>, MapKOfValue>::reverse_iterator reverse_iterator;
		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		reverse_iterator rbegin()
		{
			return _t.rbegin();
		}

		reverse_iterator rend()
		{
			return _t.rend();
		}

		pair<iterator,bool> insert(const pair<const K,Value>& data)
		{
			return _t.Insert(data);
		}

		Value& operator[](const K& key)
		{
			pair<iterator, bool> ret = _t.Insert(make_pair(key, Value()));
			return ret.first->second;
		}

		bool erase(const K& key)
		{
			return _t.Erase(key);
		}

		iterator find(const K& key)
		{
			return _t.Find(key);
		}

	private:
		RBTree<K, pair<const K, Value>,MapKOfValue> _t;
	};
}