#pragma once
#include <iostream>
#include <string>
#include <unistd.h>
class Handler
{
public:
    Handler(){}
    ~Handler(){}

    void operator()(int sock,std::string peer_ip,int peer_port)
    {
        char buffer[1024];
        while(true)
        {
            ssize_t size=read(sock,buffer,sizeof(buffer)-1);
            if(size>0)
            {
                buffer[size]=0;
                std::cout<<"client["<<peer_ip<<":"<<peer_port<<"]# "<<buffer<<std::endl;
            
                write(sock,buffer,size);
            }
            else if(size==0)
            {
                std::cout<<"client["<<peer_ip<<":"<<peer_port<<"] close!"<<std::endl;
                break;
            }
            else
            {
                std::cerr<<sock<<"read error"<<std::endl;
                break;
            }
        }
        //需关闭文件描述符，否则造成文件描述符泄漏
        close(sock);
        std::cout<<"service done"<<std::endl;
    }
};

class Task
{
private:
    int sock;
    std::string ip;
    int port;  
    Handler handler;
public:
    Task(){}

    Task(int _sock,std::string _ip,int _port):sock(_sock),ip(_ip),port(_port)
    {}

    void Run()
    {
        handler(sock,ip,port);
    }

    ~Task()
    {}
};