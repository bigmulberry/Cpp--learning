#pragma once 
#include <iostream>
#include <cstring>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <pthread.h>
#include "threadpool.hpp"
#include "Task.hpp"
#define DEFAULT 8081

//记录对端客户端的参数
class Prama
{
public:
    int sock;//传输套接字
    std::string ip;//IP地址
    int port;//端口号
public:
    Prama(int _sock,std::string _ip,int _port)
        :sock(_sock)
        ,ip(_ip)
        ,port(_port)
    {}

    ~Prama()
    {}
};

class TcpServer
{
private:
    int port;//本地端口号
    int listen_sock;//监听socket
    ThreadPool<Task> *tp;//线程池类
public:
    TcpServer(int _port=DEFAULT):port(_port),listen_sock(-1),tp(nullptr)
    {}      

    void InitTcpServer()
    {
        //创建套接字
        listen_sock=socket(AF_INET,SOCK_STREAM,0);//流式套接字
        if(listen_sock<0)
        {
            std::cerr<<"socket error"<<std::endl;
            exit(2);
        }

        //绑定socket
        struct sockaddr_in local;
        memset(&local,0,sizeof(local));
        local.sin_family=AF_INET;
        local.sin_port=htons(port);
        local.sin_addr.s_addr=INADDR_ANY;
        if(bind(listen_sock,(struct sockaddr*)&local,sizeof(local))<0)
        {
            std::cerr<<"bind error"<<std::endl;
            exit(3);
        }

        //监听socket
        if(listen(listen_sock,5)<0)
        {
            std::cerr<<"listen error"<<std::endl;
            exit(4);
        }

        //实例化线程池对象
        tp=new ThreadPool<Task>();
    }

    static void* HandlerRequest(void* arg)
    {
        //分离线程
        pthread_detach(pthread_self());
        Prama* p=(Prama*)arg;
        Service(p->sock,p->ip,p->port);
        delete(p);
        return nullptr;
    }

    void Loop()
    {
        //初始化线程池：创建多个线程
        tp->ThreadInit();

        for(;;)
        {
            //客户端的socket地址
            struct sockaddr_in peer;
            memset(&peer,0,sizeof(peer));
            socklen_t len=sizeof(peer);
            int sock=accept(listen_sock,(struct sockaddr*)&peer,&len);
            if(sock<0)
            {
                std::cout<<"accept error"<<std::endl;
                continue;
            }
            std::string client_ip=inet_ntoa(peer.sin_addr);
            int client_port=ntohs(peer.sin_port);
            std::cout<<"get a new link ->"<<sock<<": ["<<client_ip<<"]:"<<client_port<<std::endl;
            
            //服务器业务
            //线程池版本
            //将 传输socket+客户端ip+客户端端口号 初始化任务对象
            Task t(sock,client_ip,client_port);
            //加入任务队列
            tp->PushTask(t);


            //多线程版本
            //设置客户端参数对象，以便在线程创建时传入例程函数HandlerRequest
            // Prama* p=new Prama(sock,client_ip,client_port);
            // pthread_t tid;
            // //创建线程
            // pthread_create(&tid,nullptr,HandlerRequest,p);


            //多进程版本
            //让子进程执行业务
            //signal(SIGCHLD,SIG_IGN);
            //父进程忽略SIGCHLD信号，父进程不用wait，让系统自动回收子进程资源，
            // pid_t id=fork();
            // if(id==0)
            // {
            //     //child
            //     if(fork()>0)
            //     {
            //         exit(0);
            //     }
            //     //由成为孤儿进程的grandson执行业务
            //     close(listen_sock);
            //     Service(sock,peer_ip,peer_port);
            //     exit(0);//孙子进程完成业务直接退出
            // }
            // waitpid(id,nullptr,0);
             
            //close(sock);
        }
    }

    static void Service(int sock,std::string peer_ip,int peer_port)
    {
        char buffer[1024];
        while(true)
        {
            ssize_t size=read(sock,buffer,sizeof(buffer)-1);
            if(size>0)
            {
                buffer[size]=0;
                std::cout<<"client["<<peer_ip<<":"<<peer_port<<"]# "<<buffer<<std::endl;
            
                write(sock,buffer,size);
            }
            else if(size==0)
            {
                std::cout<<"client["<<peer_ip<<":"<<peer_port<<"] close!"<<std::endl;
                break;
            }
            else
            {
                std::cerr<<sock<<"read error"<<std::endl;
                break;
            }
        }
        //需关闭文件描述符，否则造成文件描述符泄漏
        close(sock);
        std::cout<<"service done"<<std::endl;
    }

    ~TcpServer()
    {
        if(listen_sock>0)
        {
            close(listen_sock);
        }
        delete tp;
    }
}; 

