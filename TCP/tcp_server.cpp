#include "tcp_server.hpp"

void Usage(char* c)
{
    std::cout<<"Usage: "<<c<<" port"<<std::endl;
}

// ./tcp_server port
int main(int argc , char* argv[])
{
    if(argc<=1)
    {
        Usage(argv[0]);
        exit(1);
    }
    std::cout<<"HELLO SERVER"<<std::endl;
    TcpServer* server=new TcpServer(atoi(argv[1]));
    server->InitTcpServer();
    server->Loop();
    delete server;
    return 0;
}
