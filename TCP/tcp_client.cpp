#include "tcp_client.hpp"

void Usage(char* c)
{
    std::cout<<"Usage: "<<c<<" server_ip server_port"<<std::endl;
}

// ./tcp_client server_ip server_port
int main(int argc,char* argv[])
{
    if(argc!=3)
    {
        Usage(argv[0]);
        exit(1);
    }
    std::cout<<"HELLO CLIENT"<<std::endl;
    std::string server_ip=argv[1];
    int server_port=atoi(argv[2]);
    TcpClient* client=new TcpClient(server_ip,server_port);
    client->InitTcpClient();
    client->Start();
    return 0;
}
