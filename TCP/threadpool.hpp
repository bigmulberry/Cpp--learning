 #pragma once 
#include <iostream>
#include <string>
#include <queue>
#include <unistd.h>
#include <pthread.h>
const int g_num=5;
template <class T>
class ThreadPool
{
public:
    ThreadPool(int num=g_num):_num(num)
    {
        pthread_mutex_init(&_mtx,nullptr);
        pthread_cond_init(&_cond,nullptr);
    }


    static void *Routine(void *args)
    {
        pthread_detach(pthread_self());
        ThreadPool<T> *tp = (ThreadPool<T> *)args;
        while (true)
        {
            tp->Lock();
            while(tp->IsEmpty())
            {
                //任务队列为空，线程将阻塞在此处
                tp->Wait();
            }
            //该任务队列中以有任务
            T t;
            tp->PopTask(&t);
            tp->Unlock();//静态变量没有this指针只能依赖传入的参数
            //处理任务不在临界区内，需先释放锁再执行相应的处理
            //执行任务
            t.Run();
        }
    }

    void ThreadInit()
    {
       pthread_t tid; 
       for(int i=0;i<_num;++i)
       {
            pthread_create(&tid,nullptr,Routine,(void*)this);
       }
    }

    void PushTask(const T& in)
    {
        Lock();
        _task_queue.push(in);
        WakeUp();
        Unlock();
    }

    void PopTask(T* out)
    {
        *out=_task_queue.front();
        _task_queue.pop();
    }

    ~ThreadPool()
    {
        pthread_mutex_destroy(&_mtx);
        pthread_cond_destroy(&_cond);
    }

    void Lock()
    {
        pthread_mutex_lock(&_mtx);
    }
    void Unlock()
    {
        pthread_mutex_unlock(&_mtx);
    }

    void Wait()
    {
        pthread_cond_wait(&_cond,&_mtx);
    }
    void WakeUp()
    {
        pthread_cond_signal(&_cond);
    }
    bool IsEmpty()
    {
        return _task_queue.empty();
    }
private:
    int _num;
    std::queue<T> _task_queue;
    pthread_mutex_t _mtx;
    pthread_cond_t _cond;
};
