#pragma once 
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

class TcpClient
{
private:
    std::string server_ip;
    int server_port;
    int sock;
public:
    TcpClient(std::string _ip,int _port):server_ip(_ip),server_port(_port),sock(-1)
    {

    }

    void InitTcpClient()
    {
        sock=socket(AF_INET,SOCK_STREAM,0);
        if(sock<0)
        {
            std::cerr<<"socket error"<<std::endl;
            exit(2);
        }
    }

    void Start()
    {
        struct sockaddr_in server_addr;
        memset(&server_addr,0,sizeof(server_addr));
        server_addr.sin_family=AF_INET;
        server_addr.sin_port=htons(server_port);/*服务器端口号转为网络字节序*/
        //inet_addr将服务器IP地址的十进制点分字符串转换为32位整型并转网络字节序
        server_addr.sin_addr.s_addr=inet_addr(server_ip.c_str());
        socklen_t len=sizeof(server_addr);
        //按照返回值判断是否connect成功
        if(connect(sock,(struct sockaddr*)&server_addr,len)==0)
        {
            //success 
            //向服务器执行请求
            std::cout<<"connect success"<<std::endl;
 
            Request(sock);
        }
        else
        {
            //false
            std::cout<<"connect error"<<std::endl;
        }
    }

    void Request(int sock)
    {
        std::string message;
        char buffer[1024];
        while(true)
        {
            std::cout<<"Please input# ";
            std::cin>>message;
            write(sock,message.c_str(),message.size());

            ssize_t retsize=read(sock,buffer,sizeof(buffer)-1);
            if(retsize>0)
            {
                buffer[retsize]=0;
            }
            std::cout<<"server echo# "<<buffer<<std::endl;
        }
    }

    ~TcpClient()
    {
        if(sock>0)
        {
            close(sock);
        }
    }
};

