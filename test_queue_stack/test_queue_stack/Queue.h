#pragma once

namespace sjl
{
	//ģ��������
	//template <class T,class Container=deque<T>>
	template <class T, class Container = list<T>>
	class queue
	{
	public:
		void push(const T& t)
		{
			_con.push_back(t);
		}

		void pop()
		{
			_con.pop_front();
		}

		T front()
		{
			return _con.front();
		}

		T back()
		{
			return _con.back();
		}
		
		size_t size()
		{
			return _con.size();
		}
		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};
}
