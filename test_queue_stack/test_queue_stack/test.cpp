#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <queue>
#include <stack>
#include <vector>
#include <list>
#include <deque>
#include "Stack.h"
#include "Queue.h"
#include <functional>
using std::cout;
using std::endl;

//void test_stack()
//{
//	std::stack<int> s1;
//	s1.push(1);
//	s1.push(2);
//	s1.push(3);
//	s1.push(4);
//
//	while (!s1.empty())
//	{
//		cout << s1.top() << " ";
//		s1.pop();
//	}
//	cout << endl;
//}
//
//void test_queue()
//{
//	std::queue<int> q;
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//
//	while (!q.empty())
//	{
//		cout << q.front() << " ";
//		q.pop();
//	}
//	cout << endl;
//}
//
//int main()
//{
//
//	test_stack();
//	test_queue();
//	return 0;
//}


//***************************************************************************************
// 自己实现stack
//***************************************************************************************

//int main()
//{
//
//	sjl::stack<int> s;
//	s.push(1);
//	s.push(2);
//	s.push(3);
//	s.push(4);
//
//	while (!s.empty())
//	{
//		cout << s.top() << ' ';
//		s.pop();
//	}
//	cout << endl;
//
//	//在调用时利用模板
//	sjl::queue<int,deque<int>> q;
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//
//	while (!q.empty())
//	{
//		cout << q.front() << ' ';
//		q.pop();
//	}
//	cout << endl;
//
//	//priority_queue 默认大堆-》大的数据优先级高，降序，less
//	std::priority_queue<int> pq;
//	pq.push(10);
//	pq.push(20);
//	pq.push(30);
//	pq.push(40);
//	while (!pq.empty())
//	{
//		cout << pq.top() << ' ';
//		pq.pop();
//	}
//	cout << endl;
//
//
//	//变成小堆  greater
//	std::priority_queue<int,vector<int>,greater<int> > pq2;
//	pq2.push(10);
//	pq2.push(20);
//	pq2.push(30);
//	pq2.push(40);
//	while (!pq2.empty())
//	{
//		cout << pq2.top() << ' ';
//		pq2.pop();
//	}
//	cout << endl;
//}




//*****************************************
//实现priority_queue
#include "Priority_queue.h"


//int main()
//{
//	std::priority_queue<int> pq;
//	pq.push(3);
//	pq.push(2);
//	pq.push(1);
//	pq.push(4);
//	pq.push(6);
//	pq.push(5);
//
//	while (!pq.empty())
//	{
//		cout << pq.top() << " ";
//		pq.pop();
//	}
//	cout << endl;
//	return 0;
//}

//仿函数——函数对象  这个类的对象可以像函数一样去使用  
//函数对象代替函数指针，类似于用引用代替指针


template <class T>
class LessInt
{
public:
	bool operator()(const T& l, const T& r)
	{
		return l < r;
	}
};


bool Lessfunc1(int l,int r)
{
	return l < r;
}



//int main()
//{
//	LessInt<int> Lessfunc2;
//	cout << Lessfunc1(1, 2) << endl;
//	cout << Lessfunc2(1, 2) << endl;
//	cout << Lessfunc2.operator()(1, 2) << endl;
//}

int main()
{
	std::priority_queue<int,vector<int>,sjl::Greater<int>> pq;
	pq.push(3);
	pq.push(2);
	pq.push(1);
	pq.push(4);
	pq.push(6);
	pq.push(5);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;
	return 0;
}




































