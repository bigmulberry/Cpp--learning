#pragma once


namespace sjl
{
	//自己创建函数对象
	template <class T>
	struct Less
	{
		bool operator()(const T& l, const T& r)
		{
			return l < r;
		}
	};

	template <class T>
	struct Greater
	{
		bool operator()(const T& l, const T& r)
		{
			return l > r;
		}
	};


	template <class T,class Container=vector<T>,class Compare=Less<T>>
	class priority_queue
	{
		//typedef Container::value_type VT;//没有实例化的对象，不知道Container是哪一个容器，也就不知道value_type的类型
		typedef typename Container::value_type VT;//typename告知编译器后面是一个类型名称，等到类模板实例化后再去找固定了的value_type

	public:
		void AdjustUp(size_t pos)
		{
			Compare com;
			size_t child = pos;
			size_t parent = (child - 1) / 2;
			while (child != 0)
			{
				//if (_con[child] > _con[parent])
				if(com(_con[parent], _con[child]))
				{
					swap(_con[child], _con[parent]);
				}
				child = parent;
				parent = (child - 1) / 2;
			}
		}

		void AdjustDown(size_t pos)//默认大堆，小的往下，大的往上
		{
			Compare com;
			size_t parent = pos;
			size_t child = pos * 2 + 1;
			while (child<_con.size())
			{
				size_t child2 = child + 1;
				if (child2<_con.size() && com(_con[child],_con[child2]))
				{
					child = child2;
				}
				if (com(_con[parent] , _con[child]))
				{
					swap(_con[child], _con[parent]);
				}
				parent = child;
				child = parent * 2 + 1;
			}
			
		}

		void push(const T& x)
		{
			_con.push_back(x);
			//插入在尾部，向上调整
			AdjustUp(_con.size() - 1);//参数1：调整的对象，参数2：调整的位置
		}

		void pop()
		{
			swap(_con.front(), _con.back());
			_con.pop_back();
			//头部向下调整
			AdjustDown( 0);
		}

		T top()
		{
			return _con.front();
		}

		bool empty()
		{
			return _con.empty();
		}

	private:
		Container _con;

	};
}