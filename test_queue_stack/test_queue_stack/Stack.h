#pragma once
using namespace std;

namespace sjl
{
	//template <class T,class Container=vector<T>>
	//template <class T,class Container=list<T>>
	template <class T,class Container=deque<T>>

	class stack
	{
	public:

		void push(const T& t)
		{
			_con.push_back(t);
		}

		void pop()
		{
			_con.pop_back();
		}

		T top()
		{
			return _con.back();
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};
}