#include "Date.h"

//  [12/8/2021 14025]
//*******************************************************************************



inline int GetMonthDay(int year, int month)
{
	static int dayArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	int day = dayArray[month];

	if (month==2 && (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
	{
		day = 29;
	}

	return day;
}


Date::Date(int year, int month, int day)
{
	//注意年月日的合法性
	//闰年
	assert(year >= 0);
	assert(month >= 1 && month <= 12);
	assert(day >= 1 && day <= GetMonthDay(year, month));
	_year = year;
	_month = month;
	_day = day;
}


void Date::Print()
{
	cout << _year <<"年" << _month << "月" << _day << "日" << endl;
}

//日期加等天数
Date& Date::operator+=(int days)
{
	_day += days;
	//判断合法性
	while(_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month > 12)
		{
			_year++;
			_month = 1;
		}
	}
	
	return *this;
}

Date Date::operator+(int days)
{
	//实现加号，是不会对this本身产生影响的
	//所以要新建对象
	Date tmp(*this);//拷贝构造


	//方法一，老老实实再把+=再写一遍，不过这里对象是tmp
	//tmp._day += days;
	//while (tmp._day > GetMonthDay(tmp._year, tmp._month))
	//{
	//	tmp._day -= GetMonthDay(tmp._year, tmp._month);
	//	tmp._month++;
	//	if (tmp._month > 12)
	//	{
	//		tmp._year++;
	//		tmp._month = 1;
	//	}
	//}

	//方法二，复用+= 我焯
	tmp += days;

	return tmp;
}


// -= this的对象还在，可以返回引用
Date& Date::operator-=(int days)
{
	_day -= days;
	while (_day <= 0)
	{
		if (_month == 1)
		{
			_year--;
			_month=12;
		}
		else
		{
			_month--;
		}

		_day += GetMonthDay(_year, _month);
	}

	return *this;
}


//只是减的话不能对原有对象进行改变，要用临时变量传回值
Date Date::operator-(int days)
{
	Date tmp(*this);//拷贝构造

	//吸取教训 直接用上复用
	tmp -= days;

	return tmp;
}


//日期的 >
bool Date::operator>(const Date& oneday)
{
	//year比较
	if (_year > oneday._year)
	{
		return true;
	}
	else if (_year == oneday._year)
	{
		if (_month > oneday._month)
		{
			return true;
		}
		else if (_month == oneday._month)
		{
			if (_day > oneday._day)
			{
				return true;
			}
		}
	}
	return false;
}

//两个日期的相减 
int Date::operator-(const Date& oneday)
{

}







