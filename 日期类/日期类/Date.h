#pragma once

#include <iostream>
#include <assert.h>
#include <stdbool.h>
using std::cout;
using std::cin;
using std::endl;

class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1);

	void Print();

	Date& operator+=(int days);

	Date operator+(int days);
	
	Date& operator-=(int days);
	Date operator-(int days);

	bool operator>(const Date& oneday);
	//bool operator<(const Date& oneday);
	//bool operator==(const Date& oneday);

	//两个日期相减
	int operator-(const Date& oneday);

	//前置++   ++d  ->   d.operator(&d);
	Date& operator++();
	//后置++   d++  ->d.operator(&d,int)
	//   int参数不许要给实参，只是为了和前置++构成重载
	Date operator++(int);

private:
	int _year;
	int _month;
	int _day;
};

