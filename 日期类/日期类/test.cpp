#define _CRT_SECURE_NO_WARNINGS 1
#include "Date.h"


int main()
{
	Date d1(2020,2,29);
	d1 += 40;
	d1.Print();
		
	Date d2=d1+100;
	d2.Print();

	Date d3(2021, 5, 24);
	d3 -= 78;
	d3.Print();

	Date d4 = d3 - 78;
	d4.Print();

	Date d5(2022, 1, 1);
	Date d6(2022, 1, 2);
	cout << (d6 > d5) << endl;

	return 0;
}

















