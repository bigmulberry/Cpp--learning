#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <map>
#include <string>
#include <stack>
#include <vector>
using namespace std;

//中缀转为后缀
vector<string> infixtosuffix()
{
	string mid;
	cin >> mid;
	vector<string> ret;//返回的字符串
	map<char, int> P;//符号优先级
	P['('] = P[')'] = 0;
	P['+'] = P['-'] = 1;
	P['*'] = P['/'] = 2;
	stack<char> sign;//符号栈

	size_t i = 0;
	while (i < mid.size())
	{
		if (isalnum(mid[i]))
		{
			string tmp;
			while (i < mid.size() && isalnum(mid[i]))
			{
				tmp += mid[i];
				i++;
			}
			ret.push_back(tmp);
		}

		while (i < mid.size() && !sign.empty() && P[mid[i]] <= P[sign.top()] && mid[i] != '(')//当前符号的优先级小于等于栈顶符号则出栈
		{
			if ((mid[i] == ')') && (sign.top() == '('))
			{
				sign.pop();
				i++;
			}
			else
			{
				ret.push_back(string(1, sign.top()));
				sign.pop();
			}
		}
		if (i < mid.size())
		{
			sign.push(mid[i]);
			i++;
		}

	}
	while (!sign.empty())
	{
		ret.push_back(string(1, sign.top()));
		sign.pop();
	}
	//cout << ret << endl;
	return ret;
}

 //后缀计算
int reverse_poland_cal(vector<string>& tokens)
{
	//操作数入栈，
	//遇到操作符
	//连拿两个操作数运算
	//运算结果压栈

	stack<int> s;
	for (size_t i = 0; i < tokens.size(); ++i)
	{
		if (tokens[i] == "+"
			|| tokens[i] == "-"
			|| tokens[i] == "*"
			|| tokens[i] == "/")
		{
			int  right = s.top();
			s.pop();
			switch (tokens[i][0])
			{
			case '+':
				s.top() += right;
				break;
			case '-':
				s.top() -= right;
				break;
			case '*':
				s.top() *= right;
				break;
			case '/':
				s.top() /= right;
				break;
			}
		}
		else
		{
			s.push(stoi(tokens[i]));
		}
	}
	return s.top();
}

int main()
{
	vector<string> s=infixtosuffix();
	for (size_t i = 0; i < s.size(); ++i)
	{
		cout << s[i] << ' ';
	}
	cout << endl;
	cout<<reverse_poland_cal(s)<<endl;
	return 0;
}