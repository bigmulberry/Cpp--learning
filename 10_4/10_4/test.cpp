#include <iostream>
using namespace std;
//
//int main()
//{
//	int arr[5] = {1,2,3,4};
//	for (int i = 0; i < 5; i++)
//	{
//		cout << arr[i] << endl;
//	}
//
//	cout << arr << endl;
//	return 0;
//}


//数组逆置
//int main()
//{
//	int arr[5] = { 1,3,2,5,4 };
//	int left, right;
//	left = 0;
//	right = 4;
//	while (left < right)
//	{
//		int a = arr[left];
//		arr[left] = arr[right];
//		arr[right] = a;
//		left++;
//		right--;
//	}
//	for (int i = 0; i < 5; i++)
//	{
//		cout << arr[i] ;
//	}
//
//
//	return 0;
//}

//冒泡排序
//int main()
//{
//	int arr[] = { 4,2,8,0,5,7,1,3,9 };
//	int i, j;
//	int flag = 0;
//	for (i = 0; i < 8&&flag==0; i++)
//	{
//		flag = 1;
//		for (j = 0; j < 8 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//				flag = 0;
//			}
//		}
//	}
//	for (i = 0; i < 9; i++)
//	{
//		cout << arr[i] << " ";
//	}
//	return 0;
//}

//int main()
//{
//	int a[2][3] = { {1,2,3},{4,5,6} };
//	for (int i = 0; i < 2; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			cout << a[i][j] << " ";
//		}
//		cout << endl;
//	}
//	return 0;
//}

//int main()
//{
//	int a = 1;
//	int b = 3;
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	cout << "a=" << a << "\nb=" << b;
//
//	return 0;
//}

//int main()
//{
//	int* p = NULL;
//	//cout << *p << endl; //0~255系统内存占用，不能访问
//	p = (int*)0x1100;
//	cout << *p << endl;//野指针，非自己申请，非法访问内存
//	return 0;
//}

//int main()
//{
//	int a[] = { 1,2,3,4,5 };
//	int* p = a;
//	for (int i = 0; i < sizeof(a) / sizeof(a[0]); i++)
//	{
//		cout << *p << " ";
//		p++;
//	}
//	return 0;
//}

//c++中，创建结构体变量时，struct关键字可以省略
struct student
{
	string name;
	int age;
	int score;
};
int main()
{
	student a1 = { "alice",18,354};
	cout << "名字" << a1.name << "\t" << "年龄" << a1.age << "\t" << "分数" << a1.score << endl;
	return 0;
}