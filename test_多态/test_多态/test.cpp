#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//class Car {
//protected:
//	string _colour = "白色"; // 颜色
//	string _num = "京A12345"; // 车牌号
//};
//class BMW : public Car {
//public:
//	void Drive() { cout << "操控" << endl; }
//};
//
//class Tire {
//protected:
//	string _brand = "Michelin"; // 品牌
//	size_t _size = 17; // 尺寸
//};
//class Car {
//protected:
//	string _colour = "白色"; // 颜色
//	string _num = "京A12345"; // 车牌号
//	Tire _t; // 轮胎
//};

//int main()
//{
//	int i;
//	char ch;
//	cin >> i;
//	cin >> ch;
//
//	cout << i << endl;
//	cout << ch << endl;
//
//	return 0;
//}


//class B
//{
//public:
//	virtual void f1()
//	{
//		cout << "B::f1()" << endl;
//	}
//
//	virtual void f2()
//	{
//		cout << "B::f2()" << endl;
//	}
//
//	void f3()
//	{
//		cout << "B::f3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//class D:public B
//{
//public:
//	virtual void f1()
//	{
//		cout << "D::f1()" << endl;
//	}
//
//	virtual void ff()
//	{
//		cout << "D::ff()" << endl;
//
//	}
//private:
//	int _d = 2;
//};
//
//int main()
//{
//	B  b;
//	D d;
//
//	return 0;
//}


///////////////////////////////////////////

//class Person
//{
//public:
//	virtual void Buy()
//	{
//		cout << "全价票" << endl;
//	}
//};
//
//class Student :public Person
//{
//public:
//	virtual void Buy()
//	{
//		cout << "半价票" << endl;
//	}
//};
//
//class Child :public Person
//{
//public:
//	virtual void Buy()
//	{
//		cout << "免费" << endl;
//	}
//};
//void func_wrong(Person p)
//{
//	p.Buy();
//}
//
//void func(Person& rp)
//{
//	rp.Buy();
//}
//
//void func(Person* pp)
//{
//	pp->Buy();
//}
//
//int main()
//{
//	Person p;
//	Student s;
//	Child c;
	//func_wrong(p);
	//func_wrong(s);
	//func_wrong(c);
	//cout << endl;

	//func(p);
	//func(s);
	//func(c);

	//func(&p);
	//func(&s);
	//func(&c);
//}
//
//void func(Person& p)
//{
//	p.Buy();
//}
//
//int main()
//{
//	Person Mike;
//	Student Alice;
//	Child Tom;
//	func(Mike);
//	func(Alice);
//	func(Tom);
//
//	return 0;
//}


//class A
//{};
//class B :public A
//{};
//class Person
//{
//public:
//	virtual A* func()
//	{
//		cout << "Person::func()" << endl;
//		return new A;
//	}
//};
//class Student :public Person
//{
//public:
//	virtual B* func()
//	{
//		cout << "Student::func()" << endl;
//		return new B;
//	}
//};
//
//int main()
//{
//	Person p;
//	Student s;
//	Person* ptr = &p;
//	ptr->func();
//	ptr = &s;
//	ptr->func();
//	return 0;
//}

/////////////////////////////////////////////////////////////////////////////////////////
//class Person {
//public:
//	 virtual ~Person() { cout << "~Person()" << endl; }
//};
//
//class Student : public Person {
//public:
//	 virtual ~Student() { cout << "~Student()" << endl; }
//};
//
//int main()
//{
//	Person* pp=new Person;
//	Person* ps=new Student;//依旧是虚函数的使用规则
//
//	delete pp; //pp->析构函数+operator delete(pp)
//	delete ps; //ps->子类重写的析构函数+operator delete(ps)
//
//	return 0;
//}


///////////////////////////////////////////////////////////
// final
//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }//此处报错，Drive不能被重写
//};


/////////////////////////////////////////////////////////////
//override

////class Car {
//public:
//	virtual void Drive() {}
//};
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};



//////////////////////////////////////////////////

//class A
//{
//public:
//	virtual void func1()
//	{
//		cout << "A::func1()" << endl;
//	}
//
//	virtual void func2()
//	{
//		cout << "A::func2()" << endl;
//	}
//};
//
//int main()
//{
//	A a;
//	A* pa = &a;
//	printf("vfptr:%p\n", *((int*)pa));
//
//	int* ptr = (int*)(*(int*)pa);
//	while (1)
//	{
//		printf("%p\n", *ptr);
//		ptr += 1;
//		if(*ptr==0)
//			break;
//	}
//
//}


/////////////////////////////////////////////////////
class Base1 {
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};
class Base2 {
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};
class Derive : public Base1, public Base2 {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};
typedef void(*VFPTR) ();
void PrintVTable(VFPTR vTable[])
{
	cout << " 虚表地址>" << vTable << endl;
	for (int i = 0; vTable[i] != nullptr; ++i)
	{
		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
		VFPTR f = vTable[i];
		f();
	}
	cout << endl;
}
int main()
{
	Derive d;
	VFPTR* vTableb1 = (VFPTR*)(*(int*)&d);
	PrintVTable(vTableb1);
	VFPTR* vTableb2 = (VFPTR*)(*(int*)((char*)&d + sizeof(Base1)));
	PrintVTable(vTableb2);
	return 0;
}