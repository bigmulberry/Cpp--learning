#pragma once
#include "reverse_iterator.h"

template<class T>
struct Listnode
{
	Listnode<T>* _next;
	Listnode<T>* _prev;
	T _val;
	Listnode(const T& val = T()) :_val(val), _next(nullptr), _prev(nullptr)
	{}
};
//*************************************************************************************************************************
//list迭代器
template<class T,class Ref,class Ptr>
class list_iterator
{
public:
	typedef Listnode<T> node;
	typedef list_iterator<T,Ref,Ptr> self;

	node* _pnode;

	//构造
	list_iterator(node* x):_pnode(x)
	{

	}

	//拷贝构造，赋值重载以及析构 都由编译器提供
	//这里无需深拷贝，因为，指针只需修改指向的内容，如果拷贝一份新的出来，那无法对原来的list结点进行修改了
	//析构也不能对空间进行释放，一个迭代器不应具备这种能力，对于空间的开辟和释放都应由list类本身实现
	//迭代器是借助结点的指针访问修改链表
	list_iterator(const self& x):_pnode(x._pnode)
	{

	}

	//解引用
	 Ref operator*()
	{
		return _pnode->_val;
	}

	 Ptr operator->()
	 {
		 return &(_pnode->_val);
	 }

	//这种针对传入const的iterator的写法是不对的，因为此时面对的是const的list对象，而不是const的迭代器
	 //const T& operator*()const
	 //{
		// return _pnode->_val;
	 //}



	//前置++
	self& operator++()
	{
		_pnode = _pnode->_next;
		return *this;
	}


	//后置++
	self operator++(int)
	{
		//self temp(_pnode);
		self temp(*this);//使用编译器提供的拷贝构造
		++(*this);
		return temp;
	}

	//前置++
	self& operator--()
	{
		_pnode = _pnode->_prev;
		return *this;
	}

	//后置--
	self operator--(int)
	{
		self temp(*this);
		--(*this);
		return temp;
	}

	bool operator==(const self& x)const
	{
		return _pnode == x._pnode;
	}

	bool operator!=(const self& x)const
	{
		return _pnode!=x._pnode;
	}


};

//专门构建一个const 迭代器*********************************************************************************************************
template<class T>
class const_list_iterator
{
public:
	typedef Listnode<T> node;
	typedef const_list_iterator<T> self;

	//构造
	const_list_iterator(node* x) :_pnode(x)
	{

	}

	//解引用
	const T& operator*()
	{
		return _pnode->_val;
	}



	//前置++
	self& operator++()
	{
		_pnode = _pnode->_next;
		return *this;
	}

	//后置++
	self operator++(int)
	{
		//self temp(_pnode);
		self temp(*this);//使用编译器提供的拷贝构造
		++(*this);
		return temp;
	}

	//前置++
	self& operator--()
	{
		_pnode = _pnode->_prev;
		return *this;
	}

	//后置--
	self operator--(int)
	{
		self temp(*this);
		--(*this);
		return temp;
	}

	bool operator==(const self& x)const
	{
		return _pnode == x._pnode;
	}

	bool operator!=(const self& x)const
	{
		return _pnode != x._pnode;
	}
private:
	node* _pnode;
};


//**************************************************************************************************************************
//list类
template<class T>
class mylist
{
public:
	typedef Listnode<T> node;
	
	//迭代器
	typedef list_iterator<T,T&,T*> iterator;
	typedef list_iterator<T,const T&,const T*> const_iterator;

	//反向迭代器
	typedef my_std::reverse_iterator<iterator, T&, T*> reverse_iterator;
	typedef my_std::reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;

	//默认构造
	mylist()
	{
		_head = new node;  
		_head->_next = _head;
		_head->_prev = _head;
	}

	template <class InputIterator>
	mylist(InputIterator first,InputIterator last)
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
		while (first != last)
		{
			push_back(*first);
			++first;
		}
	}

	mylist(size_t n, const T& x=T())//此构造会与迭代器区间构造产生冲突——非法的间接寻址 因为两个int会自动匹配到迭代器
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
		while(n)
		{
			push_back(x);
			n--;
		}
	}

	//增加重载版本——可匹配两个int的构造
	mylist(int n, const T& x = T())
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
		while (n)
		{
			push_back(x);
			n--;
		}
	}

	//拷贝构造 -尾插
	//mylist(const mylist& l):_head(new node)
	//{
	//	_head->_next=_head;
	//	_head->_prev=_head;
	//	const_iterator it = l.begin();
	//	while (it != l.end())
	//	{
	//		push_back((it++)._pnode->_val);
	//	}
	//}

	//拷贝构造 ——复用迭代器的构造函数
	mylist(const mylist& l)
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
		mylist<T> temp(l.begin(), l.end());// 复用构造
		//temp与this互换哨兵结点
		swap(temp._head, _head);
	}


	//赋值重载——尾插
	//mylist<int>& operator=(const mylist<int>& l)
	//{
	//	
	//	if (this != &l)//防止自己为自己赋值
	//	{
	//		clear();
	//		for (auto e : l)
	//		{
	//			push_back(e);
	//		}
	//	}
	//	return *this;
	//}


	//赋值重载——在传值中复用拷贝构造
	mylist<int>& operator=(mylist<int> l)
	{
		swap(_head, l._head);
		return *this;
	}


	//析构
	~mylist()
	{
		clear();
		delete _head;
		_head = nullptr;
	}

	void clear()
	{
		iterator it = begin();
		
		while (it != end())
		{
			//node* temp = it._pnode;
			//++it;
			//delete temp;

			delete ((it++)._pnode);

			//erase(it++);
		}
		_head->_next = _head;
		_head->_prev = _head;
	}

	void push_back(const T& x)
	{
		//node* newnode = new node(x);

		//node* tail = _head->_prev;
		//tail->_next = newnode;
		//newnode->_prev = tail;
		//newnode->_next = _head;
		//_head->_prev = newnode;

		//复用insert
		insert(end(), x);
	}

	//迭代器
	iterator begin()
	{
		return iterator(_head->_next);
	}
	iterator end()
	{
		return iterator(_head);
	}

	//传入的是const对象
	const_iterator begin()const
	{
		return const_iterator(_head->_next);
	}
	const_iterator end()const
	{
		return const_iterator(_head);
	}


	//反向迭代器
	reverse_iterator rbegin()
	{
		return reverse_iterator(end());
	}

	reverse_iterator rend()
	{
		return reverse_iterator(begin());
	}

	const_reverse_iterator rbegin()const
	{
		return const_reverse_iterator(end());
	}

	const_reverse_iterator rend()const
	{
		return const_reverse_iterator(begin());
	}

	iterator insert(iterator pos,const T& x)
	{
		node* cur = pos._pnode;
		node* before_cur = cur->_prev;

		node* newnode = new node(x);
		newnode->_next = cur;
		newnode->_prev = before_cur;
		before_cur->_next = newnode;
		cur->_prev = newnode;

		return iterator(newnode);
	}

	iterator erase(iterator pos)
	{
		//不能删除哨兵
		assert(pos != end());
		node* before_pos = pos._pnode->_prev;
		node* after_pos = pos._pnode->_next;

		before_pos->_next = after_pos;
		after_pos->_prev = before_pos;

		delete pos._pnode;
		return iterator(after_pos);
	 }

	void push_front(const T& x)
	{
		insert(begin(),x);
	}

	void pop_back()
	{
		erase(--end());
	}
	void pop_front()
	{
		erase(begin());
	}



private:
	node* _head;
};