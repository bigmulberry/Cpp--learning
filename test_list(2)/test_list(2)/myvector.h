#pragma once
#ifndef _MYVECTOR_H_
#define _MYVECTOR_H_
template<class T>
class myvector
{
public:
	typedef T* iterator;
	typedef const T* const_iterator;
	typedef my_std::reverse_iterator<iterator, T&, T*> reverse_iterator;
	typedef my_std::reverse_iterator<const_iterator,const T&,const T*> const_reverse_iterator;

	reverse_iterator rbegin()
	{
		return reverse_iterator(end());
	}

	reverse_iterator rend()
	{
		return reverse_iterator(begin());
	}

	//构造函数
	myvector() :_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
	{

	}

	//构造函数3
	myvector(size_t n, const T& val)
		:_start(nullptr)
		, _finish(nullptr)
		, _endofstorage(nullptr)
	{
		reserve(n); //调用reserve函数将容器容量设置为n
		for (size_t i = 0; i < n; i++) //尾插n个值为val的数据到容器当中
		{
			push_back(val);
		}
	}

	myvector(int n, const T& val)
		:_start(nullptr)
		, _finish(nullptr)
		, _endofstorage(nullptr)
	{
		reserve(n); //调用reserve函数将容器容量设置为n
		for (int i = 0; i < n; i++) //尾插n个值为val的数据到容器当中
		{
			push_back(val);
		}
	}


	//一个类模板的成员函数可以是一个函数模板?
	template<class InputIterator>
	myvector(InputIterator first, InputIterator last) :_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
	{
		while (first != last)
		{
			push_back(*first);
			first++;
		}
	}

	void swap(myvector<T>& y)
	{
		std::swap(_start, y._start);
		std::swap(_finish, y._finish);
		std::swap(_endofstorage, y._endofstorage);

	}

	//拷贝构造
	//myvector(const myvector<T>& v):_start(nullptr),_finish(nullptr),_endofstorage(nullptr)
	//{
	//    myvector<T> temp(v.begin(),v.end());
	//    swap(temp);//将两者的指针互换

	//}


	myvector(const myvector<T>& v) :_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
	{
		reserve(v.capacity());
		memcpy(_start, v._start, sizeof(T) * v.size());
		//for(size_t i=0;i<0)
		_finish = _start + v.size();


	}
	//赋值重载
	myvector<T>& operator=(myvector<T> v)
	{
		swap(v);
		return *this;
	}


	//析构
	~myvector()
	{
		if (_start)
		{
			delete[] _start;
			_start = _finish = _endofstorage = nullptr;
		}
	}
	//size
	size_t size()const
	{
		return _finish - _start;
	}

	size_t capacity()const
	{
		return _endofstorage - _start;
	}

	void clear()
	{
		_finish = _start;
	}

	//reserve
	void reserve(size_t n)
	{
		if (n > capacity())
		{
			T* temp = new T[n];
			int sz = size();
			if (_start != nullptr)
			{
				int i = 0;
				for (i = 0; i < sz; ++i)
				{
					temp[i] = _start[i];
				}
			}
			delete[] _start;
			_start = temp;
			_finish = _start + sz;
			_endofstorage = _start + n;
		}
	}

	void resize(size_t n, const T& val = T())//注意：C++中的匿名对象是pure RValue(纯粹的右值)，引用匿名对象一定要在前用const修饰，如果生成的匿名对象在外部有对象等待被其实例化，此匿名对象的生命周期就变成了外部对象的生命周期。
	{
		if (n < size())
		{
			_finish = _start + n;
		}
		else if (n > size())
		{
			reserve(n);
			//把_finish~_finish+n的位置填为val
			while (_finish < _start + n)
			{
				*_finish = val;
				++_finish;
			}
		}
	}

	//push_back
	void push_back(const T& x)
	{
		//扩容
		if (_finish == _endofstorage)
		{
			size_t newcapacity = capacity() == 0 ?4: 2 * capacity();
			reserve(newcapacity);
		}
		*_finish = x;
		_finish++;
	}

	void pop_back()
	{
		assert(_start < _finish);
		_finish--;
	}

	T& operator[](size_t i)
	{
		assert(i < size());
		return _start[i];
	}

	iterator insert(iterator pos, const T& x)
	{
		assert(pos >= _start);
		assert(pos <= _finish);
		if (_finish == _endofstorage)
		{
			size_t len = pos - _start;
			reserve(capacity() == 0 ? 4 : 2 * capacity());
			//如果空间扩容，那么原先的pos的迭代器指向的将是一片已释放空间,需记录相对位置
			pos = _start + len;
		}
		iterator it;
		for (it = _finish - 1; it != pos; it--)
		{
			*(it + 1) = *(it);
		}
		++_finish;
		*pos = x;
		return pos;
	}

	iterator erase(iterator pos)
	{
		assert(pos >= _start);
		assert(pos <= _finish);
		iterator it = pos + 1;
		while (it < _finish)
		{
			*(it - 1) = *(it);
			++it;
		}
		//erase会挪动数据，导致迭代器指向位置意义不明。   
		--_finish;
		return pos;
	}

	const T& operator[](size_t i)const
	{
		assert(i < size());
		return _start[i];
	}

	iterator begin()
	{
		return _start;
	}

	iterator end()
	{
		return _finish;
	}

	const_iterator begin()const
	{
		return _start;
	}

	const_iterator end()const
	{
		return _finish;
	}





	//
	//
	//
	//
private:
	iterator _start;
	iterator _finish;
	iterator _endofstorage;
};

#endif
