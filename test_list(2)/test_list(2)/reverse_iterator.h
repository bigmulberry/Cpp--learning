#pragma once

namespace my_std
{
	template<class Iterator,class Ref,class Ptr>
	class reverse_iterator
	{
	public:
		typedef reverse_iterator<Iterator,Ref,Ptr> self;
		//构造
		reverse_iterator(Iterator it) :current(it)
		{}


		//解引用
		Ref operator*()
		{
			Iterator temp(current);
			return *(--temp);
		}

		Ptr operator->()
		{
			return &operator*();
		}

		self& operator++()
		{
			--current; 
			return *this;
		}

		self& operator--()
		{
			++current;
			return *this;
		}

		bool operator==(const self& rit)
		{
			return  current == rit.current;
		}

		bool operator!=(const self& rit)
		{
			return current != rit.current;
		}

	private:
		Iterator current;
	};
}
