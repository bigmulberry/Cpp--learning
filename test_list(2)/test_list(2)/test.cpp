#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include <list>
#include <assert.h>
#include "mylist.h"
#include "myvector.h"
//int main()
//{
//	int a[] = { 1,2,2,3 };
//	list<int> la(a,a+sizeof(a)/sizeof(int));
//	list<int> la2(a,a+sizeof(a)/sizeof(int));
//	list<int> la3(a,a+sizeof(a)/sizeof(int));
//
//	//la.unique();
//
//	//la.remove(3);
//	for (auto e : la)
//	{
//		cout << e << ' ';
//	}cout << endl;
//
//	int b[] = { 10,20,30 };
//	list<int> lb(b, b + sizeof(b) / sizeof(int));
//	list<int> lc(lb);
//	list<int> ld(lb);
//	auto it = lb.begin();
//	it++;//it指向20
//	lb.splice(it, la);
//	for (auto e : lb)
//	{
//		cout << e << ' ';
//	}cout << endl;
//
//	lc.splice(lc.begin(), la2, la2.begin());
//	for (auto e : lc)
//	{
//		cout << e << ' ';
//	}cout << endl;
//
//	return 0;
//}

struct Date
{

	int _year;
	int _month;
	int _day;

	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}
};


void Print_mylist(const mylist<int>& l)
{
	mylist<int>::const_iterator it = l.begin();//当我们希望用迭代器去遍历一个const容器的时候，首先这个迭代器不能用const修饰，否则它便不能移动，其次迭代器不能对容器变量做修改
	
	while (it != l.end())
	{
		//(*it) *= 2;
		//const_iterator的const是用来限制修改迭代器所指代的对象
		cout << (*it) << ' ';
		++it;
	}
	cout << "done" << endl;
}

int main()
{
	//mylist<int> l1;
	//l1.push_back(1);
	//l1.push_back(2);
	//l1.push_back(3);
	//l1.push_back(4);

	//mylist<int>::iterator  it = l1.begin();
	//while (it != l1.end())
	//{
	//	(*it) *= 2;
	//	cout << (*it) << ' ';
	//	++it;
	//}cout << endl;
	//Print_mylist(l1);

	//mylist<Date> lt;
	//lt.push_back(Date(2022, 3, 12));
	//lt.push_back(Date(2022, 3, 13));
	//lt.push_back(Date(2022, 3, 14));
	//mylist<Date>::iterator itd = lt.begin();
	//while (itd !=lt.end())
	//{
	//	//cout << (*itd)._year << "/" << (*itd)._month << "/" << (*itd)._day << endl;
	//	cout << itd->_year << "/" << itd->_month << "/" << itd->_day << endl;

	//	++itd;
	//}
	//cout << endl;

	//l1.insert(l1.begin(), 5);
	//l1.insert(l1.end(), 5);
	//Print_mylist(l1);

	//l1.push_front(4);
	//l1.push_back(4);
	//Print_mylist(l1);
	//l1.erase(l1.begin());

	//Print_mylist(l1);


	////***************************************************
	//

	//mylist<int> l2(l1);
	//Print_mylist(l2);

	//mylist<int> l3;
	//l3 = l1;
	//Print_mylist(l3);

	////int a[10] = { 9,8,7,6,5,4,3,2,1,0 };

	////mylist<int> l4(a,a+10);


	//mylist<int> l4(5, 10);
	//Print_mylist(l4);

	//测试反向迭代器

	//mylist<int> l5;
	//l5.push_back(1);
	//l5.push_back(2);
	//l5.push_back(3);
	//l5.push_back(4);

	//mylist<int>::reverse_iterator rit = l5.rbegin();
	//while (rit != l5.rend())
	//{
	//	cout << (*rit) << ' ';
	//	++rit;
	//}

	myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	myvector<int>::reverse_iterator rit = v.rbegin();
	while (rit != v.rend())
	{
		cout << (*rit) << ' ';
		++rit;
	}

	return 0;
}