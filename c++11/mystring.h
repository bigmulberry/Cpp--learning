#pragma once
#pragma once


class mystring
{
public:

	typedef char* iterator;
	typedef const char* const_iterator;


	//构造函数
	//***************************************************************************************************
	mystring(const char* str = "") : _size(strlen(str)), _capacity(_size)
	{
		//开辟空间
		_str = new char[_capacity + 1];
		strcpy(_str, str);
	}

	//或者将缺省构造函数拆分：默认的构造函数和普通带参的构造函数

	//***************************************************************************************************
	//拷贝构造

	//传统写法
	//mystring(const mystring& s):_size(s._size),_capacity(s._capacity)
	//{
	//	//开辟空间
	//	_str = new char[_capacity];
	//	strcpy(_str, s._str);
	//}

	//使用库的swap函数，还是会产生多余空间，所以可以自己封装一个swap的函数来针对处理mystring类
	void swap(mystring& s2)
	{
		std::swap(_str, s2._str);
		std::swap(_size, s2._size);
		std::swap(_capacity, s2._capacity);
	}
	//拷贝构造现代写法
	mystring(const mystring& s) :_str(nullptr), _size(0), _capacity(0)
	{
		//构造生成临时对象
		cout << "mystring(const mystring& s) -- 深拷贝" << endl;
		mystring temp = mystring(s._str);
		//将临时对象的成员与*this互换
		//使用自己的swap
		swap(temp);
	}

	//***************************************************************************************************
	//赋值重载

	//传统毛糙写法
	//mystring& operator=(const mystring& s)
	//{
	//	//把原先的内容清空
	//	delete[] _str;
	//	//开辟一个与源一样大的空间
	//	_str = new char[s._capacity + 1];
	//	//字符串拷贝
	//	strcpy(_str, s._str);
	//	_size = s._size;
	//	_str = s._str;
	//	return *this;
	//}

	//缺点
	//1.如果误操作自己给自己赋值，那就会提前将自己删掉。
	//2.如果new失败，因为提前delete的缘故，将丢失原本的值

	//传统精细写法
	//mystring& operator=(const mystring& s)
	//{
	//	if (this != &s)
	//	{
	//		char* temp = new char[s._capacity + 1];//保证new不会失败，再delete自身
	//		strcpy(temp, s._str);
	//		delete[] _str;
	//		_str = temp;
	//		_size = s._size;
	//		_capacity = s._capacity;
	//	}

	//	return *this;
	//}

	//现代写法1
	mystring& operator=(const mystring& s)
	{
		//依旧是参考拷贝的现代写法
		//复用拷贝构造，产生临时对象，然后把*this和临时对象互换
		cout << "mystring operator=(const mystring& s) -- 深拷贝" << endl;
		mystring temp(s);
		swap(temp);

		return *this;
	}

	//现代写法2
	//mystring& operator=(mystring s)
	//{
	//	//依旧是参考拷贝的现代写法,但是利用的传值的拷贝构造
	//	// 由于传值了，必不可能出现自己给自己赋值的情况
	//	//然后把*this和临时对象互换
	//
	//	swap(s);

	//	return *this;
	//}

	//移动构造
	mystring(mystring&& s) :_str(nullptr), _size(0), _capacity(0)
	{
		cout << "mystring(mystring&& s) -- 移动语义" << endl;
		swap(s);
	}

	//移动赋值
	mystring& operator=(mystring&& s)
	{
		cout << "mystring& operator=(mystring&& s) -- 移动语义" << endl;
		swap(s);

		return *this;
	}

	//***************************************************************************************************
	//析构函数
	~mystring()
	{
		delete[] _str;
		_str = nullptr;
		_capacity = 0;
		_size = 0;
	}


	//***************************************************************************************************
	//遍历
	size_t size()const
	{
		return _size;
	}

	size_t capacity()const
	{
		return _capacity;
	}

	char& operator[](size_t pos)
	{
		//assert(pos < _size);
		return _str[pos];
	}

	//注意：如果*this是const mystring ，首先保证用const修饰*this，即无法在函数中用this指针，修改源对象，返回值的引用也需要用const来修饰
	const char& operator[](size_t pos)const
	{
		assert(pos < _size);
		return _str[pos];
	}


	char* c_str()
	{
		return _str;
	}

	const char* c_str()const
	{
		return _str;
	}

	//***************************************************************************************************
	//迭代器
	iterator begin()
	{
		return _str;
	}

	const_iterator begin()const
	{
		return _str;
	}

	iterator end()
	{
		return _str + _size;
	}

	const_iterator end()const
	{
		return _str + _size;
	}

	//容量相关
	//***************************************************************************************************
	// reserve ——扩容
	void reserve(size_t n)
	{
		//只有当n>_capacity 才会扩容
		if (n > _capacity)
		{
			char* temp = new char[n + 1];
			strncpy(temp, _str, _size + 1);
			delete[] _str;
			_str = temp;
			_capacity = n;
		}
	}

	//resize   
	void resize(size_t n, char c = '\0')
	{
		if (n < _size)
		{
			_str[n] = '\0';
			_size = n;
		}

		else
		{
			reserve(n);
			memset(_str + _size, c, sizeof(char) * (n - _size));
			_size = n;
			_str[_size] = '\0';
		}
	}

	//void resize(size_t n, char ch = '\0')
	//{
	//	if (n > _size)
	//	{
	//		reserve(n);
	//		memset(_str + _size, ch, n - _size);
	//	}
	//	_size = n;
	//	_str[n] = '\0';
	//}

	//empty
	bool empty()
	{
		return _size == 0;
	}

	//clear
	//void clear()
	//{
	//	_str[0] = '\0';
	//	_size = 0;
	//}

	//修改字符串
	//***************************************************************************************************
	//push_back
	void push_back(char ch)
	{
		//检查扩容
		if (_size == _capacity)
		{
			reserve(_capacity == 0 ? 4 : 2 * _capacity);
		}
		_str[_size] = ch;
		_size++;
		_str[_size] = '\0';
	}

	//append 的扩容 需要根据插入字符串的大小考虑扩容的大小
	void append(const char* s)
	{
		size_t len = strlen(s);
		if (len + _size > _capacity)
		{
			reserve(_capacity == 0 ? len : len + _size);
		}
		strcpy(_str + _size, s);
		_size += len;
	}

	//+=
	mystring& operator+=(char ch)
	{
		push_back(ch);
		return *this;
	}

	mystring& operator+=(const char* s)
	{
		append(s);
		return *this;
	}

	//+
	mystring operator+(char ch)
	{
		mystring tmp(*this);
		tmp.push_back(ch);
		return tmp;
	}





	size_t find(const char ch, size_t pos = 0)const
	{
		while (pos < _size)
		{
			if (_str[pos] == ch)
			{
				return pos;
			}
			pos++;
		}
		return npos;
	}

	size_t find(const char* s, size_t pos = 0)const
	{
		const char* ret = strstr(_str + pos, s);
		if (ret)
		{
			return ret - _str;
		}
		else
		{
			return npos;
		}
	}

	size_t rfind(const char ch, size_t pos = npos)const
	{
		mystring temp(*this);
		reverse(temp.begin(), temp.end());
		if (pos >= _size)
		{
			pos = _size - 1;
		}
		size_t rpos = _size - pos - 1;
		//*this从pos往前找，temp应从rpos往后找
		size_t ret = temp.find(ch, rpos);
		if (ret == npos)
		{
			return npos;
		}
		else
		{
			return _size - ret - 1;
		}

	}

	//这里使用逆置也可以，不过还需另辟空间将子串逆置，然后在逆置的母串中查找
	//不过我们再换种方法，顺着找到最后一个匹配字串即可
	size_t rfind(const char* s, size_t pos = npos)const
	{
		size_t locate = find(s);//此时locate有两种情况1.npos 2.第一个位置的下标（可能在pos的前后）
		size_t ret = locate;
		while (locate < pos)
		{
			ret = locate;
			locate = find(s, ret + 1);
		}
		if (ret <= pos)
		{
			return ret;
		}
		else
		{
			return npos;
		}
	}

	mystring& insert(size_t pos, char ch)
	{
		assert(pos <= _size);//_str[_size]='\0'
		//考虑扩容
		if (_size == _capacity)
		{
			reserve(_capacity = 0 ? 4 : 2 * _capacity);
		}
		if (pos == _size)
		{
			push_back(ch);
		}
		else
		{
			memmove(_str + pos + 1, _str + pos, _size - pos + 1);
			_str[pos] = ch;
			_size += 1;
		}
		return *this;
	}

	mystring& insert(size_t pos, const char* s)
	{
		assert(pos <= _size);
		int len = strlen(s);
		//考虑扩容
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		_size += len;
		memmove(_str + pos + len, _str + pos, _size - pos + 1);
		strncpy(_str + pos, s, len);
		return *this;

	}

	mystring& erase(size_t pos = 0, size_t len = npos)
	{
		assert(pos < _size);
		if (len == npos || pos + len > _size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			memmove(_str + pos, _str + pos + len, _size - pos - len + 1);
			_size -= len;
		}
		resize(_size);
		return *this;
	}

	void clear()
	{
		resize(0);
	}


private:
	char* _str;
	size_t _size;
	size_t _capacity;
	static const size_t npos;
};
const size_t mystring::npos = -1;


bool operator<(const mystring& s1, const mystring& s2)
{
	return strcmp(s1.c_str(), s2.c_str()) < 0;
}

bool operator==(const mystring& s1, const mystring& s2)
{
	return strcmp(s1.c_str(), s2.c_str()) == 0;
}

bool operator>(const mystring& s1, const mystring& s2)
{
	return strcmp(s1.c_str(), s2.c_str()) > 0;
}

bool operator>=(const mystring& s1, const mystring& s2)
{
	return !(s1 < s2);
}

bool operator<=(const mystring& s1, const mystring& s2)
{
	return !(s1 > s2);
}

istream& operator>>(istream& in, mystring& s)
{
	//先清空字符串原本的内容
	s.clear();
	char ch;
	ch = in.get();
	while (ch != ' ' && ch != '\n')
	{
		s += ch;
		ch = in.get();
	}
	return in;
}

istream& getline(istream& in, mystring& s)
{
	//先清空字符串原本的内容
	s.clear();
	char ch;
	ch = in.get();
	while (ch != '\n')
	{
		s += ch;
		ch = in.get();
	}
	return in;
}

ostream& operator<<(ostream& out, const mystring& s)
{
	//out << s.c_str();//这种写法遇到当中有\0的字符串就嗝屁了

	for (auto ch : s)
	{
		out << ch;
	}
	return out;
}

//整型转字符串
mystring to_mystring(int value)
{
	bool flag = true;
	if (value < 0)
	{
		flag = false;
		value = 0 - value;
	}
	mystring str;
	while (value > 0)
	{
		int x = value % 10;
		value /= 10;
		str += ('0' + x);
	}
	if (flag == false)
	{
		str += '-';
	}
	std::reverse(str.begin(), str.end());
	return str;
}