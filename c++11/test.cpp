#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
#include <string>
#include <cstring>
#include <list>
#include <vector>
#include <map>
#include <algorithm>
#include <thread>
#include <mutex>
#include<assert.h>
#include "mystring.h"


//int main()
//{
//	int rr1 = 10;
//	int&& rr2 = std::move(rr1);
//	int& rr3 = rr1;
//	rr1 *= 2;
//
//	return 0;
//}




//class Date
//{
//public:
//	 Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//class A
//{
//private:
//	int _a;
//public:
//	explicit A(int a) :_a(a)
//	{
//
//	}
//};
//
//template<class T>
//class vector
//{
//public:
//	typedef T* iterator;
//
//	vector(initializer_list<T> l) 
//	{
//		_start = new T[l.size()];
//		_finish = _start + l.size();
//		_endofstorage = _start + l.size();
//
//		iterator vit = _start;
//		for (const auto& e : l)
//		{
//			*vit++ = e;
//		}
//	}
//
//	vector<T>& operator=(initializer_list<T> l)
//	{
//		vector<T> tmp(l);
//		std::swap(_start, tmp._start);
//		std::swap(_finish, tmp._finish);
//		std::swap(_endofstorage, tmp._endofstorage);
//
//		return *this;
//	}
//
//private:
//	iterator _start;
//	iterator _finish;
//	iterator _endofstorage;
//};


//template<class T1, class T2>
//void F(T1 t1, T2 t2)
//{
//	decltype(t1 * t2) ret;
//	cout << typeid(ret).name() << endl;
//}
//
//int f(int a)
//{
//	return a;
//}

//int main()
//{

	//	int a = 10;
	//	int b = { 10 };
	//	int c{ 10 };
	//
	//	int* ptr = new int[5]{ 0 };
	//
	//	list<string> authors{ "authors","Shakespeare","Austen"};
	//	vector<const char*> articles{ "a","an","the" };
	//

	//Date d1(2022, 1, 1); // old style

	//// C++11支持的列表初始化，这里会调用构造函数初始化
	////多参数的构造函数，支持隐式类型转换
	//Date d2{ 2022, 1, 2 };
	//Date d3 = { 2022, 1, 3 };

	//Date* dd = new Date[2]{ {2022,1,4},{2022,1,5} };


	//map<string, string> dict1 { {"apple","苹果"},{"banana","香蕉"} };
	//map<string, string> dict2 { {"orange","橘子"},{"peach","桃子"}};


	// the type of il is an initializer_list
	//auto il = { 10, 20, 30 };
	//cout << typeid(il).name() << endl;

	//auto pf = strcpy;
	//cout << typeid(pf).name() << endl;

	//int i = 10;
	//const int ci = i;
	//auto a = ci;//a是一个int（顶层const 被忽略）
	//auto b = &i;//b是一个整型指针
	//auto c = &ci;//c是一个指向整数常量的指针（对常量独享取地址是一种底层const）

	//*b = 20;//正确
	////*c = 20;//错误：表达式必须是可修改的左值

	//auto& e = ci;

	//const int x = 1;
	//double y = 2.2;

	//decltype(x * y) ret; // ret的类型是double
	//decltype(&x) p; // p的类型是int const*
	//cout << typeid(ret).name() << endl;
	//cout << typeid(p).name() << endl;

	//F(1, 'a');



	//int i = 42;
	////decltype(i) a;
	////decltype((i)) b;

	//decltype(&f) a;
	//cout << typeid(a).name() << endl;
	//
	//int& li = ++i;
	//int&& ri1 = i++;
	//int&& ri2 = 9 ;
	//ri2 = 10; 

//	int rr1 = 42;
//	int&& r = move(rr1);
//	cout << rr1 << endl;
//
//	const int t = 12;
//	const int&& rt = std::move(t);//常量右值需由常量右值引用绑定
//
//	return 0;
//}



//完美转发

//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const左值引用" << endl; }
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const右值引用" << endl; }
//
////std::forward<T>(t)在传参的过程中保持了t的原生类型属性
//template<class T>
//void PerfectForward(T&& t)
//{
//	Fun(std::forward<T>(t));
//}
//
//int main()
//{
//	PerfectForward(10);             //右值
//
//	int a=10;
//	PerfectForward(a);              //左值
//	PerfectForward(std::move(a));   //右值
//
//	const int b = 20;
//	PerfectForward(b);              //const 左值
//	PerfectForward(std::move(b));   //const 右值
//
//	return 0;
//}


//完美转发场景
//template<class T>
//struct ListNode
//{
//	ListNode* _next = nullptr;
//	ListNode* _prev = nullptr;
//	T _data;
//};
//template<class T>
//class List
//{
//	typedef ListNode<T> Node;
//public:
//	List()
//	{
//		_head = new Node;
//		_head->_next = _head;
//		_head->_prev = _head;
//	}
//	void PushBack(T&& x)
//	{
//		//Insert(_head, x);
//		Insert(_head, std::forward<T>(x));
//	}
//
//	void Insert(Node* pos, T&& x)//右值引用
//	{
//		cout << "右值Insert" << endl;
//		Node* prev = pos->_prev;
//		//法一：移动赋值
//		//Node* newnode = new Node;
//
//		//newnode->_data = x;                    // 关键位置: x退化左值属性则调用了T类型的普通赋值重载函数
//
//		//newnode->_data = std::forward<T>(x); // 关键位置: x保存右值属性则调用了T类型的移动赋值重载函数
//
//		//法二：移动构造
//		//定位new，调用移动构造
//		Node* newnode = (Node*)malloc(sizeof(Node));
//		
//		//new(&newnode->_data)T(x);                      //传递的是左值，将调用拷贝构造
//
//		new(&newnode->_data)T(std::forward<T>(x));       //传递的是右值，将调用移动构造
//
//		// prev newnode pos
//		prev->_next = newnode;
//		newnode->_prev = prev;
//		newnode->_next = pos;
//		pos->_prev = newnode;
//	}
//	void Insert(Node* pos, const T& x)//左值引用
//	{
//		cout << "左值Insert" << endl;
//		Node* prev = pos->_prev;
//		Node* newnode = new Node;
//		newnode->_data = x; // 关键位置
//
//		// prev newnode pos
//		prev->_next = newnode;
//		newnode->_prev = prev;
//		newnode->_next = pos;
//		pos->_prev = newnode;
//	}
//private:
//	Node* _head;
//};
//int main()
//{
//	List<mystring> lt;
//	lt.PushBack("1111");
//
//	return 0;
//}


//lambda

//int main()
//{
//	int a = 2, b = 1;
//	auto swap = [&a, &b]()mutable {int z = a; a = b; b = z; };
//	swap();
//
//	return 0;
//}



struct Goods
{
	string _name; // 名字
	double _price; // 价格
	int _evaluate; // 评价
	Goods(const char* name,double price,int evaluate):_name(name),_price(price),_evaluate(evaluate)
	{}
};

struct ComparePriceLess
{
	bool operator()(const Goods& gl, const Goods& gr)
	{
		return gl._price < gr._price;
	}
};

struct ComparePriceGreater
{
	bool operator()(const Goods& gl, const Goods& gr)
	{
		return gl._price > gr._price;
	}
};

//int main()
//{
//	std::vector<Goods> v = { { "可乐", 3.1, 5}, { "乌龙茶", 3, 4}, { "雪碧", 2.8,3}, {"矿泉水", 1.5, 4} };
//	//sort(v.begin(), v.end(), ComparePriceLess());
//	//sort(v.begin(), v.end(), ComparePriceGreater());
//	sort(v.begin(), v.end(), [](const Goods& l, const Goods& r)->bool {return l._price < r._price; });
//
//	return 0;

	// 每增加一个比较方式，就要提供一个对应的仿函数
//
//	//int a = 2, b = 3;
//	////省略参数列表和返回值类型，返回值类型由编译器推导为int
//	//auto add1 = [=] {return a + b; };
//	//cout << add1() << endl<<endl;
//
//	////省略返回值类型，无返回值类型
//	//auto add2 = [&](int c) {a = b + c; };
//	//add2(10);
//	//cout << a << " " << b << endl << endl;//a=13,b=3
//
//	////各部分很完善的lambda表达式
//	//auto add3 = [=, &b](int c)->int {return b += a + c; };
//	//cout << add3(10) << endl << endl;//a=13,b=26
//
//
//	// 复制捕捉x
//	int x = 10;
//	auto add_x = [x](int y)mutable{ x++; return  x; };
//
//	cout << add_x(10) << endl;//捕获值x=11
//	cout << x << endl<<endl;
//
//	cout << add_x(10) << endl;//捕获值x=12
//	cout << x << endl;
//
//	return 0;
//}




//void (*PF)();
//int main()
//{
//	auto f1 = [] {cout << "hello world" << endl; };
//	auto f2 = [] {cout << "hello world" << endl; };
//	// 此处先不解释原因，等解释lambda表达式底层实现原理后就清楚了
//	//f1 = f2; // 编译失败--->提示找不到operator=()
//	// 允许使用一个lambda表达式拷贝构造一个新的副本
//	auto f3(f2);
//	f3();
//	// 可以将lambda表达式赋值给相同类型的函数指针
//	PF = f2;
//	PF();
//	return 0;
//}




//class Rate
//{
//public:
//	Rate(double rate) : _rate(rate)
//	{}
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//private:
//	double _rate;
//};
//int main()
//{
//	// 仿函数类对象
//	double rate = 0.02;
//	Rate r1(rate);
//	r1(10000, 2);
//
//	// lambda
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year;
//	};
//	r2(10000, 2);
//
//	cout << typeid(r2).name() << endl;
//	return 0;
//}


//int main()
//{
//	int a = 1, b = 2;
//	auto swap = [](int& x, int& y) {int z = x; x = y; y = z; };
//
//	cout << typeid(swap).name() << endl;
//}


//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p)
//		:_name(p._name)
//		, _age(p._age)
//	{}
//	
//	//Person(Person&& p) = default;
//
//
//	//Person(Person& p) = delete;
//	
//
//
//private:
//	mystring _name;
//	int _age;
//};
//
//int main()
//{
//	Person s1;
//	Person s2 = s1;
//	Person s3 = std::move(s1);
//	return 0;
//}


//递归展开参数包
//template<typename T>
//void Showlist(const T& t)
//{
//	cout << t << endl<<endl;
//}
//
//template<typename T, typename... Args>
//void Showlist(const T& t, const Args&... rest)
//{
//	cout << t << endl;
//	Showlist(rest...);
//}
//
//int main()
//{
//	int i = 0; double d = 3.14; string s = "hello world";
//	Showlist(i, s, 42, d); 
//	Showlist(d, s);     
//	Showlist("hi");
//	return 0;
//}




//逗号表达式展开参数包
//template<typename T>
//int printarg(T val)
//{
//	cout << typeid(T).name() << ":" << val << endl;
//	return 0;
//}
//
//template< typename... Args>
//void Showlist(const Args&... rest)
//{
//	int arr[] = { printarg(rest)... };
//	cout << endl;
//}
//
//int main()
//{
//	int i = 0; double d = 3.14; string s = "hello world";
//	Showlist(i, s, 42, d);
//	Showlist(d, s);      
//	Showlist("hi");     
//	return 0;
//}

//int main()
//{
//	vector<mystring> v;
//	v.push_back("2222");
//	v.emplace_back("123");
//	return 0;
//}





//线程库
//#include<iostream>
//#include<thread>
//
//void Fn()
//{
//	//do stuff
//}
//
//int main()
//{
//	std::thread foo;
//	std::thread bar(Fn);
//
//	std::cout << "Joinable after construction" <<std::boolalpha <<endl;
//	std::cout << "foo: " << foo.joinable() << endl;
//	std::cout << "bar: " << bar.joinable() << endl;
//
//	if (foo.joinable()) foo.join();
//	if (bar.joinable()) bar.join();
//
//	std::cout << "Joinable after joining" <<endl;
//	std::cout << "foo: " << foo.joinable() << '\n';
//	std::cout << "bar: " << bar.joinable() << '\n';
//
//	return 0;
//}


//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	// 函数名
//	cout << useF<double(*)(double),double>(f, 11.11) << endl<<endl;
//	// 函数对象
//	cout << useF(Functor(), 11.11) << endl<<endl;
//	// lamber表达式
//	cout << useF([](double d)->double { return d / 4; }, 11.11) << endl<<endl;
//	return 0;
//}




#include <functional>
//
//int f(int a, int b)
//{
//	return a + b;
//}
//
//struct Functor
//{
//	int operator()(int a, int b)
//	{
//		return a * b;
//	}
//};
//
//class Plus
//{
//public:
//	static int Plusi(int a, int b)
//	{
//		return a + b;
//	}
//
//	double Plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	//函数名（函数指针）
//	cout << "包装函数指针" << endl;
//	//std::function<int(int, int)> func1 = &f;
//	std::function<int(int, int)> func1 = f;
//	cout << func1(1, 2) << endl;
//
//	//仿函数类对象
//	cout << "包装仿函数对象" << endl;
//	std::function<int(int, int)> func2 = Functor();
//	cout << func2(1, 2) << endl;
//
//	//成员函数
//	cout << "包装成员函数" << endl;
//	//静态成员函数与普通函数一致
//	//std::function<int(int, int)> func3 = &Plus::Plusi;
//	std::function<int(int, int)> func3 = Plus::Plusi;
//	cout << func3(1, 2) << endl;
//	//普通成员函数需在参数前加上类名（非静态的成员函数需要由对象去调用），以及取成员函数时需要加上 & 符号
//	std::function<double(Plus,double,double)> func4 = &Plus::Plusd;
//	cout << func4(Plus(),1.1, 2.2) << endl;
//
//	//lambda表达式
//	cout << "包装lambda表达式" << endl;
//	std::function<int(int, int)> func5 = [](int a, int b)->int {return a + b; };
//	cout << func5(1, 2) << endl;
//
//	return 0;
//}


//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	// 函数名
//	std::function<double(double)> func1 = f;
//	cout << useF(func1, 11.11) << endl<<endl;
//
//	// 函数对象
//	std::function<double(double)> func2 = Functor();
//	cout << useF(func2, 11.11) << endl<<endl;
//	
//	// lamber表达式
//	std::function<double(double)> func3 = [](double d)->double { return d / 4; };
//	cout << useF(func3, 11.11) << endl<<endl;
//	
//	map<int, int> m = { {1,1},{2,3} };
//	return 0;
//}




//int f(int a, int b, int c)
//{
//	return a + b - c;
//}
//
//int main()
//{
//	//使用std::function接收返回值
//	//绑位置
//	std::function<int(int, int, int)> func1 = bind(f,placeholders::_3,placeholders::_2,placeholders::_1);
//	auto func2 = bind(f,placeholders::_3,placeholders::_2,placeholders::_1);
//	//绑定指定实参
//	std::function<int()> func3 = bind(f,5,6,7);
//	//混合
//	auto func4= bind(f, placeholders::_1,  placeholders::_2,7);
//	//已绑定的值将不会改变
//	std::function<int(int,int,int)>  func5= bind(f, placeholders::_1, 6, placeholders::_3);
//
//	cout << f(5, 6, 7) << endl;
//	cout << func1(7, 6, 5) << endl;
//	cout << func2(7, 6, 5) << endl;
//	cout << func3() << endl;
//	cout << func4(5,6) << endl;
//	cout << func5(5,10,7) << endl;
//
//	return 0;
//}


//class Sub
//{
//public:
//	int sub(int a,int b)
//	{
//		return a - b;
//	}
//};
//
//int main()
//{
//	//绑定成员函数
//	std::function<int(Sub,int, int)> func1 = &Sub::sub;
//	cout << func1(Sub(), 2, 1)<<endl;
//
//	//更改绑定参数数量，将类对象绑死。
//	std::function<int(int, int)> func2 = bind(&Sub::sub,Sub(),placeholders::_1,placeholders::_2);
//	cout << func2( 2, 1) << endl;
//
//	std::function<int(int)> func3 = bind(&Sub::sub, Sub(), 100, placeholders::_1);
//	cout << func3(2) << endl;
//
//	return 0;
//}

//
//int x = 0;
//void Func(int n)
//{
//	for (int i = 0; i < n; ++i)
//	{
//		++x;
//	}
//	cout << std::this_thread::get_id() << endl;
//}
//
//int main()
//{
//	thread t1(Func,1000000);
//	thread t2(Func,2000000);
//	t1.join();
//	t2.join();
//
//	cout << x << endl;
//
//	return 0;
//}




//std::thread::id main_thread_id=std::this_thread::get_id();
//
//void is_main_thread()
//{
//	if (main_thread_id == std::this_thread::get_id())
//	{
//		cout << "This is the main thread." << endl;
//	}
//	else
//	{
//		cout << "This is not the main thread." << endl;
//	}
//}
//
//int main()
//{
//	is_main_thread();
//	thread t(is_main_thread);
//	t.join();
//	return 0;
//}





//线程函数的参数 ref
// 
//void ThreadFunc1(int& x)
//{
//	x += 10;
//}
//void ThreadFunc2(int* x)
//{
//	*x += 10;
//}
//int main()
//{
//	int a = 10;
//
//	// 在线程函数中对a修改，不会影响外部实参，
//	//因为：线程函数参数虽然是引用方式，但其实际引用的是线程栈中的拷贝
//	//thread t1 构造的时候不知道 ThreadFunc1 的参数是引用的，thread只会盲目地复制 a 的值，
//	//而这个复制出来的值是const的类型，这与 ThreadFunc1 需要的参数类型不匹配，因为 ThreadFunc1 需要的是non - const的引用，
//	//因此报错——错误 C2672 “std::invoke”: 未找到匹配的重载函数
//	
//	//thread t1(ThreadFunc1, a);
//	//t1.join();
//	//cout << a << endl;
//	 
//	
//	// 如果想要通过形参改变外部实参时，必须借助std::ref()函数
//	thread t2(ThreadFunc1, std::ref(a));
//	t2.join();
//	cout << a << endl;
//	// 或者地址的拷贝
//	thread t3(ThreadFunc2, &a);
//	t3.join();
//	cout << a << endl;
//	return 0;
//}

// 互斥量

//volatile int counter(0);
//std::mutex mtx;
//void add_to_1000()
//{
//	for (int i = 0; i < 1000; ++i)
//	{
//		//if (mtx.try_lock())
//		//{
//		//	counter++;
//		//	mtx.unlock();
//		//}
//
//		mtx.lock();
//		counter++;
//		mtx.unlock();
//		
//	}
//}
//
//int main()
//{
//	std::thread threads[10];
//	// spawn 10 threads:
//	for (int i = 0; i < 10; ++i)
//		threads[i] = std::thread(add_to_1000);
//
//	for (auto& th : threads) th.join();
//	std::cout << counter << " successful increases of the counter.\n";
//
//	return 0;
//}


//原子操作
//#include <time.h>
//int main()
//{
//	atomic<int> x = 0;
//	int N = 5, M = 1000;
//	vector<thread> vthds(N);
//	for (auto& e : vthds)
//	{
//		e = thread([M,&x] {
//			for (int i = 0; i < M; ++i)
//			{
//				cout <<"id-> " <<std::this_thread::get_id()<<"-> " << x << endl;
//				++x;
//			}
//			});
//	}
//	for (auto& e : vthds)
//	{
//		e.join();
//	}
//	cout << x <<endl;
//	
//}

//atomic<bool> ready = false;
//void count1m(int id)
//{
//	while (!ready)
//	{
//		std::this_thread::yield();
//	}
//	for (volatile int i = 0; i < 1000000; ++i) {}
//	cout << id<<' ';
//}
//int main()
//{
//	vector<thread> vthds(10);
//	cout << "race of 10 threads that count to 1 million:\n";
//	for(int i=0;i<10;++i)
//	{
//		vthds[i] = thread(count1m, i);
//	}
//	ready = true;
//	for (auto& e : vthds)
//	{
//		e.join();
//	}
//	return 0;
//}

//#include <chrono>         // std::chrono::seconds
//
//int main()
//{
//	std::cout << "countdown:\n";
//	for (int i = 10; i > 0; --i) {
//		std::cout << i << '\n';
//		std::this_thread::sleep_for(std::chrono::seconds(1));
//	}
//	std::cout << "Lift off!\n";
//
//	return 0;
//}


//std::recursive_mutex rtx;
//int g_val = 5;
//void g()
//{
//	rtx.lock();
//	cout << "g_val:"<<g_val-- << endl;
//	if(g_val>0)
//		g();
//	rtx.unlock();
//}
//
//int main()
//{
//	thread t1(g);
//	t1.join();
//	return 0;
//}


//std::timed_mutex ttx;
//
//void fireworks()
//{
//	//每等待200ms加锁，如果返回false，打印 '-'
//	while (!ttx.try_lock_for(std::chrono::milliseconds(200)))
//	{
//		cout << '-';
//	}
//	//成功加锁，等待1s，随后打印 '*'
//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//	std::cout << "*\n";
//	ttx.unlock();
//}
//
//int main()
//{
//	std::thread threads[10];
//	// spawn 10 threads:
//	for (int i = 0; i < 10; ++i)
//		threads[i] = std::thread(fireworks);
//
//	for (auto& th : threads) th.join();
//
//	return 0;
//}

//template <class Lock>
//class LOCKGUARD
//{
//public:
//	LOCKGUARD(Lock& lock):_lock(lock)
//	{
//		_lock.lock();
//	}
//
//	~LOCKGUARD()
//	{
//		_lock.unlock();
//	}
//private:
//	Lock& _lock;
//};
//
//
//void Func(vector<int>& v,int base,int N,mutex& mtx )
//{
//	//捕获异常
//	try
//	{
//		for (int i = 0; i < N; ++i)
//		{
//			cout << this_thread::get_id() << ":" << base + i << endl;
//			//mtx.lock();
//			//v.push_back(base + i);//申请空间失败会抛异常，unlock不执行，第二个线程再lock(),会死锁
//
//
//
//			lock_guard<mutex> lock(mtx,adopt_lock);
//
//			
//
//			int* p1 = new int;
//			*p1 = i;
//			//模拟临界区内抛异常
//			if (base == 1000 && i == 888)
//			{
//				throw bad_alloc(); 
//			}
//			delete p1;
//			//mtx.unlock();
//		}
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//		//mtx.unlock();
//	}
//}
//
//int main()
//{
//	thread t1;
//	thread t2;
//	vector<int> v;
//	mutex mtx;
//
//	t1 = thread(Func, std::ref(v), 1000, 1000,std::ref(mtx));
//	t2 = thread(Func, std::ref(v), 2000, 2000,std::ref(mtx));
//
//	t1.join();
//	t2.join();
//	cout << v.size() << endl;
//	return 0;
//}


//条件变量，偶数奇数交替打印
//#include <condition_variable>
//
//bool flag = false;
//int main()
//{
//	int n=100;
//	int i = 0;
//	mutex mtx;
//	condition_variable cv;
//
//	thread t1([n, &i,&mtx,&cv] {
//		while (i < n)
//		{
//			unique_lock<mutex> lock(mtx);
//			cv.wait(lock, []() {return flag; });
//			cout << this_thread::get_id() << ":" << i << endl;
//			++i;
//			flag = false;
//			cv.notify_one();
//		}
//
//	});
//
//	thread t2([n, &i,&mtx, &cv] {
//		while (i < n)
//		{
//			unique_lock<mutex> lock(mtx);
//			cv.wait(lock, []() {return !flag; });
//			cout << this_thread::get_id() << ":" << i << endl;
//			++i;
//			flag = true;
//			cv.notify_one();
//
//		}
//
//	});
//	t1.join();
//	t2.join();
//	return 0;
//}



//归并
