#pragma once
namespace call2
{
	template <typename T>
	void func(T const& v)
	{
		cout << "func2:" << v << endl;
	}
}

void caller2()
{
	call2::func(2);
	call2::func(0.2f);
}