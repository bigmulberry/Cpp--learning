#define _CRT_SECURE_NO_WARNINGS 1
#include "func.h"

template <typename T>
T const& func(T const& v)
{
	return v; 
}

template int const& func(int const& v);