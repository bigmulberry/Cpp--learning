#pragma once
namespace call1
{
	template <typename T>
	void func(T const& v)
	{
		cout << "func1:" << v << endl;
	}
}
void caller1()
{
	call1::func(1);
	call1::func(0.1);
}