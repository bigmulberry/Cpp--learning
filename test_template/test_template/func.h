#pragma once

template <typename T>
T const& func(T const& v);
