#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <assert.h>
using namespace std;

//void Swap(int a, int b)
//{
//	int temp = a;
//	a = b;
//	b = temp;
//}
//
//void Swap(double a, double b)
//{
//	double temp = a;
//	a = b;
//	b = temp;
//}


//泛型编程 ——模板
//template <typename T>
//template <class T>//定义模板参数可以用class（多），也可以用typename
//void Swap(T& x1, T& x2)
//{
//	T temp = x1;
//	x1 = x2;
//	x2 = temp;
//}
//
//int main()
//{
//	int a = 0, b = 10;
//	Swap<int>(a, b);
//
//	double c = 1.1, d = 2.2;
//	Swap<double>(c, d);
//
//	int a1 = 1;
//	double c1 = 1.1;
//	Swap<int>(a1, a);
//
//	return 0;
//}

//模板的实例化
//函数模板的推演——自己写一个模板，编译器帮助生成对应的函数 
//实参推出形参的类型T是 double int char..


//隐式实例化 与显式实例化
//template <class T>
//T Add(const T& x1, const T& x2)
//{
//	return x1 + x2;
//}
//
//int main()
//{
//	int a = 10, b = 20;
//	double c = 1.1, d = 2.2;
//
//	//隐式的实例化
//	//编译器会通过实参推演形参的类型T分别为int和double
//	Add(a, b);
//	Add(c, d);
//
//	//不让编译器推演类型，显式地指定类型
//	//这种方式称为显式实例化
//	
//	//当我们想让两种类型作为形参时会报错，因为隐式的实例化没有与参数列表匹配,编译器推演不出
//	//cout << Add(a, c) << endl;
//	//只有显式实例化
//	cout << Add<int>(a, c) << endl;
//	cout << Add<double>(a, c) << endl;
//
//	return 0;
//
//}



//一个非模板函数和模板函数同时存在，而且模板函数还能够实例化成这个非模板函数
// 专门处理int的加法函数
//int Add(int left, int right)
//{
//	return left + right;
//}
//// 通用加法函数
//template<class T>
//T Add(T left, T right)
//{
//	return left + right;
//}
//
//template <class T1,class T2>
//T1 Add(T1 left, T2 right)
//{
//	return left + right;
//}
//
//void main()
//{ 
//	Add(1, 2); // 与非模板函数匹配，编译器不需要特化  “不想绕路模板再实例化，直接走现成的”
//	Add<int>(1, 2); // 调用编译器特化的Add版本，调用模板显式实例化生成的函数
//
//
//	//对于非模板函数和同名函数模板，如果其他条件都相同，在调动时会优先调用非模板函数而不会从该模板产生出一个实例。
//	//如果模板可以产生一个具有更好匹配的函数， 那么将选择模板
//	Add(3, 4);//有完美匹配的非模板函数，不需要调用函数模板实例化
//	Add<int>(3, 4.0);//模板函数可以生成更加匹配的函数，编译器根据实参推演出更加合适的Add函数
//	//模板调用，有现成的绝不实例化模板
//	//有更匹配的，优先匹配最合适的
//}


//************************************************************************
//类模板

//案例：
//typedef int VDatatype;
//class vector
//{
//public:
//
//private:
//	VDatatype* _a;
//	int _size;
//	int _capacity;
//};
//
//int main()
//{
//	vector v1;//double
//	vector v2;//int
//	//不能同时实现double和int的vector类，但是这两个类实现几乎是一样的
//
//	return 0;
//}

//解决	——使用类模板 

//namespace sjl
//{
//	template <class T>
//	class vector
//	{
//	public:
//		vector() :_a(nullptr), _size(0), _capacity(0)
//		{
//
//		}
//
//		~vector()
//		{
//			delete[] _a;
//			_a = nullptr;
//			_size = _capacity = 0;
//		}
//
//		void push_back(const T& x)
//		{
//			if (_size == _capacity)
//			{
//				int newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
//				T* newspace = new T[newcapacity];
//				if (_a)
//				{
//					memcpy(newspace, _a, sizeof(T) * _size);
//					delete[] _a;
//				}
//				_a = newspace;
//				_capacity = newcapacity;
//			}
//			_a[_size] = x;
//			++_size;
//		}
//		
//		T& operator[](int pos)
//		{
//			assert(pos < _size);
//			return _a[pos];
//		}
//
//		int size()
//		{
//			return _size;
//		}
//	private:
//		T* _a;
//		int _size;
//		int _capacity;
//	};
//}
//
//int main()
//{
//	//类模板的类型是我们赋予的，没有办法隐式推演，只好我们显式指定 
//	sjl::vector<int> v1;//double
//	v1.push_back(1);
//	v1.push_back(2);
//	v1.push_back(3);
//
//	for (int i = 0; i<v1.size(); i++)
//	{
//		cout << v1[i] << ' ';
//	}
//	cout << endl;
//
//	std::vector<int> v2;//int
//	v2.push_back(1);
//	v2.push_back(2);
//	v2.push_back(3);
//
//
//	return 0;
//}




//****************************************************
//类模板中的函数在类外定义

//namespace sjl
//{
//	template <class T>
//	class vector
//	{
//	public:
//		vector() :_a(nullptr), _size(0), _capacity(0)
//		{
//
//		}
//
//		~vector()
//		{
//			delete[] _a;
//			_a = nullptr;
//			_size = _capacity = 0;
//		}
//
//		void push_back(const T& x)
//		{
//			if (_size == _capacity)
//			{
//				int newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
//				T* newspace = new T[newcapacity];
//				if (_a)
//				{
//					memcpy(newspace, _a, sizeof(T) * _size);
//					delete[] _a;
//				}
//				_a = newspace;
//				_capacity = newcapacity;
//			}
//			_a[_size] = x;
//			++_size;
//		}
//		
//		T& operator[](int pos);
//
//
//		int size();
//
//	private:
//		T* _a;
//		int _size;
//		int _capacity;
//	};
//
// 
// *********************************************************
//	//类外定义函数
//	//模板不支持分离编译，也就是声明在.h,定义在.cpp
//	//建议定义在一个文件里
//	template <class T>
//	T& vector<T>::operator[](int pos)
//	{
//		assert(pos < _size);
//		return _a[pos];
//	}
//
//	template <class T>
//	int vector<T>::size()
//	{
//		return _size;
//	}
//}
//
 
//***************************************************************************
// 类型模板参数默认值 
//template <typename T0=int, typename T1, typename T2=double, typename T3, typename T4>
//T2 func(T1 t1, T3 t2 , T4 t3)
//{
//	static T0 index;
//	...
//}
//
//int main()
//{
//	double x1 = func<int, double, double>(0.1, 1, 2);
//
//	int x2 = func<int, int, int>(1, 0.1, 0.2);
//
//	return 0;
//}

//分离编译
//#include "func.h"
//int main()
//{
//	std::cout<<func(0)<<std::endl;
//	return 0;
//}

//识别重复模板
//#include "caller1.h"
//#include "caller2.h"
//
//int main()
//{
//	caller1();
//	caller2();
//	return 0;
//}
























