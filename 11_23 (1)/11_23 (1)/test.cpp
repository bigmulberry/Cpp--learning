#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>



// 命名冲突问题
// 1、我们自己定义的变量、函数可能跟库里面重名冲突
// 2、进入公司项目组以后，做的项目通常比较大。多人协作，两个同事写的代码，命名冲突。
// C语言没有办法很好的解决这个问题
// CPP提出一个新语法，命名空间

// 定义了一个叫bit的命名空间 -- 域
//namespace bit
//{
//	// 他们还是全局变量，放到静态区的
//	int rand = 0;
//	int a = 1;
//}
//
//int a = 0;
//
//int main()
//{
//	printf("hello world\n");
//	printf("%d\n", rand);
//	printf("%d\n", bit::rand);
//
//	int a = 1;
//	printf("%d\n", a);
//
//	printf("%d\n", ::a);
//
//	return 0;
//}

//namespace szy
//{
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//
//	struct Node
//	{
//		struct Node* next;
//		int val;
//	};
//}
//
//namespace bit
//{
//	// 1、命名空间中可以定义变量/函数/类型
//	int rand = 10;
//
//	int Add(int left, int right)
//	{
//		return left + right + 10;
//	}
//
//	struct Node
//	{
//		struct Node* next;
//		int val;
//	};
//}
//
////2. 命名空间可以嵌套
//namespace N1
//{
//	int a;
//	int b;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//
//	namespace N2
//	{
//		int c;
//		int d;
//		int Sub(int left, int right)
//		{
//			return left - right;
//		}
//	}
//}
//
//// 
//int main()
//{
//	bit::rand = 10;
//
//	struct bit::Node node;
//	bit::Add(1, 2);
//	szy::Add(1, 2);
//
//	N1::Add(1, 2);
//	N1::N2::Sub(3, 4);
//
//	struct szy::ListNode ln;
//	szy::ListInit();
//
//	return 0;
//}

// 把整个命名空间展开 -- 展开到全局了
//using namespace szy;

// 单独展开某一个，其他不展开
//using szy::ListNode;
//
//int main()
//{
//	struct ListNode ln;
//	szy::ListInit();
//
//	printf("%d\n", szy::rand);
//
//	return 0;
//}

//#include <iostream>
//using namespace std;  // C++库的实现定义在一个叫std的命名空间中
//
//int main()
//{
//	cout << "hello world" << endl;
//
//	return 0;
//}

//#include <iostream>
////using namespace std;  // C++库的实现定义在一个叫std的命名空间中
//
//int main()
//{
//	std::cout << "hello world" << std::endl;
//
//	return 0;
//}

//#include <iostream>
////using namespace std;
//// 常用的展开
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "hello world" << endl;
//	cout << "hello world" << endl;
//	cout << "hello world" << endl;
//
//	return 0;
//}



//命名冲突问题
//1.自己定义的变量，函数与库中重名冲突
//2.项目比较大，多人协作，正好命名冲突
//c语言很难解决好上述问题
//cpp 提出新语法 —— 命名空间  namespace
// 
// 
// 
//定义一个叫bit的命名空间——域 ，定义在全局 其中的变量仍为全局变量，放在静态区，仅仅是为了隔离编译链接时的重名冲突
namespace big1
{
	//变量
	int rand = 0;
	int b = 1;
	//函数
	int Add(int a, int b)
	{
		return a + b;
	}
	//结构体
	struct Node
	{
		struct Node* next;
		int data;
	};
	//2.命名空间可以嵌套
	namespace big2
	{
		int c = 0;
	}
	//3.命名空间名字相同，在同一个工程文件下会合并
	//4.指定作用域可以最好的命名隔离，但使用不方便，
	//把整个命名空间展开到全局  using namespace bit 可以直接使用bit中的变量，不用加::  ，但隔离就失效了——慎用
	//指定展开某一个 using bit::b  可直接使用b
}

int a = 0;
using big1::b;

int main()
{
	printf("%d\n", rand);//先去局部找rand，没有再去全局找
	printf("%d\n", big1::rand);//域作用限定符，rand取左边bit域中的变量

	int a = 1;
	printf("%d\n", a);
	printf("%d\n",::a);//域为空白，默认全局域
	
	big1::Add(2, 3);
	struct big1::Node node;
	printf("%d\n", b);
	return 0;
}

