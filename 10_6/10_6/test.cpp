#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//int main()
//{
//	int a = 5;
//	int& b = a;
//	//int& c;//引用在定义时就需初始化
//	
//	int arr[5] = { 0 };
//	//int& c[5] = arr;//不允许使用引用的数组
//
//	int& c = b;//可以引用的引用
//	int* p = &c;//p存放的就是a的地址
//
//	//引用存放的是被引用方的地址，引用实际上是指针常量
//	return 0;
//}

//void swap(int& x, int& y)
//{
//	x = x ^ y;
//	y = x ^ y;
//	x = x ^ y;
//}
//
//int main()
//{
//
//	int a = 5;
//	int b = 3;
//	swap(a, b);
//	cout << "a=" << a << " " << "b=" << b << endl;
//
//	return 0;
//}

//*************************
//new
struct student
{
	string name;
	int age;
};
//
//int main()
//{
//	int* p1 = new int(2);
//	delete p1;
//
//	int* p3 = new int[3]{ 1,2,3 };
//	delete[] p3;//删除数组的格式
//
//	student* ps = new student[2]{ {"Alice",18},{"Bob",19} };
//	int i = 0;
//	for (i = 0; i < 2; i++)
//	{
//		cout << ps[i].name << " " << ps[i].age << endl;
//	}
//	delete[] ps;
//	return 0;
//}

//找出两个字符串中相同的字串
//int main()
//{
//	int a = 5;
//	int b = 3;
//
//	cout << a << b << endl;
//
//	return 0;
//}

//类的声明
//public——公共成员，可以被本类中的成员函数引用，也可以被类的作用域的其他函数所引用，即从类的外部是可以调用的；
//private——只能被本类的成员函数所引用，类外部不能调用
//protected——受保护的成员，不能被类外访问，这点类似private，但是可以被派生类的成员函数访问
//不加访问限定符，默认为private

//访问限定符可以在一个类中多次添加

//class CStudent
//{
//	//属性——成员变量
//	string name;
//	int num;
//public:
//	int age;
//private:
//	char sex;
//
//	//方法——成员函数
//public:
//	void print_age()
//	{
//		cout << "age=" << age << endl;
//	}
//
//	void set_sex(char ch)
//	{
//		sex = ch;
//	}
//};
////类,类似于结构体，但是可以添加函数
//
//int main()
//{
//	CStudent zhang_san;
//	zhang_san.age = 15;
//	zhang_san.print_age();
//	//zhang_san.name = "张三";//非public，不可访问
//	//无法访问sex 但是可以通过成员函数修改
//	zhang_san.set_sex('F');//将该类中的sex的值修改为F
//
//	return 0;
//}


//class CNumber
//{
//private:
//	int get_maxoftwo(int x, int y)//主函数中调用的辅助函数无需暴露于外
//	{
//		return x > y ? x : y;
//	}
//public:
//	int get_maxofthree(int a, int b, int c)//主函数负责对外接口
//	{
//		int max_a_b = get_maxoftwo(a, b);
//		return max_a_b > c ? max_a_b : c;
//	}
//};
//
//int main()
//{
//	CNumber number;
//	cout << number.get_maxofthree(25, 15, 56) << endl;
//	return 0;
//}


//作业：定义一个学生类，将所有成员变量声明为私有，并添加一个公有成员函数，录入对象的成员信息
class CPupil
{
	private:
	string name;
	int age;
	int num;
public:
	void AddInfo()
	{
		cout << "录入学生姓名" << endl;
		cin >> name;
		cout << "录入年龄" << endl;
		cin >> age;
		cout << "录入学号" << endl;
		cin >> num;
	}
};

int main()
{
	CPupil p;
	p.AddInfo();


	return 0;
}
