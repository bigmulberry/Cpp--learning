#define _CRT_SECURE_NO_WARNINGS 1
//#include <iostream>
//using namespace std;

//引用真正的价值体现在做参数和做返回值
//1.做参数 a.提高效率 不用拷贝 b.形参可以直接修改（输出型参数——常见于用指针修改）
//2.做返回值  （不要返回局部变量的引用）    其返回值（左值）可读可写！！！！！！

//const引用

//int main()
//{
//	double d = 1.11;
//	int i1 = d;//生成int临时变量，拷贝d（截短 or 提升），再复制给i1
//	//int& i2 = d;//错误
//	const int& i3 = d;//临时变量带有常性，不能被修改。所以要用const
//
//
//	int a = 3, b = 4;
//	const int& c = a+b;//也会产生临时变量，需要const
//	//const Type& 可以接受各种类型的对象
//	//使用引用传参，如果函数中不改变形参的值（不做输出参数），建议直接加const，还能省下拷贝空间时间
//
//	return 0;
//}
 

////extern "C"
//// 包含头文件
//// 在工程属性中添加静态库
//
//extern "C"
//{
//	#include "../../DS/DS/Stack.h"
//}
//
//
//bool isValid(char* s) {
//	//创建栈
//	ST st;
//	//初始化栈
//	StackInit(&st);
//
//
//	//遇到左括号填入栈中，遇到右括号与栈顶进行匹配，
//	//如果匹配则弹栈
//	//如果匹配不上返回false
//	//如果遇到右括号时,栈为空,则返回false
//	//遇到'\0',栈不为空时, 返回false
//
//
//	while (*s)
//	{
//		if (*s == '(' || *s == '[' || *s == '{') //左括号压栈
//		{
//			StackPush(&st, *s);
//		}
//		else
//		{
//			if (StackEmpty(&st))//遇到右括号栈为空
//			{
//				return false;
//			}
//			else if ((*s == '}' && StackTop(&st) != '{')
//				|| (*s == ')' && StackTop(&st) != '(')
//				|| (*s == ']' && StackTop(&st) != '['))//匹配不上
//			{
//				return false;
//			}
//			else//匹配 弹栈
//			{
//				StackPop(&st);
//			}
//		}
//		s++;
//	}
//
//	//判空，此时不为空则false
//	if (!StackEmpty(&st))
//	{
//		return false;
//	}
//
//	//销毁栈 防止内存泄漏
//	StackDestroy(&st);
//	return true;
//}
//
//
//
//
//int main()
//{
//	char s1[] = "{[()]}";
//	char s2[] = "{[}]";
//	cout << isValid(s1)<< endl;
//	cout << isValid(s2)<< endl;
//
//
//	return 0;
//}




//C项目
//#include "../../DS/DS/Stack.h"//调用c的静态库是ok的

//C调用C++
//#include "../../DS/DS/Stack.h"//头文件展开后C语言是不认是extern "C"的！
//
//
//#include <stdio.h>
//#include <stdbool.h>
//bool isValid(char* s) {
//	//创建栈
//	ST st;
//	//初始化栈
//	StackInit(&st);
//
//
//	//遇到左括号填入栈中，遇到右括号与栈顶进行匹配，
//	//如果匹配则弹栈
//	//如果匹配不上返回false
//	//如果遇到右括号时,栈为空,则返回false
//	//遇到'\0',栈不为空时, 返回false
//
//	while (*s)
//	{
//		if (*s == '(' || *s == '[' || *s == '{') //左括号压栈
//		{
//			StackPush(&st, *s);
//		}
//		else
//		{
//			if (StackEmpty(&st))//遇到右括号栈为空
//			{
//				return false;
//			}
//			else if ((*s == '}' && StackTop(&st) != '{')
//				|| (*s == ')' && StackTop(&st) != '(')
//				|| (*s == ']' && StackTop(&st) != '['))//匹配不上
//			{
//				return false;
//			}
//			else//匹配 弹栈
//			{
//				StackPop(&st);
//			}
//		}
//		s++;
//	}
//
//	//判空，此时不为空则false
//	if (!StackEmpty(&st))
//	{
//		return false;
//	}
//
//	//销毁栈 防止内存泄漏
//	StackDestroy(&st);
//	return true;
//}
//
//
//
//int main()
//{
//	char s1[] = "{[()]}";
//	char s2[] = "{[}]";
//
//	printf("%d\n", isValid(s1));
//	printf("%d\n", isValid(s2));
//
//	return 0;
//}


#define Add(x,y) ((x)+(y))

 

//有了内联函数，我们就不需要C的宏，应为宏很复杂，很容易出错

//长函数和递归函数不适合展开，调用地方多 程序会变大