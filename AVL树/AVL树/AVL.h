#pragma once
#include <iostream>
using namespace std;
#include <assert.h>
template<class K,class V>
struct AVLTreeNode
{
	AVLTreeNode<K, V>* _parent;
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	//存储键值对
	pair<K, V> _kv;
	//平衡因子
	int _bf;

	AVLTreeNode(const pair<K,V>& kv):
		_kv(kv),
		_left(nullptr),
		_right(nullptr),
		_parent(nullptr),
		_bf(0)
	{}
};

template <class K,class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	AVLTree() :_root(nullptr)
	{

	}
	//拷贝构造和赋值重载都要深拷贝

	~AVLTree()
	{
		_Destroy(_root);
		_root = nullptr;
	}

	pair<Node* , bool > insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return make_pair(_root,true);
		}
		Node* cur = _root;
		Node* parent=_root;
		while (cur)
		{
			parent = cur;
			if (kv.first > cur->_kv.first)
			{
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				cur = cur->_left;
			}
			else
			{
				return make_pair(cur,false);
			}
		}
		cur = new Node(kv);
		Node* newnode = cur;
		if (kv.first > parent->_kv.first)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}
		cur->_parent = parent;

		//更新平衡因子，检查是否需要旋转
		while (parent)//最远可更新到_root
		{
			//更新平衡因子
			if (cur == parent->_right)
				parent->_bf++;
			else
				parent->_bf--;
			//讨论平衡因子情况
			if (parent->_bf == 0)
			{
				break;
			}
			else if(parent->_bf==1 || parent->_bf==-1)
			{
				//parent的高度增加，会影响 parent->_parent
				//继续往上更新
				cur = parent;
				parent = parent->_parent;
			}
			else if(parent->_bf == 2 || parent->_bf == -2)
			{
				//parent所在子树已不平衡，旋转当前子树
				if (parent->_bf == 2 )
				{
					if (cur->_bf == 1)
						RotateL(parent);//左单旋
					else//cur->_bf == -1
						RotateRL(parent);//右左双旋
				}
				else //parent->_bf == -2
				{
					if (cur->_bf == -1)
						RotateR(parent);//右单旋
					else//cur->_bf == 1
						RotateLR(parent);//左右双旋
				}
				break;//旋转处理后，parent的平衡因子已经为0
			}
			else
			{
				assert(false);//说明在插入此节点之前树已经不平衡了，需检查代码。
			}
		}
	
		return make_pair(newnode,true);//插入成功
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool  IsAVLTree()
	{
		int height = 0;
		return _IsBalance(_root,height);
	}

	Node* Find(const K& key)
	{
		Node* cur = _root;
		while (cur)
		{
			if (key > cur->_kv.first)
			{
				cur = cur->_right;
			}
			else if (key < cur->_kv.first)
			{
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}

	bool erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}
		Node* cur = _root;//遍历
		Node* parent = nullptr;
		Node* delPos=nullptr;//标记删除结点
		Node* delParentPos=nullptr;
		while (cur)
		{
			if (cur->_kv.first > key)//key比当前结点小
			{
				parent = cur;
				cur = cur->_left;//往左子树走
			}
			else if(cur->_kv.first < key)//key比当前结点大
			{
				parent = cur;
				cur = cur->_right;//往右子树走
			}
			else//找到删除结点
			{
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						_root = _root->_right;
						if (_root != nullptr)
							_root->_parent = nullptr;
						delete cur;
						return true;
					}
					else
					{
						delParentPos = parent;//标记删除结点的父结点
						delPos = cur;//标记为删除结点
					}
					break;//找到了删除点，需更新平衡因子
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						_root = _root->_left;
						if (_root != nullptr)
							_root->_parent = nullptr;
						delete cur;
						return true;
					}
					else
					{
						delParentPos = parent;
						delPos = cur;
					}
					break;
				}
				else//左右均不为空
				{
					//替换法删除,找到右子树中key最小的结点
					Node* minParent = cur;
					Node* minRight = cur->_right;
					while (minRight->_left)
					{
						minParent = minRight;
						minRight = minRight->_left;
					}
					cur->_kv.first = minRight->_kv.first;
					cur->_kv.second = minRight->_kv.second;//替换
					delParentPos = minParent;
					delPos = minRight;//实际删除结点
					break;
				}
			}
		}
		//找不到删除结点
		if (cur == nullptr)
		{
			return false;
		}
		//下面开始更新平衡因子
		cur = delPos;
		parent = delParentPos;
		while (parent != nullptr)//最坏情况更新到根结点
		{
			//更新结点平衡因子
			if (cur == parent->_left)
			{
				parent->_bf++;
			}
			else if (cur == parent->_right)
			{
				parent->_bf--;
			}

			//旋转情况讨论
			if (parent->_bf == 1 || parent->_bf == -1)
			{
				break;//高度没有变化，更新完毕
			}
			else if (parent->_bf == 0)
			{
				//需往上继续更新
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				//开始旋转处理
				//前两种情况旋转会为我们处理平衡因子，
				//但是subR平衡因子为0的情况需要我们手动修改
				if (parent->_bf == 2)
				{
					Node* subR = parent->_right;
					if (subR->_bf == 1)//左单旋
					{
						RotateL(parent);
						parent = subR;
					}
					else if (subR->_bf == -1)//右左双旋
					{
						Node* subRL = subR->_left;
						RotateRL(parent);
						parent = subRL;
					}
					else if (subR->_bf == 0)//左单旋
					{
						RotateL(parent);
						//手动修改平衡因子
						parent->_bf = 1;
						subR->_bf = -1;
						//旋后树高不变，无需再往上更新
						break;
					}
				}
				else if (parent->_bf == -2)
				{
					Node* subL=parent->_left;
					if (subL->_bf == -1)//右单旋
					{
						RotateR(parent);
						parent = subL;
					}
					else if (subL->_bf == 1)//左右双旋
					{
						Node* subLR = subL->_right;
						RotateLR(parent);
						parent = subLR;
					}
					else if (subL->_bf == 0)
					{
						RotateR(parent);
						//手动修改平衡因子
						parent->_bf = -1;
						subL->_bf = 1;
						//旋后树高不变，无需再往上更新
						break;
					}
				}
				//到了此处，说明还需要往上更新
				cur = parent;
				parent = parent->_parent;
			}
			else
			{
				assert(false);//删除前平衡因子就有错误
			}

		}
		//进行删除，现在的删除结点的度<=1
		if (delPos->_left == nullptr)//实际删除结点的左子树为空
		{
			if (delPos == delParentPos->_left)
			{
				delParentPos->_left = delPos->_right;
				//delParentPos->_bf++;
			}
			else if(delPos==delParentPos->_right)
			{
				delParentPos->_right = delPos->_right;
				//delParentPos->_bf--;
			}
			//由于删除结点的右树可能为空，需要判断
			if (delPos->_right != nullptr)
			{
				delPos->_right->_parent = delParentPos;
			}
		}
		else if (delPos->_right == nullptr)
		{
			//走到这说明删除节点的左子树不为空,右子树为空

			if (delPos == delParentPos->_left)
			{
				delParentPos->_left = delPos->_left;
				//delParentPos->_bf--;

			}
			else if (delPos == delParentPos->_right)
			{
				delParentPos->_right = delPos->_left;
				//delParentPos->_bf++;
			}
			delPos->_left->_parent = delParentPos;
		}
		delete delPos;
		delPos = nullptr;
		return true;
	}

	V& operator[](const K& key)
	{
		Node* ptr=insert(make_pair(key, V())).first;
		return ptr->_kv.second;
	}
	

private:
	
	
	void RotateL(Node* parent)
	{
		Node* Pparent = parent->_parent;
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		//parent 和 subR 的关系建立
		parent->_parent = subR;
		subR->_left = parent;

		//parent 和 subRL 的关系建立
		parent->_right = subRL;
		if (subRL != nullptr)
		{
			subRL->_parent = parent;
		}

		//subR 和 Pparent 的关系建立
		if (Pparent != nullptr)
		{
			subR->_parent = Pparent;
			if (parent == Pparent->_left)
			{
				Pparent->_left = subR;
			}
			else
			{
				Pparent->_right = subR;
			}
		}
		else
		{
			_root = subR;
			subR->_parent = nullptr;
		}

		//更新结点平衡因子
		subR->_bf = 0;
		parent->_bf = 0;
	}

	void RotateR(Node* parent)
	{
		Node* Pparent = parent->_parent;
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		// parent 与 subL的关系建立
		subL->_right = parent;
		parent->_parent = subL;

		// parent 与 subLR 的关系建立
		if (subLR != nullptr)
		{
			subLR->_parent = parent;
		}
		parent->_left = subLR;
		
		// Pparent 与 subL的关系建立
		if (Pparent != nullptr)
		{
			subL->_parent = Pparent;
			if (Pparent->_left == parent)
			{
				Pparent->_left = subL;
			}
			else
			{
				Pparent->_right = subL;
			}
		}
		else
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		//更新结点平衡因子
		subL->_bf = 0;
		parent->_bf = 0;
	}

	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		//两次单旋后会更改subRL的平衡因子，之后还需判断，所以先备份下来
		int bf = subRL->_bf;

		RotateR(subR);
		RotateL(parent);

		subRL->_bf = 0;
		if (bf == 0)
		{
			parent->_bf = 0;
			subR->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = -1;
			subR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			subR->_bf = 1;
		}
		else
		{
			assert(false);
		}
	}

	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		//两次单旋后会更改subLR的平衡因子，之后还需判断，所以先备份下来
		int bf = subLR->_bf;

		RotateL(subL);
		RotateR(parent);

		subLR->_bf = 0;
		if (bf == 0)
		{
			parent->_bf = 0;
			subL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			subL->_bf = -1;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	
	
	bool _IsBalance(Node* root,int& height)
	{

		if (root == nullptr)
		{
			height = 0;
			return true;
		}

		//判断左子树是否为AVL树，同时通过输出型参数得到左子树高度
		int leftHeight = 0;
		if (_IsBalance(root->_left, leftHeight) == false)
			return false;

		//判断右子树是否为AVL树，同时通过输出型参数得到右子树高度
		int rightHeight = 0;
		if (_IsBalance(root->_right, rightHeight) == false)
			return false;

		//检查平衡因子是否有误
		if (rightHeight - leftHeight != root->_bf)
		{
			cout << root->_kv.first << " 的平衡因子应为：" << rightHeight - leftHeight << ",实为：" << root->_bf << endl;
			return false;
		}
		//计算当前树的高度，左右子树的较高者+1
		height = max(leftHeight, rightHeight) + 1;
		return abs(rightHeight - leftHeight) < 2;
	}

	void _Destroy(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_Destroy(root->_left);
		_Destroy(root->_right);
		delete root;

	}

private:
	Node* _root;
};