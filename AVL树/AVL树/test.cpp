#define _CRT_SECURE_NO_WARNINGS 1
#include "AVL.h"
#include <iostream>
using namespace std;
void TestAVLTree()
{
	int a[] = { 18,14,20,12,16,15};
	AVLTree<int, int> T;
	cout << "------insert-------" << endl;
	for (auto& e : a)
	{
		T.insert(make_pair(e, e));
		cout << e << ":" << T.IsAVLTree() << endl;
	}

	cout << "------modify-------" << endl;
	T[18]*=10;
	T[14]*=10;
	T[12]*=10;
	T.InOrder();

	cout << "-------erase-------" << endl;
	for (auto& e : a)
	{
		T.erase(e);
		cout << e << ":" << T.IsAVLTree() << endl;
	}
}

int main()
{

	TestAVLTree();
	return 0;
}