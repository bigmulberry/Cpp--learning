#include "Student.h"

//①、这样所有对象都可以直接访问这个静态成员变量，而且值是一样的；
//②、静态成员变量在内存中只占用一份存储空间；
//③、静态成员变量的值对于所有对象来说都是一样的。如果其中一个对象调用函数将其改变了，
//    那么其他成员在访问这个静态成员变量的时候的值都是改变之后的；
//④、只要在类中定义了类的静态成员变量，那么就占用存储空间了，不管有没有定义这个类的对象，
//    因为静态成员变量不属于任何类对象，而是属于该类；
//⑤、静态数据成员需要在类外进行声明或声明并初始化，否则在使用的时候会报链接的错误；
//⑥、类的静态成员在定义的时候需要加 static，在类外声明的时候不需要加 static 关键字；
//⑦、不能用构造函数的参数初始化表的形式对静态成员进行初始化操作；
//⑧、静态数据成员既可以通过对象名引用，也可以通过类名来直接引用.
//⑨、public 公有类型的静态数据成员，可以被类的对象引用，也可以直接用类名来引用.
//    但如果静态数据成员被定义成private私有的，那么通过对象或者类名来引用都是不可以的，
//    必须通过类的public类型的静态成员函数来引用了。

int main()
{
	Student zhangsan;
	zhangsan.SetTeacher(40);
	Student lisi;
	//lisi.master = 100;
	//cout << lisi.master << endl;
	
	cout << lisi.GetTeacher() << endl;
}

