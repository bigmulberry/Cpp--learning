#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <iostream>
using namespace std;
class Student
{
public:
	char name[50];
	int num;
	int age;
	char sex;


	//静态成员 属于整个类而非单独一个对象
	static int master;
	Student() {};
	Student(char* t_name,int t_age,int t_num,char t_sex);
	int GetTeacher();
	void SetTeacher(int t);
	
private:
	static int teacher;
};

