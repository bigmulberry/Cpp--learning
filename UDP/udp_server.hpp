#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#define DEFAULT 8081
class UdpServer
{
private:
    int port;//服务器端口号
    int sockfd;//socket文件描述符 
    //std::string ip;//IP
public:
    //UdpServer(std::string _ip,int _port=DEFAULT):port(_port),sockfd(-1),ip(_ip)
    UdpServer(int _port=DEFAULT):port(_port),sockfd(-1)//,ip(_ip)
    {
    }

     bool InitUdpServer()
    {
        sockfd=socket(AF_INET,SOCK_DGRAM,0);//创建套接字（打开文件）
        if(sockfd<0)
        {
            std::cerr<<"socket error"<<std::endl;
            return false;
        }
        std::cout<<"socket create success,sockfd:"<<sockfd<<std::endl;//3

        //服务器的socket与IP地址和端口号绑定

        struct sockaddr_in local;//IPv4 结构体
        memset(&local,'\0',sizeof(local));
        //给定协议
        local.sin_family=AF_INET;
        //给定端口号，端口号需转为网络序列
        local.sin_port=htons(port);
        //给定IP地址,需将字符串转换为32位整型，以及网络字节序转换
        //local.sin_addr.s_addr=inet_addr(ip.c_str());
        local.sin_addr.s_addr=INADDR_ANY;

        if(bind(sockfd,(struct sockaddr*)&local,sizeof(local))<0)
        {
            std::cerr<<"bind false"<<std::endl;
            return false;
        }
        std::cout<<"bind success,sockfd:"<<sockfd<<std::endl;//3

        return true; 
    }

    void Start()
    {
        #define SIZE 128
        char buffer[SIZE]={0};
        for(;;)  
        {
            //接收来自对端信息的结构体peer，靠输出型参数获取
            struct sockaddr_in peer;
            socklen_t len=sizeof(peer);
            
            //服务器接收客户端的数据
            ssize_t size= recvfrom(sockfd,buffer,sizeof(buffer)-1,0,(struct sockaddr*)&peer,&len);//输出型参数 
            if(size>0)
            {
                buffer[size]=0;
                int _port=ntohs(peer.sin_port);//网络传输的端口号 主机序列化  —— 来自于客户端的的端口号
                std::string _ip=inet_ntoa(peer.sin_addr);//四字节转字符串  —— 来自于客户端的IP 
                std::cout<<_ip<<":"<<_port<<"#"<<buffer<<std::endl;

                //执行业务
                std::string cmd=buffer;
                std::string result;
                if(cmd=="ls")
                {
                    int pipes[2];
                    pipe(pipes);
                    pid_t id=fork();
                    if(id==0)
                    {
                        //child
                        close(pipes[0]);
                        dup2(pipes[1],1);
                        execl("/usr/bin/ls","ls","-a","-l","-i",nullptr);
                        exit(1);
                    }
                    close(pipes[1]);\
                    char c;
                    while(1)
                    {
                        if(read(pipes[0],&c,1)>0)
                        {
                            result.push_back(c);
                        }
                        else
                        {
                            break;
                        }
                    }
                    wait(nullptr);
                }


                //服务器sendto数据到客户端
                std::string echo_msg;
                if(result.empty())
                {
                    echo_msg="server get->";
                    echo_msg+=buffer;
                }
                else
                {
                    echo_msg=result;
                }

                sendto(sockfd,echo_msg.c_str(),echo_msg.size(),0,(struct sockaddr*)&peer,len);
            }
            else{
                std::cerr<<"recvfrom error"<<std::endl;
            }
        }

    }

    ~UdpServer()
    {
        if(sockfd>0)
        {
            close(sockfd);
        }
    }
    
};