#include "udp_client.hpp"
// ./udp_client server_ip server_port
int main(int argc,char* argv[])
{
    if(argc!=3)
    {
        std::cerr<<"Usage:"<<argv[0]<<" server_ip server_port"<<std::endl;
        return 1;
    }
    std::string ip=argv[1];
    int port =atoi(argv[2]); 
    UdpClient* client=new UdpClient(ip,port);
    client->InitUdpClient();
    client->Start();
    return 0;    
}