#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
class UdpClient
{
private:
    int sockfd;
    std::string server_ip;
    int server_port;
public:
    UdpClient(std::string _ip,int _port):server_ip(_ip),server_port(_port)
    {}

    bool InitUdpClient()
    {
        sockfd=socket(AF_INET,SOCK_DGRAM,0);//打开网络连接（理解为打开文件）
        if(sockfd<0)
        {
            std::cerr<<"socket error"<<std::endl;
            return false;
        }

        //客户端需要port，但是为隐式绑定（bind），空闲的端口号会被赋予给socket
        return true;
    }
    
    void Start()
    {
        //发送给对端的结构体
        struct sockaddr_in peer; 
        memset(&peer,0,sizeof(peer));
        peer.sin_family=AF_INET;
        peer.sin_port=htons(server_port);//发向哪个端口号
        peer.sin_addr.s_addr=inet_addr(server_ip.c_str());//发向哪一个IP地址

        std::string msg; 
        for(;;)
        {
            std::cout<<"Please input# ";
            std::cin>>msg;
            sendto(sockfd,msg.c_str(),msg.size(),0,(struct sockaddr*)&peer,sizeof(peer));

            char buffer[128];
            struct sockaddr_in tmp;
            socklen_t len=sizeof(tmp);
            ssize_t size=recvfrom(sockfd,buffer,sizeof(buffer)-1,0,(struct sockaddr*)&tmp,&len);//服务端的ip和端口号是已知的，recvfrom的结构体交给临时变量就好
            if(size>0)
            {
                buffer[size]=0;
                std::cout<<"client receive echo: "<<buffer<<std::endl;
            }
        }
    }
    
    
    ~UdpClient()
    {
        if(sockfd>0)
        {
            close(sockfd);
        }
    }
};