#include "udp_server.hpp"
#include <arpa/inet.h>


//udp_server  port
//INADDR_AN->0
int main(int argc,char* argv[])
{
    if(argc!=2)
    {
        std::cerr<<"Usage:"<<argv[0]<<" port"<<std::endl;
        return 1;
    }
    //std::string ip="127.0.0.1";//127.0.0.1 == local  ：标识本地主机：本地环回
    int port=atoi(argv[1]);
    //UdpServer* server=new UdpServer(ip,port);
    UdpServer* server=new UdpServer(port);
    server->InitUdpServer(); 
    server->Start();
    return 0;
}