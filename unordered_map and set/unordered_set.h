#pragma once
#include "HashTable.h"

namespace mystd
{
	template <class K,class HashFunc=OpenHash::Hash<K>>
	class unordered_set
	{
		struct SetKOfValue
		{
			//setʹ��
			const K& operator()(const K& k)
			{
				return k;
			}
		};

	public:
		typedef  typename OpenHash::HashTable<K, K, SetKOfValue, HashFunc>::iterator iterator;

		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		iterator Find(const K& key) {
			return _ht.find(key);
		}

		pair<iterator,bool> Insert(const K& k)
		{
			return _ht.insert(k); 
		}
		
		void erase(const K& key)
		{
			_ht.erase(key);
		}
	private:
		OpenHash::HashTable<K, K, SetKOfValue, OpenHash::Hash<K>> _ht;
	};


	void test_unordered_set()
	{
		unordered_set<int> us;
		int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
		for (auto e : arr)
		{
			us.Insert(e);
		}

		unordered_set<int>::iterator it = us.begin();
		while (it != us.end())
		{
			cout << *it << endl;
			++it;
		}
	}



}