#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include<time.h>
#include <vector>
#include "HashTable.h"
#include "unordered_map.h"
#include "unordered_set.h"




//void efficiency_test()
//{
//	srand((unsigned int)time(0));
//	int n = 1000000;
//	vector<int> data;
//	data.reserve(n);
//	set<int> s;
//	unordered_set<int> us;
//	for (int i = 0; i < n; ++i)
//	{
//		data.push_back(rand());
//	}
//
//	cout<<"-----插入效率测试-----" << endl;
//	clock_t begin1, end1, begin2, end2;
//
//	begin1 = clock();
//	for (auto e : data)
//	{
//		s.insert(e);
//	}
//	end1 = clock();
//
//	begin2 = clock();
//	for (auto e : data)
//	{
//		us.insert(e);
//	}
//	end2 = clock();
//
//	cout << "set insert spend : " << end1 - begin1 << endl;
//	cout << "unordered_set insert spend : " << end2 - begin2 << endl;
//
//
//	cout<<"-----查询效率测试-----" << endl;
//	begin1 = clock();
//	for (auto e : data)
//	{
//		s.find(e);
//	}
//	end1 = clock();
//
//	begin2 = clock();
//	for (auto e : data)
//	{
//		us.find(e);
//	}
//	end2 = clock();
//
//	cout << "set find spend : " << end1 - begin1 << endl;
//	cout << "unordered_set find spend : " << end2 - begin2 << endl;
//
//	cout<<"-----删除效率测试-----" << endl;
//
//	begin1 = clock();
//	for (auto e : data)
//	{
//		s.erase(e);
//	}
//	end1 = clock();
//
//	begin2 = clock();
//	for (auto e : data)
//	{
//		us.erase(e);
//	}
//	end2 = clock();
//
//	cout << "set erase spend : " << end1 - begin1 << endl;
//	cout << "unordered_set erase spend : " << end2 - begin2 << endl;
//}


//void TestCloseHashTable1()
//{
//	int arr[] = { 1,5,10,100,1000,8,9,12 ,11};
//	CloseHash::HashTable<int, int> h;
//	for (auto e : arr)
//	{
//		h.insert(make_pair(e, e));
//	}
//
//	auto ret = h.find(100);
//	if (ret)
//		cout << "找到了" << endl;
//	else 
//		cout << "没有找到" << endl;
//	h.erase(100);
//	ret = h.find(100);
//	if (ret) 
//		cout << "找到了" << endl;
//	else 
//		cout << "没有找到" << endl;
//}
//
//
//
//void TestCloseHashTable2()
//{
//	CloseHash::HashTable<string, int, CloseHash::Hash<string>> CountFruit;
//	string fruit[] = { "apple","banana","watermelon","apple","banana" };
//	for (auto& e : fruit)
//	{
//		auto ret = CountFruit.find(e);
//		if (ret)
//		{
//			ret->_kv.second++;
//		}
//		else
//		{
//			CountFruit.insert(make_pair(e,1));
//		}
//	}
//}
//
//
//void testOpenHashTable1()
//{
//	int arr[] = { 1,5,10,100,1000,8,9,12 ,11 };
//	OpenHash::HashTable<int, int> h;
//	for (auto e : arr)
//	{
//		h.insert(make_pair(e, e));
//	}
//
//	auto ret = h.find(100);
//	if (ret)
//		cout << "找到了" << endl;
//	else
//		cout << "没有找到" << endl;
//	h.erase(100);
//	ret = h.find(100);
//	if (ret)
//		cout << "找到了" << endl;
//	else
//		cout << "没有找到" << endl;
//}
//
//void testOpenHashTable2()
//{
//	OpenHash::HashTable<string, int, OpenHash::Hash<string>> CountFruit;
//	string fruit[] = { "apple","banana","watermelon","apple","banana" };
//	for (auto& e : fruit)
//	{
//		auto ret = CountFruit.find(e);
//		if (ret)
//		{
//			ret->_kv.second++;
//		}
//		else
//		{
//			CountFruit.insert(make_pair(e, 1));
//		}
//	}
//}

//test unordered_map
//struct student
//{
//	string name;
//	int number;
//	char gender;
//
//	bool operator==(const student& s)const
//	{
//		return s.number == number;
//	}
//};
//
//struct Hash
//{
//	size_t operator()(const student& s)const
//	{
//		return (size_t)s.number;
//	}
//};





int main()
{
	//efficiency_test();
	//TestCloseHashTable1();
	//TestCloseHashTable2();

	//testOpenHashTable1();
	//testOpenHashTable2();
	

	//unordered_map<student, int,Hash> mp;
	//student A = { "Alice",25,'f' };
	//student B = { "Bob",24,'m' };
	//student C = { "Celina",23,'f' };

	//mp[A] = 100;
	//mp[B] = 85;
	//mp[C] = 100;

	//unordered_map<int,int> m;
	//m.reserve(100);
	//m.insert(make_pair(1, 1));
	//m.insert(make_pair(2, 1));
	//m.insert(make_pair(3, 1));
	//m.insert(make_pair(4, 1));
	//int n= m.bucket_count();
	//for(int i=0;i<n;++i)
	//{
	//	cout << i << " has " << m.bucket_size(i)<<" elements" << endl;
	//}

	mystd::test_unordered_set();
	mystd::test_unordered_map();



	return 0;
}


