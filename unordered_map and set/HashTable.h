#pragma once
#include <utility>


//namespace CloseHash
//{
//	enum State
//	{
//		EMPTY,
//		EXIST,
//		DELETE
//	};
//
//	template <class Value>
//	struct HashData
//	{
//		pair<K, V> _kv;
//		State _state = EMPTY;
//	};
//
//	template <class K>
//	struct Hash
//	{
//		const size_t operator()(const K& key)
//		{
//			return (size_t)key;
//		}
//	};
//
//	template<>
//	struct Hash<string>
//	{
//		const size_t operator()(const string& key)
//		{
//			size_t hash = 0;
//			// 把字符串的所有字母加起来   hash = hash*131 + key[i]
//			for (size_t i = 0; i < key.size(); ++i)
//			{
//				hash *= 131;
//				hash += key[i];
//			}
//			return hash;
//		}
//	};
//
//	template <class K, class V, class HashFunc = Hash<K>>//HashFunc负责将key值（可能为string）转换为int，方便取余
//	class HashTable
//	{
//	public:
//		bool insert(const pair<K, V>& kv)
//		{
//			if (find(kv.first) != nullptr)//防止重复
//			{
//				return false;
//			}
//
//			//扩容逻辑
//			//初始时是一个空表，则resize空间。
//			if (_table.size() == 0)
//			{
//				_table.resize(10);
//			}
//			else if (10 * _n / _table.size() > 7)//或者负载因子超过0.7，则需要换一个更大的表
//			{
//				//法一 新建一个table，然后将元素转移
//				//vector<HashData> newtable;
//				//newtable.resize(2 * _table.size());
//				//for (auto& e : _table)
//				//{
//				//	if (e._state == EXIST)//因为新表的容量更大，所以元素位置会变化
//				//	{
//				//		//重新按照线性探测或二次探测规则在新表中插入
//				//	}
//				//}
//
//				//法二 新建一个HashTable类(推荐)
//				HashTable<K, V, HashFunc> newHT;
//				newHT._table.resize(2 * _table.size());
//				for (const auto& e : _table)
//				{
//					if (e._state == EXIST)
//					{
//						newHT.insert(e._kv);//复用了该类的insert成员函数
//					}
//				}
//				_table.swap(newHT._table);
//			}
//
//			//插入新值的逻辑
//			HashFunc hf;//仿函数 将key转换为int
//			size_t start = hf(kv.first) % _table.size();
//			size_t index = start;
//			size_t i = 1;
//			while (_table[index]._state == EXIST)
//			{
//				index = start + i;//线性探测
//				//index=start+i*i;//二次探测
//				index %= _table.size();
//				++i;
//			}
//			_table[index]._kv = kv;
//			_table[index]._state = EXIST;
//			_n++;
//
//			return true;
//		}
//
//		HashData<K, V>* find(const K& key)
//		{
//			if (_table.size() == 0)
//			{
//				return nullptr;
//			}
//			HashFunc hf;
//			size_t start = hf(key) % _table.size();
//			size_t index = start;
//			int i = 1;
//			while (_table[index]._state != EMPTY)
//			{
//				if (_table[index]._kv.first == key && _table[index]._state == EXIST)
//				{
//					return &_table[index];
//				}
//				index = start + i;
//				//index = start+i*i;//二次探测
//				i++;
//				index %= _table.size();
//			}
//			return nullptr;
//		}
//
//		bool erase(const K& key)
//		{
//			HashData<K, V>* del = find(key);
//			if (del == nullptr)//没有该点
//			{
//				return false;
//			}
//			del->_state = DELETE;
//			_n--;
//			return true;
//		}
//
//	private:
//		vector<HashData<K, V>> _table;//容器
//		size_t _n = 0;//实际存储的元素个数
//	};
//
//}


namespace OpenHash
{
	//********************************************************************************
	//Key转size_t仿函数
	template<class K>
	struct Hash
	{
		const size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	template<>
	struct Hash<string>
	{
		const size_t operator()(const string& key)
		{
			size_t hash = 0;
			for (size_t i = 0; i < key.size(); ++i)
			{
				hash *= 131;
				hash += key[i];
			}
			return hash;
		}
	};
	
	//***********************************************************************
	//Value中提取Key 仿函数




	template <class Value>
	struct HashData
	{
		//链表指针
		HashData<Value>* _next;
		//链表结点数据
		Value _data;

		HashData(const Value& data) :_data(data), _next(nullptr)
		{

		}
	};


	//前置声明哈希表(声明无需给默认参数，定义时给即可)
	template <class K, class Value, class KeyOfValue, class HashFunc >
	class HashTable;


	//迭代器
	template <class K, class Value, class KeyOfValue, class HashFunc = Hash<K> >
	struct HashIterator
	{
		typedef HashData<Value> Node;
		typedef HashIterator<K, Value, KeyOfValue, HashFunc> Self;
		typedef HashTable<K, Value, KeyOfValue, HashFunc> HT;//迭代器需要表

		Node* _node;
		HT* _pht;

		HashIterator(Node* node,HT* pht):_node(node),_pht(pht)
		{
			 
		}

		Self& operator++()
		{

			//桶中若还有数据，就往当前桶后面走
			if (_node->_next)
			{
				_node=_node->_next;
			}
			else//当前桶走完了 换下一个桶
			{
				KeyOfValue kov;
				HashFunc hf;

				//计算当前在哪个桶 
				size_t index = hf((kov(_node->_data))) % _pht->_table.size();
				index++;
				while (index < _pht->_table.size())//防止越界
				{
					if (_pht->_table[index])
					{
						_node = _pht->_table[index];
						return *this;
					}
					else
					{
						index++;
					}
				}
				_node = nullptr;
			}
			return *this;
		}



		Value& operator*()
		{
			return _node->_data;
		}

		Value* operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& s)
		{
			return _node != s._node;
		}

		bool operator==(const Self& s)
		{
			return _node == s._node;
		}

	};


	template <class K, class Value, class KeyOfValue,class HashFunc = Hash<K> >
	class HashTable
	{
		typedef HashData<Value> Node;

		template <class K, class Value, class KeyOfValue, class HashFunc>
		friend struct HashIterator;
	public:
		typedef HashIterator< K, Value, KeyOfValue, HashFunc> iterator;


		HashTable() = default;//显示指定生成默认构造

		//拷贝构造

		HashTable(const  HashTable& ht)
		{
			_n = ht._n;
			_table.resize(ht._table.size());
			for (int i = 0; i < ht._table.size(); ++i)
			{
				Node* cur = ht._table[i];
				while (cur)
				{
					Node* newnode = new Node(cur->_data);
					//头插
					newnode->_next = _table[i];
					_table[i] = newnode;

					cur = cur->_next;
				}
			}
		}

		//赋值重载
		HashTable& operator=(const HashTable ht)
		{
			_table.swap(ht._table);
			swap(_n, ht._n);
			return *this;
		}


		//析构
		~HashTable()
		{
			for (size_t i = 0; i < _table.size(); ++i)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
		}




		iterator begin()
		{
			size_t index = 0;
			while (index < _table.size())
			{
				if (_table[index] != nullptr)
				{
					return iterator(_table[index], this);
				}
				index++;
			}
			return end();
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		iterator find(const K& key)
		{
			
			if (_table.size() == 0)//元素数量为0，无法查找
			{
				return end();
			}

			KeyOfValue kov;
			HashFunc hf;//key转size_t 仿函数实例化

			//计算哈希地址
			size_t index = hf(key) % _table.size();
			Node* cur = _table[index];
			while (cur)//遍历哈希桶
			{
				if (kov(cur->_data) == key)
				{
					return iterator(cur,this);
				}
				cur = cur->_next;
			}
			return end(); 
		}

		pair<iterator,bool> insert(const Value& data)
		{
			KeyOfValue kov;
			iterator ret = find(kov(data));
			if (ret!=end())
			{
				return make_pair(ret,false);
			}
			HashFunc hf;//key转size_t仿函数实例化
			

			//负载因子为1 增容
			if (_n == _table.size())
			{
				size_t newsize = _n == 0 ? 10 : 2 * _table.size();
				vector<Node*> newtable;
				newtable.resize(newsize);
				for (size_t i = 0; i < _n; ++i)
				{
					if (_table[i])//桶不空，遍历该哈希桶
					{
						//拿到这个桶的头结点
						Node* cur = _table[i];
						while (cur)
						{
							Node* next = cur->_next;

							//重新计算映射位置
							size_t index = hf(kov(cur->_data)) % newtable.size();
							//插入至新表
							cur->_next = newtable[index];
							newtable[index] = cur;
							cur = next;
						}
					}
				}
				_table.swap(newtable);//交换哈希表
			}


			size_t index = hf(kov(data)) % _table.size();
			Node* newnode = new Node(data);

			//头插
			newnode->_next = _table[index];
			_table[index] = newnode;
			++_n;

			return make_pair(iterator(newnode,this),true);
		}

		bool erase(const K& key)
		{
			KeyOfValue kov;
			HashFunc hf;
			
			//1.计算哈希地址
			size_t index = hf(key) % _table.size();

			//2.在桶中寻找待删除结点
			Node* cur = _table[index];
			Node* prev = nullptr;
			while (cur)
			{
				if (kov(cur->_data) == key)
				{
					if (prev == nullptr)//cur为头结点
					{
						_table[index] = cur->_next;
					}
					else//cur为中间结点
					{
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}


	private:
		vector<Node*> _table;//指针数组
		size_t _n = 0;//实际存放的元素数量
	};



}