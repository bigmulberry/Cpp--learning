#pragma once
#include "HashTable.h"

namespace mystd
{
	template <class K,class Value,class HashFunc=OpenHash::Hash<K>>
	class unordered_map
	{
		struct MapKOfValue
		{
			//mapʹ��
			const K operator()(const pair<const K, Value>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename OpenHash::HashTable<K, pair<K, Value>, MapKOfValue, HashFunc>::iterator iterator;

		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		iterator Find(const K& key) 
		{
			return _ht.find(key);
		}

		pair<iterator,bool> Insert(const pair<K, Value>& kv)
		{
			return _ht.insert(kv);
		}

		void erase(const K& key)
		{
			_ht.erase(key);
		}

		Value& operator[](const K& key)
		{
			pair<iterator, bool> ret = Insert(make_pair(key,Value()));
			return ret.first->second;
		}

	private:
		OpenHash::HashTable<K, pair<K,Value>, MapKOfValue, HashFunc> _ht;

	};

	void test_unordered_map()
	{
		unordered_map<int,int> um;
		int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
		for (auto e : arr)
		{
			um.Insert(make_pair(e,e*10));
		}

		um[11] = 110;

		unordered_map<int,int>::iterator it = um.begin();
		while (it != um.end())
		{
			cout << it->first <<':'<<it->second << endl;
			++it;
		}

		unordered_map<string, string> Countfruit;
		Countfruit["string"]="�ַ���";
		Countfruit["heap"]="��";
		Countfruit["stack"]="ջ";
		unordered_map<string,string>::iterator cit = Countfruit.begin();
		while (cit != Countfruit.end())
		{
			cout << cit->first << ':' << cit->second << endl;
			++cit;
		}
	}
}