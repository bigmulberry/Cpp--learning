#define _CRT_SECURE_NO_WARNINGS 1
template<class T>
T Add(const T& t1, const T& t2)
{
	return t1 + t2;
}

//在函数定义的cpp文件中实例化，但比较麻烦，多一个特例就多一个定义
//template<>
//int Add<int>(const int& t1, const int& t2)
//{
//	return t1 + t2;
//}