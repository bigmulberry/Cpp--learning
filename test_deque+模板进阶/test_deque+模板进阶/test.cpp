#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <math.h>
#include <string>
#include <string.h>
using namespace std;


//deque类似于vector+list但是不能替代这两者
//deque作为stack和queue的容器适配器，是因为deque适合头尾的插入和删除，不适合大量的中间插入删除和大量的随机访问


//模板参数分为类型形参和非类型形参

//类型模板参数，传类型 A<int>
//非类型模板参数，传整型 

//#define N 1000
//template <class T=int,size_t N=10>
//class A
//{
//private:
//	T _a[N];
//};
//
//
//
//int main()
//{
//	A<int,100> aa1;//100
//	A<int,1000> aa2;//1000
//	A<int> aa3;//10
//	A<> aa4;//int,10
//	return 0;
//}

//模板的特化——默认函数模板或者类模板在特殊场景下不能正确处理逻辑，需要针对一些情况进行特殊化处理

//template <class T>
//bool IsEqual(const T& left,const T& right)
//{
//	return left == right;
//}
//
////针对字符串类型的特殊化处理--写一份函数模板的特化
//template<>
//bool IsEqual<const char* >(const char* const &left,const char* const &right)
//{
//	return strcmp(left, right)==0;
//}
//
//
//int main()
//{
//	cout << IsEqual<int>(1, 1) << endl;
//	cout << IsEqual(1.1, 1.2) << endl;
//
//	//利用该函数来比较字符串就需要特化
//	char p1[] = "hello";
//	char p2[] = "hello";  
//	cout << IsEqual(p1, p2) << endl;//这里比较的是栈空间的两个指针，无非内容一致，但是没有用来比较
//
//	const char *p3 = "hello";
//	const char *p4 = "hello";
//	cout << IsEqual(p3, p4) << endl;//这里比较的是常量区同一字段的地址，自然是相等的
//
//	cout << typeid(p1).name() << endl;
//	return 0;
//}


//类模板的特化
//template<class T1, class T2>
//class Data
//{
//public:
//	Data()
//	{
//		cout << "Data<T1,T2>" << endl;
//	}
//private:
//	T1 _d1;
//	T2 _d2;
//};
//
////类模板的全特化 
//template<>
//class Data<int, int>
//{
//public:
//	Data()
//	{
//		cout << "Data<int,int>" << endl;
//	}
//private:
//	int _d1;
//	int _d2;
//};
//
////类模板的偏特化
//template<class T1>
//class Data<T1,int>
//{
//public:
//	Data()
//	{
//		cout << "Data<T1,int>" << endl;
//	}
//private:
//	T1 _d1;
//	int _d2;
//};
//
////偏特化 还能指定参数的类型——模板参数是指针类型就调用此模板
//template <class T1, class T2>
//class Data<T1*, T2*>
//{
//public:
//	Data()
//	{
//		cout << "Data<T1*,T2*>" << endl;
//	}
//
//};
//
////偏特化——模板参数是引用类型就调用此模板
//template <class T1, class T2>
//class Data<T1&, T2&>
//{
//public:
//	Data()
//	{
//		cout << "Data<T1&,T2&>" << endl;
//	}
//
//};
//
//template <class T1, class T2>
//class Data<T1*, T2&>
//{
//public:
//	Data()
//	{
//		cout << "Data<T1*,T2&>" << endl;
//	}
//};
//
//int main()
//{
//	Data<int, double> d1;//原模板
//	Data<int, int> d2;//全特化
//	Data<char, int> d3;//部分偏特化
//	Data<int*, int*> d4;//范围偏特化（指针）
//	Data<int&, int&> d5;//范围偏特化（引用）
//	Data<int*, int&> d6;//范围偏特化（指针，引用）
//	return 0;
//}



//模板不支持分离编译
#include "vector.h"

int main()
{
	int a = 10;
	int b = 20;
	cout << Add(a, b) << endl;

	double c = 1.1;
	double d = 2.2;
	cout << Add(c, d) << endl;

	return 0;
}



//模板在编译时没有实例化——无法构成对应的符号表，在链接时找不到函数的定义
//总结：分离编译导致——定义模板的cpp没有实例化，实例化的cpp只有声明没有定义

//解决方法
//1显式实例化——在头文件中指定实例化
//2.更常见的解决方式——对于模板就不分离编译了——让用的地方有定义


