#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;

//class Person
//{
//public:
//	string _name;
//	int _age;
//	string _gender;
//	int _num = 10;
//
//
//};
//
//class Student1:public Person
//{
//public:
//	int _num=100;
//	int parent_num = Person::_num;
//};

//int main()
//{
//	Student1 s;
//	cout << s._num << endl;
//	cout << s.Person::_num << endl;
//
//	return 0;
//}

//
//class Student2 :private Person
//{
//protected:
//	int _stuid;
//};



//int main()
//{
//	
//	Student1 s;
//	Person p = s;
//	Person* pp = &s;
//	Person& rp = s;
//
//	//子类给父类 访问权限放大 出错
//	//Student2 s2;
//	//Person p2 = s2;
//	//Person* pp2 = &s2;
//	//Person& rp2 = s2;
//
//
//	//错误 父类对象赋值给子类对象
//	//Student s1 = p;
//	//Student s2 = (Student)p;
//	
//	
//	//Student s;
//	//Person* pp1 = &s;
//	//Person& rp1 = s;
//
//	//父类对象指针/引用 赋值给子类的对象和引用（强转）
//	//Student* ps1 = (Student*)pp1;
//	//Student& rs1 = (Student&)rp1;
//
//	//Person p2;
//	//Person* pp2 = &p2;
//	//Person& rp2 = p2;
//	//Student* ps2 = (Student*)pp2;
//	//Student& rs2 = (Student&)rp2;
//
//	//s = p;
//
//
//
//	//Teacher t;
//
//
//	return 0;
//}

//class A
//{
//public:
//	void func1()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B:public A
//{
//public:
//	void func(int i)
//	{
//		cout << "func(int i)" << endl;
//		func1();
//	}
//};
//
//int main()
//{
//	B b;
//	b.func(10);
//	b.A::func1();
//	return 0;
//}




//********************************************
//默认成员函数

//class Person
//{
//public:
//
//	Person()
//	{}
//	Person(string name ) : _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p) : _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//		return *this;
//	}
//
//	//~Person()
//	//{
//	//	cout << "~Person()" << endl;
//	//}
//
//	string _name;
//};
//
//class Stu :public Person
//{
//public:
//	//我们不写，使用编译器默认生成的
//	Stu(string name="Alice",int num = 100, string address = "Queen Avenue") 
//		:Person(name),_num(num), _address(address)
//	{
//		
//	}
//
//	Stu(const Stu& s)
//		:_num(s._num),
//		_address(s._address)
//	{
//
//	}
//
//	Stu& operator=(const Stu& s)
//	{
//		if (this != &s)
//		{
//			_num = s._num;
//			_address = s._address;
//			Person::operator=(s);
//		}
//		return *this;
//	}
//
//	~Stu()
//	{
//		Person::~Person();
//	}
//
//	int _num;//内置类型
//	string _address;
//};

//int main()
//{
//	Stu s1;
//	Stu s2(s1);
//	Stu s3;
//	s3 = s1;
//		
//	return 0;
//}


//***************************************************************************************
//友元不能继承
//class Student;
//class  Person
//{
//public:
//	friend void Print(const Person& p, const Student& s);
//
//
//protected:
//	int _num;
//};
//
//class  Student :public Person
//{
//protected:
//	string _name;
//};
//
//void Print(const Person& p, const Student& s)
//{
//	cout << p._num << endl;
//	cout << s._name << endl;
//}
//
//int main()
//{
//	Person P;
//	Student S;
//	Print(P, S);
//	return 0;
//}


//静态成员变量
//class  Person
//{
//public:
//	Person()
//	{
//		_count++;
//	}
//	Person(const Person& p)
//	{
//		_name = p._name;
//		_count++;
//	}
//
//protected:
//	string _name;
//public:
//	static int _count;
//};
//
//int Person::_count = 0;
//
//class  Student :public Person
//{
//protected:
//	int _num;
//};
//
//class  Graduate :public Student
//{
//protected:
//	int _year;
//};
//
//int main()
//{
//	Person p;
//	Student s;
//	Graduate g;
//
//	cout << Person::_count << endl;
//	cout << Student::_count << endl;
//	cout << Graduate::_count << endl;
//
//	cout <<&Person::_count << endl;
//	cout <<&Student::_count << endl;
//	cout <<&Graduate::_count << endl;
//}


//菱形继承

class Person
{
public:
	string _name;
};

class Student:public Person
{
protected:
	int _Stuid;//学号
};

class Teacher :public Person
{
protected:
	int _Jobid;//工号
};

class Assistant :public Student, public Teacher
{
protected:
	int _courseid;//课程
};

int main()
{

	Assistant a;
	a.Student::_name = "Dr.Peter";
	a.Teacher::_name = "Mr.Peter";
	return 0;
}

//*************************************
//虚拟继承
//class A
//{
//public:
//	int _a;
//};
//// class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//// class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}


// Car和BMW Car和Benz构成is-a的关系
//class Car {
//protected:
//	string _colour = "白色"; // 颜色
//	string _num = "陕ABIT00"; // 车牌号
//};
//class BMW : public Car {
//public:
//	void Drive() { cout << "好开-操控" << endl; }
//};
//class Benz : public Car {
//public:
//	void Drive() { cout << "好坐-舒适" << endl; }
//};
//// Tire和Car构成has-a的关系
//class Tire {
//protected:
//	string _brand = "Michelin"; // 品牌
//	size_t _size = 17; // 尺寸
//};
//class Car {
//protected:
//	string _colour = "白色"; // 颜色
//	string _num = "陕ABIT00"; // 车牌号
//	Tire _t; // 轮胎
//};