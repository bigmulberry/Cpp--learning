#define _CRT_SECURE_NO_WARNINGS 1
#include "Student.h" 
//用一个对象给同类的另外一个对象赋值。
//这个过程是通过成员复制来完成的，即，将一个对象的成员的值一个一个的复制给另外一个对象的对应成员。
void test()
{
	char buffer[] = "zhangsan";
	Student zhangsan(buffer, 'f', 1001, 20);
	Student lisi = zhangsan;//对象的赋值在析构函数时，要考虑释放空间的问题。
	//zhangsan在赋值给lisi时，会使pname指向同一个空间
	//但是析构时会使pname空间释放了两次


	Student wangwu(zhangsan);//对象的复制,参数一般为本类的对象引用
	//拷贝构造函数的特点：
	//	①、也是构造函数，所以函数名字就是类名字，并且无返回值；
	//	②、参数上有点特殊，参数是一般是本类的对象的引用；
	//	③、如果类没有提供拷贝构造函数，那么编译器会默认生成一个拷贝构造函数，
	//      作用比较单一，只是简单的将成员变量的值进行复制过去而已。
	//自己实现拷贝构造函数，可避免重复delete同一片空间的弊病

}

int main()
{
	
	test();
	return 0;
}