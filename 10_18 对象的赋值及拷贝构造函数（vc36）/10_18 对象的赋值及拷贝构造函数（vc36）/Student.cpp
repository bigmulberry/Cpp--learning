#include "Student.h"
#define _CRT_SECURE_NO_WARNINGS 1
Student::Student(char* t_name, char t_sex,int t_num, int t_age)
{
	//strcpy_s(name, t_name);
	int len = strlen(t_name);
	pname = new char[len+1];
	memset(pname, 0, len + 1);
	strcpy(pname,t_name);
	sex = t_sex;
	num = t_num;
	age = t_age;
}

Student::Student(Student& stud)
{
	age = stud.age;
	sex = stud.sex;
	num = stud.num;
	int len = strlen(stud.pname);
	pname = new char[len + 1];
	memset(pname, 0, len + 1);
	strcpy(pname, stud.pname);
}
Student::~Student()
{
	if (pname)
	{
		delete pname;
	}
	cout << "Student::~Student()" << endl;
}
