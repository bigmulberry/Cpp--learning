#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <iostream>
#include <string.h>
using namespace std;
class Student
{
public:
	//char name[50];
	char* pname;
	char sex;
	int num;
	int age;

	Student(char* t_name,char sex, int t_num, int t_age);
	~Student();

	//按理说 Student lisi(zhangsan); 这句代码也应该调用的是构造函数，对不对？
	// 没错，确实是，只不过这个构造函数有点特殊，名为拷贝构造函数。
	//原型如下：
	Student(Student& stud);
};

