#pragma once
#include<iostream>
#include <vector>
#include <climits>
#include <bitset>
using namespace std;

namespace mystd 
{
	template<size_t NUM>
	class bitset
	{
	public:

		bitset()
		{
			_bits.resize(NUM/8+1, 0);
		}

		void set(size_t pos)//设置指定位或者所有位
		{
			size_t i = pos / 8;
			size_t j = pos % 8;

			_bits[i] |= (1 << j);
		}

		void reset(size_t pos)//清空指定位或者所有位
		{
			size_t i = pos / 8;
			size_t j = pos % 8;
			
			_bits[i] &= (~(1 << j));
		}

		bool test(size_t pos)//获取指定位状态
		{
			size_t i = pos / 8;
			size_t j = pos % 8;

			return _bits[i] & (1 << j);
		}

		size_t count()
		{
			size_t count;
			for (auto e : _bits)
			{
				int n = e;
				while (n)
				{
					n = n & (n - 1);
					count++;
				}
			}
			return count;
		}

		bool none()
		{
			//遍历每个char
			for (auto e : _bits)
			{
				if (e != 0) //该整数中有位被设置
					return false;
			}
			return true; //全部整数都是0，则没有位被设置过
		}

		bool any()
		{
			return !none();
		}

		bool all()
		{
			size_t size = _bits.size();
			for (size_t i = 0; i < size - 1; ++i)
			{
				if (~_bits[i] != 0)//取反应该为0，否则取反之前不全为1
					return false;
			}

			for (size_t j = 0; j < NUM % 8; ++j)
			{
				if ((_bits[size - 1] & (1 << j)) == 0)
				{
					return false;
				}
			}
			return true;
		}

	private:
		vector<char> _bits;//位图

	};


	template<size_t N>
	class TwoBitset
	{
	public:
		void set(size_t pos)
		{
			if (!_bs1.test(pos) && !_bs2.test(pos))//00 -> 01
			{
				_bs1.set(pos);
			}
			else if (_bs1.test(pos) && !_bs2.test(pos))//01->10
			{
				//出现次数置为2
				_bs1.reset(pos);
				_bs2.set(pos);
			}
			//10 表示已经出现两次或以上，不用处理
		}

		//找出 出现次数为1（01）的整数
		void PrintOnceNum()
		{
			for (size_t i = 0; i < N; ++i)
			{
				if (_bs1.test(i))
				{
					cout << i << ' ';
				}
			}
		}

	private:
		std::bitset<N> _bs1;
		std::bitset<N> _bs2;
	};


	void test_bitset()
	{
		bitset<100> bs;
		bs.set(10);
		bs.set(20);
		bs.set(30);
		cout << bs.all() << endl;

		cout << bs.test(10) << endl;
		cout << bs.test(20) << endl;
		cout << bs.test(30) << endl;
		cout << bs.test(40) << endl;

		bs.reset(10);
		bs.reset(20);
		bs.reset(30);

		//bitset<UINT_MAX> bs;
		TwoBitset<16> tbs;
		srand((unsigned int)time(nullptr));
		tbs.set(1);
		tbs.set(2);
		tbs.set(3);
		tbs.PrintOnceNum();
	}
}