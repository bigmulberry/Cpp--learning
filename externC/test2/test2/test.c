#define _CRT_SECURE_NO_WARNINGS 1



//C调用C++
#include "../../DS/DS/Stack.h"//头文件展开后C语言是不认是extern "C"的！


#include <stdio.h>
#include <stdbool.h>
bool isValid(char* s) {
	//创建栈
	ST st;
	//初始化栈
	StackInit(&st);


	//遇到左括号填入栈中，遇到右括号与栈顶进行匹配，
	//如果匹配则弹栈
	//如果匹配不上返回false
	//如果遇到右括号时,栈为空,则返回false
	//遇到'\0',栈不为空时, 返回false

	while (*s)
	{
		if (*s == '(' || *s == '[' || *s == '{') //左括号压栈
		{
			StackPush(&st, *s);
		}
		else
		{
			if (StackEmpty(&st))//遇到右括号栈为空
			{
				return false;
			}
			else if ((*s == '}' && StackTop(&st) != '{')
				|| (*s == ')' && StackTop(&st) != '(')
				|| (*s == ']' && StackTop(&st) != '['))//匹配不上
			{
				return false;
			}
			else//匹配 弹栈
			{
				StackPop(&st);
			}
		}
		s++;
	}

	//判空，此时不为空则false
	if (!StackEmpty(&st))
	{
		return false;
	}

	//销毁栈 防止内存泄漏
	StackDestroy(&st);
	return true;
}



int main()
{
	char s1[] = "{[()]}";
	char s2[] = "{[}]";

	printf("%d\n", isValid(s1));
	printf("%d\n", isValid(s2));

	return 0;
}