#define _CRT_SECURE_NO_WARNINGS 1
#include "Stack.h"

//初始化栈
void StackInit(ST* ps)
{
	assert(ps);
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}

//销毁栈
void StackDestroy(ST* ps)
{
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->top = ps->capacity = 0;
}

void StackPush(ST* ps, STDataType x)//压栈
{
	if (ps->capacity == ps->top)
	{
		int newcapacity = (ps->capacity == 0) ? 4 : 2 * ps->capacity;
		STDataType* newspace = (STDataType*)realloc(ps->a, sizeof(STDataType) * newcapacity);
		assert(newspace);
		ps->a = newspace;
		ps->capacity=newcapacity;
	}
	ps->a[ps->top] = x;
	ps->top++;
}

void StackPop(ST* ps)//弹栈
{
	assert(ps);
	assert(!StackEmpty(ps));
	ps->top--;
}

//当前栈的大小
int StackSize(ST* ps)
{
	assert(ps);
	return ps->top;
}

//栈是否为空
bool StackEmpty(ST* ps)
{
	assert(ps);
	return ps->top == 0;
}

//获取栈顶元素
STDataType StackTop(ST* ps)
{
	assert(ps);

	return ps->a[ps->top-1];
}