#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>


typedef int STDataType;

typedef struct Stack
{
	STDataType* a;
	int top;//栈顶位置
	int capacity;//容量
}ST;

//C++调用C
//stack.c

////初始化栈
//void StackInit(ST* ps);
//
////销毁栈
//void StackDestroy(ST* ps);
//
//void StackPush(ST*ps,STDataType x);//压栈
//
//void StackPop(ST*ps);//弹栈
//
////当前栈的大小
//int StackSize(ST* ps);
//
////获取栈顶元素
//STDataType StackTop(ST* ps);
//
////栈是否为空
//bool StackEmpty(ST* ps);




//*************************************************

//C调用C++
//Stack.cpp

//C++静态库就会以C的函数名修饰规则去处理以下函数--一定注意避免重载

//extern 的两种包法

//第一种
#ifdef __cplusplus
extern "C"
{
#endif
	
	//初始化栈
	void StackInit(ST * ps);

	//销毁栈
	void StackDestroy(ST* ps);

	void StackPush(ST* ps, STDataType x);//压栈

	void StackPop(ST* ps);//弹栈

	//当前栈的大小
	int StackSize(ST* ps);

	//获取栈顶元素
	STDataType StackTop(ST* ps);

	//栈是否为空
	bool StackEmpty(ST* ps);
	
#ifdef __cplusplus
}
#endif 


//第二种
//#ifdef __cplusplus
//#define EXTERN_C extern "C"//以C的命名规则去修饰函数
//#else
//#define EXTERN_C
//#endif
//
////初始化栈
//EXTERN_C void StackInit(ST* ps);
//
////销毁栈
//EXTERN_C void StackDestroy(ST* ps);
//
//EXTERN_C void StackPush(ST* ps, STDataType x);//压栈
//
//EXTERN_C void StackPop(ST* ps);//弹栈
//
////当前栈的大小
//EXTERN_C int StackSize(ST* ps);
//
////获取栈顶元素
//EXTERN_C STDataType StackTop(ST* ps);
//
////栈是否为空
//EXTERN_C bool StackEmpty(ST* ps);




