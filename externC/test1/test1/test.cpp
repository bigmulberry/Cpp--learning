#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//extern "C"
// ����ͷ�ļ�
// �ڹ������������Ӿ�̬��

extern "C"
{
#include "../../DS/DS/Stack.h"
}


bool isValid(char* s) {
	//����ջ
	ST st;
	//��ʼ��ջ
	StackInit(&st);


	//��������������ջ�У�������������ջ������ƥ�䣬
	//���ƥ����ջ
	//���ƥ�䲻�Ϸ���false
	//�������������ʱ,ջΪ��,�򷵻�false
	//����'\0',ջ��Ϊ��ʱ, ����false


	while (*s)
	{
		if (*s == '(' || *s == '[' || *s == '{') //������ѹջ
		{
			StackPush(&st, *s);
		}
		else
		{
			if (StackEmpty(&st))//����������ջΪ��
			{
				return false;
			}
			else if ((*s == '}' && StackTop(&st) != '{')
				|| (*s == ')' && StackTop(&st) != '(')
				|| (*s == ']' && StackTop(&st) != '['))//ƥ�䲻��
			{
				return false;
			}
			else//ƥ�� ��ջ
			{
				StackPop(&st);
			}
		}
		s++;
	}

	//�пգ���ʱ��Ϊ����false
	if (!StackEmpty(&st))
	{
		return false;
	}

	//����ջ ��ֹ�ڴ�й©
	StackDestroy(&st);
	return true;
}




int main()
{
	char s1[] = "{[()]}";
	char s2[] = "{[}]";
	cout << isValid(s1) << endl;
	cout << isValid(s2) << endl;


	return 0;
}