#pragma once
#include <string.h>
#include <iostream>
#include <assert.h>
namespace sjl
{
	//支持增删查改

	class string
	{
	public:
		//构造函数
		string(const char* str = "") 
		{
			cout << "string()" << endl ;

			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		//传统写法：深拷贝 拷贝构造和赋值重载
		////拷贝构造
		//string(const string& s):_str(new char[strlen(s._str)+1])
		//{
		//	strcpy(_str, s._str);
		//}

		////赋值运算符重载
		//string& operator=(const string& s)
		//{
		//	//防止自己给自己复制
		//	if (this != &s)
		//	{
		//		//抛弃_str原本的空间
		//		delete[] _str;
		//		//重新分配空间
		//		_str = new char[strlen(s._str) + 1];
		//		strcpy(_str, s._str);
		//	}
		//	return *this;
		//}

		//将swap封装成函数
		void swap(string& s)
		{
			::swap(_str, s._str);      //域作用限定符:: 指定全局函数，避免就近原则调用命名空间中的swap↑
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}




		//现代写法
		//拷贝构造（交换指针思想 ）
		string(const string& s):_str(nullptr),_size(0),_capacity(0)    //这里的初始化列表一定要用nullptr初始化_str，否则交换后delete的tmp将是随机值
		{
			cout << "string(const string & s)" << endl << endl;

			string tmp(s._str);        //不能改变const修饰的s的值，这里调用构造函数，用s._str（const* char）初始化构造tmp
			//swap(_str, tmp._str);      //tmp指向的空间的指针，和_str指向其空间的指针互换，实现了空间的交换，_str得到了s._str的地址内容
			//swap(_size, tmp._size);
			//swap(_capacity,tmp._capacity);
			//我们将swap封装
			swap(tmp);


			//我们自己在命名空间（sjl）封装的swap，较之全局swap(*this,tmp),效率更高，只需要互换成员变量，避免开辟一个对象的空间进行对象互换
		}                              //tmp析构

		//赋值运算符重载
		string& operator=(string s)//函数传值，需先调用拷贝构造函数建立s，拷贝构造的复用。就没必要再建立tmp了
		{
			cout << "string& operator=(string s)" << endl << endl;    

			//swap(_str, s._str);
			//swap(_size, s._size);
			//swap(_capacity, s._capacity);
			//我们将swap封装
			swap(s);
			return *this;
		}
		//要么就是传引用&s，然后用临时string tmp记录下s，交换tmp和this的成员指针，然后delete tmp
		//要么就是传值s，直接交换拷贝来的函数参数（临时拷贝构造来的参数，但此时一定是有了拷贝函数才能这么干）
		//这里自己给自己拷贝，会导致空间内容不变，但是空间更换的情况。

		//实现遍历
		
		size_t size()const
		{
			return _size;
		}
		//不修改成员变量的话，多数用const进行修饰
		//这样可以兼容 const/非cosnt的参数 的函数

		//[]运算符重载
		//at函数也是访问元素，但是越界是抛异常
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		const char& operator[](size_t pos)const       //const string* this
		{
			assert(pos < _size);
			return _str[pos];
		}

		//当返回类型为引用时，意味着我们留下了修改成员变量的接口
		//但是有时我们会接收到const修饰的对象参数，那意味着不能留下修改成员变量的接口，同时返回的引用也需要以const修饰，防止调用[]后对const对象修改
		//于是我们要重载这类函数，带const的和不带const的

		//实现迭代器
		//实现可读可写
		typedef char* iterator;
		//实现只读
		typedef const char* const_iterator;

		iterator begin()
		{
			return _str;
		}

		const_iterator begin()const
		{
			return _str;
		}

		iterator end()
		{
			return _str+_size;
		}

		const iterator end()const
		{
			return _str + _size;
		}

		//增删查改---------------------------------------------------------------------------

		//增
		//开空间，扩展capacity
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n+1];
				strncpy(tmp, _str,_size+1);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}

		//开空间+初始化，扩展capacity 并且初始化空间，size 也要动
		//resize 的三种情况
		//①        n<_size
		//②  _size<n<_capacity
		//③        n>_capacity
		void resize(size_t n, char ch = '\0')
		{
			if (n < _size)
			{
				_size = n;
				_str[_size] = '\0';
			} 
			else
			{
				if (n > _capacity)//需要扩容
				{
					reserve(n);//复用reserve
				}
				memset(_str + _size, ch,_capacity-_size);
				_size = n ;
				_str[_size] = '\0';
			}
		}
		void push_back(char ch)
		{
			//查看空间是否足够
			if (_size == _capacity)
			{
				reserve(_capacity==0?4:_capacity * 2);

			}
			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';

		}

		void append(const char* str)
		{
			size_t len = _size + strlen(str);
			if (len > _capacity)
			{
				reserve(len); 
			}
			strncpy(_str + _size, str,strlen(str)+1);
			_size = len;
		}

		string& operator+=(const char ch)
		{
			push_back(ch);
			return *this;
		}

		string& operator+=(const char* s)
		{
			append(s);
			return *this;
		}

		//insert//在pos位置之前插入
		string& insert(size_t pos, char ch)
		{
			//确定pos的合法性
			assert(pos <= _size);
			//确定还有空间
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : 2 * _capacity);
			}

			//将pos及之后的字符都向后移动一位
			//方法一 遍历下标
			//size_t end = _size + 1;
			//while (pos < end)
			//{
			//	_str[end] = _str[end - 1];
			//	--end;
			//}

			//方法二 遍历指针
			char* end = _str + _size;
			while (end >= _str+pos)
			{
				*(end + 1) = *end;
				--end;
			}
			
			//插入字符
			_str[pos] = ch;
			_size += 1;
			return *this;
		}

		string& insert(size_t pos,const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (_capacity - _size < len)
			{
				reserve(_size+len);
			}

			//pos往后的字符往后移strlen(str)个字符位
			// 法一：遍历指针
			//char* end = _str + _size;
			//while (_str + pos <= end)
			//{
			//	*(end + len) = *end;
			//	--end;
			//}

			//法二：直接用memmove
			memmove(_str + pos + len, _str + pos, _size - pos+1);


			//插入字符串
			memmove(_str + pos, str, len);
			_size += len;
			return *this;
		}

		//删
		void pop_back()
		{
			_str[--_size] = '\0';
		}

		string& erase(size_t pos = 0, size_t  len=npos)
		{
			assert(pos <= _size);
			size_t leftLen = _size - pos;
			if (leftLen> len)
			{
				memmove(_str + pos, _str + pos + len, _size - pos - len+1);
				_size -= len;
			}
			else
			{
				_str[pos] = '\0';
				_size = pos;
			}
			resize(_size);

			return *this;
		}

		void clear()
		{
			resize(0);
		}

		//查
		//find
		size_t find(char ch, size_t pos = 0)const
		{
			for (size_t i = pos; i < _size; ++i)
			{
				if (ch == _str[i])
				{
					return i;
				}
			}
			return npos;
		}

		size_t find(const char* str, size_t pos = 0)const
		{
			assert(pos <= _size);
			char* locate = strstr(_str + pos, str);
			if (locate != NULL)
			{
				return locate-_str;
			}
			else
			{
				return npos;
			}
		}

		//rfind
		//复用find
		size_t rfind(char ch, size_t pos = npos)const
		{
			if (pos > _size) 
				pos = _size;
			size_t locate = find(ch);
			size_t flag = locate;
			while (locate <= pos)
			{
				flag = locate;
				locate = find(ch ,locate+1);
			}
			if (flag <= pos)
			{
				return flag;
			}
			else
			{
				return npos;
			}
		}

		size_t rfind(const char* str, size_t pos = npos)const
		{
			if (pos > _size)
				pos = _size;
			size_t locate = find(str);
			size_t flag = locate;
			while (locate <= pos)
			{
				flag = locate;
				locate = find(str, locate + 1);
			}
			if (flag <= pos)
			{
				return flag;
			}
			else
			{
				return npos;
			}
		}


		//改
		//opeartor[]

		//类的比较----------------------------------------------------------------------------------------------------------------
		friend bool operator<(const string& s1,const string& s2);
		friend bool operator>(const string& s1,const string& s2);
		friend bool operator<=(const string& s1, const string& s2);
		friend bool operator>=(const string& s1, const string& s2);
		friend bool operator==(const string& s1, const string& s2);
		friend bool operator!=(const string& s1, const string& s2);
		
		//输入输出的运算符重载---------------------------------------------------------------------------------------------------------
		//全局


		//实现c_str
		const char* c_str()const
		{
			return _str;
		}


		//析构函数
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		static const size_t npos;
	};

	//比较的非成员函数
	inline bool operator<(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) < 0;
	}

	inline bool operator>(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) > 0;
	}
	inline bool operator==(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) == 0;
	}
	inline bool operator>=(const string& s1, const string& s2)
	{
		return !(s1 < s2);
	}
	inline bool operator<=(const string& s1, const string& s2)
	{
		return (s1 < s2) || (s1 == s2);
	}
	inline bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}

	ostream& operator<<(ostream& out, const string& s)
	{
		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	} 

	istream& operator>>(istream& in,string& s)
	{
		s.clear();
		char ch;
		in.get(ch);
		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			in.get(ch);
		}
		return in;
	} 

	istream& getline(istream& in, string& s)
	{
		s.clear();
		char ch;
		in.get(ch);
		while (ch != '\n')
		{
			s += ch;
			in.get(ch);
		}
		return in;
	}

	const size_t string::npos = -1;

	void f(const string& s1)
	{
		for (size_t i = 0; i < s1.size(); ++i)
		{
			cout << s1[i] << " ";
		}
		cout << endl;
	}

	void test1_string()
	{
		string s1("hello world!");
		string s2(s1);
		cout << s1.c_str() << endl << endl;
		cout << s2.c_str() << endl << endl;

		string s3("hello");
		s1 = s3;
		cout << s3.c_str() << endl << endl;
		cout << s1.c_str() << endl << endl;

	}

	void test2_string()
	{
		string s1("hello world");
		//实现[]重载
		s1[0] = 'x';
		cout << s1[1] << endl;
		//cout << s1[100] << endl;
		cout << s1.c_str() << endl;

		//const的处理
		f(s1);

		//
		for (size_t i = 0; i < s1.size(); ++i)
		{
			cout << s1[i] << " ";
		}
		cout << endl;
	}

	void test3_string()
	{
		string s1("hello world");

		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		//范围for是由迭代器支持的
		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}

	void test4_string()
	{
		string s1("hello world");
		s1 += '!';
		cout << s1.c_str() << endl;
		s1.append("abcdefghijklmnopqrstuvwxyz");
  		cout << s1.c_str() << endl;

	}

	void test5_string()
	{
		string s1("hello");
		s1.resize(3);
		cout << s1.c_str() << endl;
		
		string s2("hello");
		s2.resize(12,'A');
		cout << s2.c_str() << endl;
	}

	void test6_string()
	{
		string s1("hello");
		s1.insert(0, 'x');
		cout << s1.c_str() << endl;
		s1.insert(1, " Al");
		cout << s1.c_str() << endl;
	}

	void test7_string()
	{
		string s1("hello world!!");
		s1.pop_back();
		cout << s1.c_str() << endl;
		s1.erase(5);
		cout << s1.c_str() << endl;

	}

	void test8_string()
	{
		string s1("hello world!!");
		cout << s1.find('e') << endl;
		cout << s1.find("world") << endl;

		string s2("abcabcabca");
		cout << s2.rfind('a',8) << endl;
		cout << s2.rfind("abc", 8) << endl;
	}

	void test9_string()
	{
		string s1("abcd");
		string s2("bcd");
		cout << (s1 < s2) << endl;
		cout << (s1 <= s2) << endl;
		cout << (s1 > s2) << endl;
		cout << (s1 >= s2) << endl;
		cout << (s1 == s2) << endl;
		cout << (s1 != s2) << endl; 
	}

	void test10_string()
	{
		string s1("hello");
		s1.resize(20);
		s1 += "!!!";
		cout << s1 << endl;
		cout << s1.c_str() << endl;

		string s2("123");
		string s3("456");
		cin >> s2 >> s3;
		cout << s2 << s3 << endl;

		string s4;
		getline(cin, s4);
		cout << s4 << endl;

		std::string s5("hello");//当字符串<16时，字符串存在buf中（实际容量为16），占对象的总大小
		std::string s6("helloaaaaaaaaaaaaaaaaaaaaaaaaa");//字符串>16时，存在ptr指向的堆空间上，指针只占有对象的4个字节空间
		//std::string 的private成员：char _Buf[16] , char* _Ptr , size_t _Mysize , size_t _MyRes

	}
}

//关于string深浅拷贝的解决方案：
//1.深拷贝
//2.引用计数浅拷贝+写时拷贝
//  当有新对象拷贝时，如果是只读，那么就直接拷贝指针，计数器+1，
// 但是一旦需要写入或者修改时（若这时引用计数！=1，就不能原地修改，因为这块空间是和多个对象共享的），
// 此时不得不分配空间，同时计数器-1，在申请空间拷贝后再修改，
//计数器为1后，则可原地修改，而且析构可释放空间
