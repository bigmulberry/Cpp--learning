#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <vector>
using namespace std;


//int main()
//{
//	//string的7个构造 <C++98>
//	//***********************
//	string s0("Initial string");
//
//
//	//default (1)	
//	//string();//无参
//	string s1;
//	cout << "s1:" << s1 << endl;
//
//	//copy(2)
//	//string(const string & str);//拷贝构造
//	string s2(s0);
//	cout << "s2:" << s2 << endl;
//
//	 
//	//substring(3)
//	//string(const string & str, size_t pos, size_t len = npos);
//	//子字符串，从第pos个字符开始，长度为len（超出长度，则直到str尾部停止）。pos越界会报警，len越界会到结尾停下
//	//如果len是-1，或者不给参数使用默认npos（string的静态成员变量，类型是size_t，值为-1，实际上也是让len为极大值），则自动到结尾
//	string s3(s0, 8);
//	//string s3(s0, 8，-1);
//	//string s3(s0, 8,string::npos);
//	//string s3(s0, 8,string().npos);//匿名变量突破类域访问静态成员变量
//	cout << "s3:" << s3 << endl;
//
//
//	//from c - string(4)
//	//string(const char* s);
//	string s4("a character sequence");
//	//string s4="a character sequence";//隐式类型转换编译器优化后的拷贝构造
//	cout << "s4:" << s4 << endl;
//
//
//	//from sequence(5)
//	//string(const char* s, size_t n);//字符序列s的前n个字符
//	string s5("another one", 7);
//	cout << "s5:" << s5 << endl;
//
//	
//	//fill(6)
//	//string(size_t n, char c);//n个字符c
//	string s6(10, 'x');
//	cout << "s6:" << s6 << endl;
//
//	//range(7)
//	//template <class InputIterator>
//	//string(InputIterator first, InputIterator last);//两个位置间的子字符串
//	string s7(s0.begin(), s0.begin() + 7);  
//	cout << "s7:" << s7 << endl;
//	
//	return 0;
//}


//********************

//遍历string 

//int main()
//{
//	
//
//	//三种遍历方式
//	//************************************************************************************
//	//1.下标+[]
//	string s1("hello world");
//	for (size_t i=0; i < s1.size(); i++)
//	{
//		cout << s1[i] << " ";
//	}
//	cout << endl;
//	//Both string::sizeand string::length are synonymsand return the exact same value.
//	for (size_t i = 0; i < s1.length(); i++)
//	{
//		cout << s1.operator[](i) << " ";
//		//[]运算符重载了两个版本，返回的是引用，还可以进行修改，如果参数是const则返回const的引用
//
//		//std::string::at 也具有下标访问的功能
//		cout << s1.at(i) << " ";
//
//	}
//	cout << endl;
//	//s1[20];//越界断言
//	//s1.at(20);//越界抛异常
//
//	//**************************************************************************************
//	//2.迭代器
//	string s2("hello universe");
//	//[begin(),end()]    end不是返回最后一个位置的迭代器，而是返回最后一个位置下一个位置的迭代器
//	//C++的迭代器一般给的是左闭右开的区间
//	//迭代器是类似指针的东西
//	string::iterator it = s2.begin();
//	while (it != s2.end())//这里多用不等于号
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	//迭代器的意义：像string vector支持[]遍历，但list map等容器不支持[]
//	//我们就用迭代器遍历，所以迭代器是统一的遍历方式
//
//	vector<int> v = { 1,2,3,4,5 };
//	vector<int>::iterator vit = v.begin();
//	while (vit != v.end())
//	{
//		cout << *vit << ' ';
//		vit++;
//	}
//	cout << endl;
//
//	//反向迭代器
//	//同时（正/反）迭代器可读可写
//	string s4("12345678");
//	string::reverse_iterator rit = s4.rbegin();
//	while (rit != s4.rend())
//	{
//		*rit += 1;
//		cout << *rit << " ";
//		rit++;
//	}
//	cout << endl;
//	  
//	//begin的声明
//	//iterator begin();
//	//const_iterator begin() const;
//	 
//	//If the string object is const - qualified, the function returns a const_iterator.Otherwise, it returns an iterator.
//	//const对象只能用const_iterator去遍历——只读
//	const string s5(s4);
//	string::const_reverse_iterator crit = s5.rbegin();
//	while (crit != s5.rend())
//	{
//		cout << *crit << " ";
//		crit++;
//	}
//	cout << endl;
//
//	//begin end rbegin rend 将会根据string是否为const  来判断是否返回带const的迭代器
//	//在c++11标准中 cbegin cend crbegin crend 返回的是const迭代器,比较显目
//	
//	//总结：1.iterator 2.const_iterator  3.reverse_iterator  4.const_reverse_iterator
//
//
//	//**************************************************************************************
//	//<C++11> 提供范围for
//	string s3("goodbye");
//	for (auto c : s3)//这里auto的实际类型是char，写成char也可以
//	{
//		cout << c << " ";
//	}
//	cout << endl;
//
//	//如果需要在遍历时改变s3中的内容,则添加引用
//	for (auto& c : s3)
//	{
//		c += 1;
//		cout << c << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//*****************************************************
//front 
//back
 
//int main()
//{
//	string s1("1234");
//	cout << s1.front() << endl;//front 取第一个字符
//	cout << s1.back() << endl;//back 取最后一个字符
//	//但用的少
//
//	cout << s1[0] << endl;
//	cout << s1[s1.size()-1] << endl;
//
//
//	return 0;
//}


//****************************************
//插入单个字符 push_back
//弹出单个字符 pop_back
//添加字符串append
//+= 运算符重载，添加字符串或单个字符   用的最多
//insert 插入字符串(或其字串)或单个字符 可选择任意位置进行插入，入如果插入位置超过string 的长度会抛异常
//erase

//int main()
//{
//	string s1("12345");
//
//	s1.push_back('6');
//	s1.push_back('7');
//	cout << s1 << endl;
//
//	s1.pop_back();
//	s1.pop_back();
//	s1.pop_back();
//	cout << s1 << endl;
//
//	//但是插入的效率太低了
//	//append 插入char* 字符串
//	s1.append("56");
//	cout << s1 << endl;
//	//还可以指定添加字符串的头n个字符
//	s1.append("789",2);
//	cout << s1 << endl;
//
//
//	//append中可以迭代器作为参数
//	string s2("hello ");
//	string s3("world!!");
//	s2.append(s3.begin(), s3.end());
//	cout << s2 << endl;
//	
//	// +=
//	string s4 = "good";
//	s4 += ' ';//底层调用push_back
//	s4 += "bye";//底层调用append
//	cout << s4 << endl;
//
//	//insert
//	string s5("fk");
//	s5.insert(1, "**");
//	cout << s5 << endl;
//	
//
//	//erase
//	string s6("show me code");
//	//s6.erase();//默认的参数是删完的 string& erase (size_t pos = 0, size_t len = npos);
//	//s6.erase(s6.begin() + 4, s6.begin() + 7);//可传入迭代器  iterator erase (iterator first, iterator last);
//	s6.erase(s6.begin());//删去单个字符 
//	cout << s6 << endl;
//
// //尽量少用insert和erase，因为底层实现是数组，头部或中间插入需要挪动数据
// 
//	return 0;
//}


//************************************************************************************
//max_size
//capacity
//empty
//clear

//int main()
//{
//	string s1;
//	string s2("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
//	
//	//
//	cout << s1.max_size() << endl;
//	cout << s2.max_size() << endl;
//
//	//size只计入有效数字的大小 不计入结尾\0
//	cout << s1.size() << endl;
//	cout << s2.size() << endl;
//
//
//	cout << s1.capacity() << endl;//15个字节 最后一个空间给\0
//	cout << s2.capacity() << endl;
//
//	//emtpy  判断是否为空
//	cout << s1.empty() << endl;
//	cout << s2.empty() << endl;
//
//	//clear 清空字符串
//	s2.clear();
//	cout << s2.size() << endl;//长度为零
//	cout << s2.capacity() << endl;//但是空间没有释放或者缩小
//
//	return 0;
//}


//********************************************************************************
//两个重要的接口：
//reserve 
//resize
//int main()
//{
//	string s1("abcdefg");
//	cout <<"s1.size()=" << s1.size() << endl;
//	cout << "s1.capacity()=" << s1.capacity() << endl;
//
//	//resize 将字符串缩放到n个字节大小 ，若有字符则用来填充后续扩充的空间，没有则填充\0
//	//声明
//	//void resize (size_t n);
//	//void resize(size_t n, char c);
//
//
//	s1.resize(30,'z');
//
//	cout << "*******after resize******" << endl;	
//	cout << s1 << endl;
//	cout << "s1.size()=" << s1.size() << endl;
//	cout << "s1.capacity()=" << s1.capacity() << endl;
//
//	s1.resize(140);
//	cout << "*******after resize******" << endl;
//	cout << "s1.size()=" << s1.size() << endl;
//	cout << "s1.capacity()=" << s1.capacity() << endl;//capacity变大后是不一定跟着缩小的（取决于编译器）
//
//
//	//reserve  改变容量capacity 不改变size
//	string s2("abcdefg");
//
//	s2.reserve(140); 
//	cout << "*******after reserve******" << endl;
// 	cout << s2 << endl;
//	cout << "s2.size()=" << s2.size() << endl;
//	cout << "s2.capacity()=" << s2.capacity() << endl;
//	//resize 和reserve的区别
//	//resize是直接填满n个空间，如果在添加数据是在n+1个位置开始
//	//reserve扩充的则是可利用的空间
//	return 0;
//}


//***************************************************************
//c_str   获取c形式的字符串（以\0结束的字符串 ） 返回的是字符数组的指针

//int main()
//{
//	string s1("hello \0world");//来源于C的初始化，            string (const char* s);           ，只获取了\0前的字符串
//
//	cout << s1 << endl; //调用 operator<<(cout,s1)  这时string类的运算符重载
//	cout << s1.c_str() << endl; //调用 operator<<(cout,const char*)  内置类型的运算符重载
//
//
//	cout << sizeof(s1) << endl;
//	cout << sizeof(s1.c_str()) << endl;
//
//	s1.resize(20);
//	s1 += "!!!";
//	cout << s1 << endl; //string的输出是会滤掉当中所有的\0的
//	cout << s1.c_str() << endl;//但是C字符串只到\0就结束了
//	//c_str可配合使用
//
//	string s2(s1);//在用string类为string初始化时，string (const string& str);，是获取全部的size内容的，不管当中有多少个\0
//	cout << "s2:" << s2 << endl;
//	return 0;
//}


//****************************************************************************************
//find
//rfind
//replace
//substr

//int main()
//{
//	//find
//	//假设要求去取出文件的后缀
//	string filename = "test.cpp";
//	size_t pos = filename.find('.');
//	if (pos != string::npos)
//	{
//		string suff(filename, pos);//拷贝构造的一种形式
//		cout << suff << endl;
//
//		//使用substr可以起到相同的效果
//		//string substr (size_t pos = 0, size_t len = npos) const;
//		string suff2 = filename.substr(pos);
//		cout << suff2 << endl;
//
//	}
//
//	//另一种情况 
//	string s2 = "test.cpp.zip";
//	//已上述方式取后缀会得到 .cpp.zip
//	//于是我们需从后往前找
//	size_t pos2 = s2.rfind('.');
//	if (pos2 != string::npos)
//	{
//		string suff3 = s2.substr(pos2);
//	}
//
//	//replace 替换选定字符
//	string str = "The sixth sick sheik's sixth sheep's sick.";
//	string key="sixth";
//
//	size_t found = str.rfind(key);
//	if (found != string::npos)
//	{
//		str.replace(found, key.size(), "seventh");
//		cout << str << endl;
//	}
//
//	return 0;
//}



//任务  给出网址挑出域名和协议
//string GetDomain(const string& url)
//{
//	size_t start = url.find("://") + 3;
//	
//	if (start != string::npos)
//	{
//		size_t end = url.find('/', start);
//		if (end != string::npos)
//		{
//			return url.substr(start, end - start);
//		}
//		else
//		{
//			return "not found";
//		}
//	}
//	else
//	{
//		return "not found";
//	}
//}
//
//string GetProtocol(const string& url)
//{
//	string key = "://";
//	size_t pos = url.find(key);
//	if (pos != string::npos)
//	{
//		return url.substr(0, pos-0);
//	}
//	else
//	{
//		return "not found";
//	}
//}
//
//int main()
//{
//	string url1 = "http://www.cplusplus.com/reference/string/string/substr/";
//	string url2 = "https://fanyi.youdao.com/?keyfrom=fanyi-new.logo";
//	cout << GetDomain(url1) << endl;
//	cout << GetProtocol(url1) << endl;
//
//	cout << GetDomain(url2) << endl;
//	cout << GetProtocol(url2) << endl; 
//	return 0;
//}



//********************************************
//当用输入流为string对象赋值时，遇到空格会跳向赋值给下一个对象
//如果需要将带有空格的字符串赋值给一个string对象，则需要使用函数——getline

//int main()
//{
//	string s1;
//	getline(cin, s1);
//	cout << s1 << endl;
//
//	return 0;
//}


//***************************************************
//#include <locale>

//class Solution {
//public:
//	bool isPalindrome(string s) {
//		for (auto& x : s)
//		{
//			x=tolower(x);
//		}
//		int left = 0, right = s.size() - 1;
//		while (left < right)
//		{
//			while (left < right && !isalnum(s[left]))
//			{
//				++left;
//			}
//			while (left < right && !isalnum(s[right]))
//			{
//				--right;
//			}
//			if (s[right] != s[left])
//			{
//				return false;
//			}
//			else
//			{
//				++left;
//				--right;
//			}
//		}
//		return true;
//	}
//};
//
//int main()
//{
//	string s1 = "A man, a plan, a canal : Panama";
//	Solution A;
//	A.isPalindrome(s1);
//	return 0;
//}


//**************************************************************
//class Solution {
//public:
//	string addStrings(string num1, string num2) {
//		string ret;
//		int end1 = num1.size() - 1, end2 = num2.size() - 1;
//		int next = 0;
//		while (end1 >= 0 || end2 >= 0)
//		{
//			int sum = 0;
//			if (end1 >= 0)
//			{
//				sum += num1[end1--] - '0';
//			}
//
//			if (end2 >= 0)
//			{
//				sum += num2[end2--] - '0';
//			}
//			sum += next;
//			if (sum > 9)
//			{
//				sum -= 10;
//				next = 1;
//			}
//			else
//			{
//				next = 0;
//			}
//			ret += (sum+'0');
//		}
//		if (next == 1)
//		{
//			ret += next + '0';
//		}
//		reverse(ret.begin(), ret.end());
//		return ret;
//	}
//};
//
//int main()
//{
//	Solution A;
//	string s1("456");
//	string s2("77");
//	cout << A.addStrings(s1, s2) << endl;
//	return 0;
//}

 
//**********************************************************************************
//自己实现string
#include "string.h"


int main()
{
	//sjl::test1_string();
	//sjl::test2_string();
	//sjl::test3_string();
	//sjl::test4_string();
	//sjl::test5_string();
	//sjl::test6_string();
	//sjl::test7_string();
	//sjl::test8_string();
	//sjl::test9_string();
	sjl::test10_string();



	return 0;
}
























