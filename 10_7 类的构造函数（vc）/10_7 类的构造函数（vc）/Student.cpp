
#include "Student.h"
#include <string>
Student::Student()
{
	memset(name, 0, 50);
	sex = 'm';
	num = 0;
	age = 0;
}

Student::Student(int num_value)
{
	num = num_value;
	cout << "学号初始化构造函数" << endl;
}

Student::Student(char* t_name, int t_age, int t_num, char t_sex)
{
	strcpy(name, t_name);
	age = t_age;
	num = t_num;
	sex = t_sex;
	cout << "全部初始化构造函数" << endl;
}