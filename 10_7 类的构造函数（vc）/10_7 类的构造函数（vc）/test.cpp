#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;
#include "Student.h"
//构造函数——对象的初始化
//无需用户主动调用，构造函数会在对象被建立时自动被调用，是用来处理对象的初始化操作


//注意事项
//构造函数的名字必须与类同名，不能随意命名。
//构造函数不具有任何类型，没有返回值，连void也不是。
//构造函数不需要用户调用，也不应该调用，对象建立时将自动有系统调用
//如果没有定义构造函数，系统将自动生成默认的构造函数，不过函数体是空的

int main()
{
	Student s1;//调用默认的构造函数，不带参数的构造函数
	//Student s1(); //同理

	Student s2(250);//带参数的构造函数
	char buffer[] = { 's','a','d','\0' };
	Student s3(buffer, 18, 1001, 'm');
	return 0;
}