#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <iostream>
using namespace std;

class Student
{
private:
	char name[50];
	int age;
	int num;
	char sex;
public:
	Student();//构造函数,与类名同名，无返回值
	Student(int num_value);//带参数的构造函数
	Student(char* t_name,int t_age,int t_num,char t_sex);
};

