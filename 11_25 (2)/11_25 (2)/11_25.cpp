#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//void Func(int a=10,int b=20,int c=30)//缺省参数
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl<<endl;
//}
//
//
//void Func1(int a, int b, int c = 30)//缺省部分参数，必须从右往左缺省，必须连续缺省
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl << endl;
//}
//
//int main()
//{
//	//        << --流插入
//	//        >> 流提取运算符
//	
//	//缺省参数 所有的调用函数，不能跳过传参
//	Func();
//	Func(1);
//	Func(1,2);
//	Func(1,2,3);
//
//	//半缺省
//
//
//	return 0;
//}



#include "Stack.h"

//int main()
//{
//	Stack st;
//	//缺省函数不能同时放在函数的声明和定义中，只能放在其中一者身上
//	//缺省参数一般放在头文件中声明，如果放在定义函数上面，那么在使用函数时，缺省函数就失去了他的意义，
//	//因为根据声明，参数是无法缺省的
//
//	//StackInit(&st,4);
//	printf("%d\n", a);
//	return 0;
//}



//函数重载
void Func(int a, double b)
{
	cout << a<<endl;
	cout << b << endl << endl;
}

void Func(double a, int b)
{
	cout << a << endl;
	cout << b << endl << endl;
}

void Func1()
{
	cout << "Func1" << endl;
}

void Func1(int a=0)
{
	cout << "Func1" << endl;
}


int main()
{
	Func(1.0, 2);
	Func1();//缺省参数不能构成重载
}

//预处理：头文件展开，宏替换，条件编译，删注释
//编译：检查语法，生成汇编代码
//汇编：汇编代码转换成二进制的机器码
//链接：