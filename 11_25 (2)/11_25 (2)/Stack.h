#pragma once
#include <stdio.h>
#include <stdlib.h>

struct Stack
{
	int* a;
	int top;
	int capacity;
};
//extern int a;

static int a = 0;//加上static是能在名字对应的源文件中可见，不会造成重定义，static只能适用于自己的源文件范围

void StackInit(struct Stack* ps,int capacity=0);

void StackPush(struct Stack* ps, int x);
