#pragma once
#include <utility>
namespace CloseHash
{
	enum State
	{
		EMPTY,
		EXIST,
		DELETE
	};

	template <class K,class V>
	struct HashData
	{
		pair<K, V> _kv;
		State _state=EMPTY;
	};

	template <class K>
	struct Hash
	{
		 const size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	template<>
	struct Hash<string>
	{
		const size_t operator()(const string& key)
		{
			size_t hash = 0;
			// 把字符串的所有字母加起来   hash = hash*131 + key[i]
			for (size_t i = 0; i < key.size(); ++i)
			{
				hash *= 131;
				hash += key[i];
			}
			return hash;
		}
	};

	template <class K, class V,class HashFunc=Hash<K>>//HashFunc负责将key值（可能为string）转换为int，方便取余
	class HashTable
	{
	public:
		bool insert(const pair<K,V>&  kv)
		{
			if (find(kv.first) != nullptr)//防止重复
			{
				return false;
			}

			//扩容逻辑
			//初始时是一个空表，则resize空间。
			if (_table.size() == 0)
			{
				_table.resize(10);
			}
			else if ( 10 * _n / _table.size() > 7)//或者负载因子超过0.7，则需要换一个更大的表
			{
				//法一 新建一个table，然后将元素转移
				//vector<HashData> newtable;
				//newtable.resize(2 * _table.size());
				//for (auto& e : _table)
				//{
				//	if (e._state == EXIST)//因为新表的容量更大，所以元素位置会变化
				//	{
				//		//重新按照线性探测或二次探测规则在新表中插入
				//	}
				//}

				//法二 新建一个HashTable类(推荐)
				HashTable<K, V,HashFunc> newHT;
				newHT._table.resize(2*_table.size());
				for (const auto& e : _table)
				{
					if (e._state == EXIST)
					{
						newHT.insert(e._kv);//复用了该类的insert成员函数
					}
				}
				_table.swap(newHT._table);
			}

			//插入新值的逻辑
			HashFunc hf;//仿函数 将key转换为int
			size_t start = hf(kv.first)%_table.size();
			size_t index = start;
			size_t i = 1;
			while (_table[index]._state == EXIST)
			{
				index =start+ i;//线性探测
				//index=start+i*i;//二次探测
				index %= _table.size();
				++i;
			}
			_table[index]._kv = kv;
			_table[index]._state = EXIST;
			_n++;

			return true;
		}

		HashData<K, V>* find(const K& key)
		{
			if (_table.size() == 0)
			{
				return nullptr;
			}
			HashFunc hf;
			size_t start = hf(key) % _table.size();
			size_t index = start;
			int i = 1;
			while (_table[index]._state != EMPTY)
			{
				if (_table[index]._kv.first == key && _table[index]._state==EXIST)
				{
					return &_table[index];
				}
				index = start+i;
				//index = start+i*i;//二次探测
				i++;
				index %= _table.size();
			}
			return nullptr;
		}

		bool erase(const K& key)
		{
			HashData<K, V>* del = find(key);
			if (del == nullptr)//没有该点
			{
				return false;
			}
			del->_state = DELETE;
			_n--;
			return true;
		}

	private:
		vector<HashData<K,V>> _table;//容器
		size_t _n=0;//实际存储的元素个数
	};

}


namespace OpenHash
{
	template<class K>
	struct Hash
	{
		const size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	template<>
	struct Hash<string>
	{
		const size_t operator()(const string& key)
		{
			size_t hash=0;
			for (size_t i = 0; i < key.size(); ++i)
			{
				hash *= 131;
				hash += key[i];
			}
			return hash;
		}
	};


	template <class K,class V>
	struct HashData
	{
		//链表指针
		HashData<K, V>* _next;
		//链表结点数据
		pair<K, V> _kv;

		HashData(const pair<K, V>& kv) :_kv(kv), _next(nullptr)
		{

		}
	};

	template <class K, class V,class Hash=Hash<K> >
	class HashTable
	{
		typedef HashData<K, V> Node;
	public:

		Node* find(const K& key)
		{
			Hash hf;//key转size_t 仿函数实例化
			if (_table.size() == 0)//元素数量为0，无法查找
			{
				return nullptr;
			}
			//计算哈希地址
			size_t index = hf(key) % _table.size();
			Node* cur = _table[index];
			while (cur)//遍历哈希桶
			{
				if (cur->_kv.first == key)
				{
					return cur;
				}
				cur = cur->_next;
			}
			return nullptr;
		}

		bool insert(const pair<K,V>& kv)
		{
			if (find(kv.first))
			{
				return false;
			}
			Hash hf;//key转size_t仿函数实例化


			//负载因子为1 增容
			if (_n == _table.size())
			{
				size_t newsize = _n == 0 ? 10 : 2 * _table.size();
				vector<Node*> newtable;
				newtable.resize(newsize);
				for (size_t i = 0; i < _n; ++i)
				{
					if (_table[i])//桶不空，遍历该哈希桶
					{
						//拿到这个桶的头结点
						Node* cur = _table[i];
						while (cur)
						{
							Node* next = cur->_next;

							//重新计算映射位置
							size_t index = hf(cur->_kv.first) % newtable.size();
							//插入至新表
							cur->_next = newtable[index];
							newtable[index] = cur;
							cur = next;
						}
					}
				}
				_table.swap(newtable);//交换哈希表
			}


			size_t index = hf(kv.first) % _table.size();
			Node* newnode = new Node(kv);

			//头插
			newnode->_next = _table[index];
			_table[index]= newnode;
			++_n;

			return true;
		}

		bool erase(const K& key)
		{
			HashFunc hf;
			//1.计算哈希地址
			size_t index = hf(key) % _table.size();

			//2.在桶中寻找待删除结点
			Node* cur = _table[index];
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					if (prev == nullptr)//cur为头结点
					{
						_table[index]= cur->_next;
					}
					else//cur为中间结点
					{
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}
	

	private:
		vector<Node*> _table;//指针数组
		size_t _n=0;//实际存放的元素数量
	};



}