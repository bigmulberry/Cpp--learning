#pragma once
class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}
	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}
	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}
	friend ostream& operator<<(ostream& _cout, const Date& d)
	{
		_cout << d._year << "-" << d._month << "-" << d._day;
		return _cout;
	}
private:
	int _year;
	int _month;
	int _day;
};


template<class T>
class Less
{
public:
	bool operator()(const T& a,const T& b)
	{
		return a < b;
	}
};

//类的特化，针对特殊的类型

template<>
class Less<Date*>
{
public:
	bool operator()( Date* const& a,  Date* const& b)
	{
		return *a < *b;
	}
};



template<class T>
class Greater
{
public:
	bool operator()(const T& a,const T& b)
	{
		return a > b;
	}
};

template<class T,class Container = vector<T>,class Compare=Less<T>>
class my_priority_queue
{
public:
	template<class InputIterator>
	my_priority_queue(InputIterator first, InputIterator last) :_con(first, last)
	{
		for (int i = (_con.size() - 1 - 1) / 2; i >= 0; --i)
		{
			AdjustDown(i);
		}
	}

	my_priority_queue()
	{}

	void push(const T& x)
	{
		_con.push_back(x);
		AdjustUp( _con.size()-1);
	}

	void pop()
	{
		swap(_con[0], _con[_con.size() - 1]);
		_con.pop_back();
		AdjustDown(0);
	}

	const T& top()
	{
		return _con.front();
	}

	size_t size()
	{
		return _con.size();
	}

	bool empty()
	{
		return _con.empty();
	}

private:
	void AdjustUp(size_t child)//传入尾部元素下标
	{
		Compare com;

		size_t parent = (child - 1) / 2;
		while (child>0)
		{
			if (com(_con[parent] , _con[child]))//建大堆
			{
				swap(_con[child], _con[parent]);
			}
			else
			{
				break;
			}
			child = parent;
			parent= (child - 1) / 2;
		}
	}

	void AdjustDown(size_t parent)
	{
		Compare com;
		size_t child = parent * 2 + 1;
		while (child < _con.size())
		{
			if (child + 1 < _con.size() && com(_con[child ] , _con[child+1]))//建大堆
			{
				child++;
			}
			if (com(_con[parent] , _con[child]))
			{
				swap(_con[child], _con[parent]);
				parent = child;
			}
			else
			{
				break;
			}
		}
	}

	Container _con;
};