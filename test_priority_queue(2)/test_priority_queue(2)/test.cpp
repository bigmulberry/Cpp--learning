#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;
#include<deque>
#include <vector>
#include <algorithm>
#include <functional>
#include <queue>
#include "mystack.h"
#include "myqueue.h"
#include "my_priority_queue.h"

//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{}
//	bool operator<(const Date& d)const
//	{
//		return (_year < d._year) ||
//			(_year == d._year && _month < d._month) ||
//			(_year == d._year && _month == d._month && _day < d._day);
//	}
//	bool operator>(const Date& d)const
//	{
//		return (_year > d._year) ||
//			(_year == d._year && _month > d._month) ||
//			(_year == d._year && _month == d._month && _day > d._day);
//	}
//	friend ostream& operator<<(ostream& _cout, const Date& d)
//	{
//		_cout << d._year << "-" << d._month << "-" << d._day;
//		return _cout;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};

class LessPDate
{
public:
	bool operator()(const Date* d1, const Date* d2)
	{
		return *d1 < *d2;
	}
};


int main()
{
	//my_priority_queue<int,vector<int>,Greater<int>> p;
	//p.push(1);
	//p.push(7);
	//p.push(5);
	//p.push(2);
	//p.pop();
	//p.pop();
	//p.pop();


	//my_priority_queue<Date*,vector<Date*>,LessPDate> days;
	my_priority_queue<Date*,vector<Date*>,Less<Date*>> days;

	days.push(new Date(2022, 3, 16));
	days.push(new Date(2023, 3, 23));
	days.push(new Date(2021, 10, 16));

	while (!days.empty())
	{
		cout << *days.top() << endl;
		days.pop();
	}
	cout << endl;

	/*priority_queue<int, vector<int>, less<int>> p1;
	p1.push(1);
	p1.push(3);
	p1.push(3);
	p1.push(6);
	p1.push(9);
	cout << "�º���less��";
	while (!p1.empty())
	{
		cout << p1.top()<<' ';
		p1.pop();
	}
	cout << endl;

	priority_queue<int, vector<int>, greater<int>> p2;
	p2.push(1);
	p2.push(3);
	p2.push(3);
	p2.push(6);
	p2.push(9);
	cout << "�º���greater��";
	while (!p2.empty())
	{
		cout << p2.top() << ' ';
		p2.pop();
	}
	cout << endl;

	int myints[] = { 10,20,30,5,15 };
	std::vector<int> v(myints, myints + 5);

	std::make_heap(v.begin(), v.end());
	std::cout << "initial max heap   : " << v.front() << '\n';

	std::pop_heap(v.begin(), v.end());
	v.pop_back();
	std::cout << "max heap after pop : " << v.front() << '\n';

	v.push_back(99);
	std::push_heap(v.begin(), v.end());
	std::cout << "max heap after push: " << v.front() << '\n';

	std::sort_heap(v.begin(), v.end());

	std::cout << "final sorted range :";
	for (unsigned i = 0; i < v.size(); i++)
		std::cout << ' ' << v[i];

	std::cout << '\n';*/


	return 0;
}