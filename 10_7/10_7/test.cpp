#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//类的成员函数


//普通成员函数
//只能由类的实例化对象调用
//*静态成员函数（属于整个类，不属于某个对象）

//inline 内联函数
//实际上inline内联函数是从C语言的宏发展而来的
//当一个函数体短小精悍，且经常被调用，
//为减少函数调用的压栈的花销，使用inline关键字声明，将函数代码直接嵌入到调用的地方


//默认情况下，class中的成员函数作为inline内联函数来处理
class pupil
{
public:
	string name;
	int age;
private:
	char sex;

	inline int max_ab(int a, int b)
	{
		return a > b ? a : b;
	}
public:
	int maxabc(int a,int b,int c)
	{
		int max = max_ab(a, b);
		return max = max_ab(max, c);
	}

	void print_name();
	//void print_name()
	//{
	//	cout << "name=" << name << endl;
	//}
};
//类的声明与成员函数的分离
//成员函数声明放在类的内部，定义放在外部再加上作用域就行

void pupil::print_name()//类的成员函数外部定义+作用域修饰 student::
{
	cout << "name=" << name << endl;
}

//一种更贴近我们平时编程开发的方法：将类的声明放到 .h 头文件中，
//将类的实现放到 .cpp 实现文件中，谁要使用这个类，就 include 包含 .h 类的头文件就可以啦！
#include "PeoInfo.h"


//int main()
//{
//	
//	PeoInfo peo1;
//	peo1.name = "alice";
//	peo1.print_name();
//	return 0;
//}



//**************************************************************************
//this 指针
#include "Student.h"
//int main()
//{
//	Student s1;
//
//
//	//int n_size = sizeof(s1);
//	//cout << "n_size=" << n_size << endl;//成员函数没有占用空间
//	//因为针对某个类的多个对象而言，不同的只是数据成员，
//	//而函数成员来说都是一样的代码，所以没有必要为每个对象都保存一份成员函数的代码。这样可以很大程度上节省空间。
//
//	Student s2;
//	strcpy(s1.name, "zhang_san");
//	strcpy(s2.name, "li_si");
//
//	s1.print_name();
//	s2.print_name();
//
//	//成员函数可以区分是哪个对象调用它——this 指针
//	//this是一个指针，它时时刻刻指向成员函数所作用的对象
//  //this==当前对象的指针
//
//	//什么时候需要手动添加this
//	//当成员函数的形参名与成员变量重名时，使用this指针指向成员变量作为区别——>见类Student的set_age函数
//	
//	s1.set_age(18);
//	//函数的原型在编译器看来就是
//	//void set_age(Student *const this ,int age)
//
//
//	cout << "s1.age=" << s1.age << endl;
//   	return 0;
//}

class A
{
	int i;
public:
	void Hello()    //void Hello(A* this)
	{
		cout << "hello world" << endl;
	}

	void Hello_i() //void Hello_i(A* this)
	{
		cout << "hello " <<i<< endl;
	}
};

int main()
{
	//成员函数的第一个参数是指向对象的指针
	A* p = NULL;
	p->Hello(); //执行Hello()形式相当于：Hello(p);
	
	//此处成员函数的参数为NULL，因为p为NULL。
	//但是函数用不到任何成员变量，即使this=NULL，也可以正常执行
	
	//如果此时调用成员函数就会奔溃了，因为this指针是空的，使用了 空的指针指向了成员变量i，程序就会奔溃。
	p->Hello_i();
}








