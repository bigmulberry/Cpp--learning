#pragma once
#include <iostream>
using namespace std;
#pragma pack(push) 
#pragma pack(1) //强制转换对齐数
class Student
{
public:
	char name[50];
	char sex;
	int age;
	int num;

	void print_name()
	{
		cout << "name=" << this->name << endl;
	}

	void set_age(int age)
	{
		this->age = age;
	}
};
#pragma pack(pop)
