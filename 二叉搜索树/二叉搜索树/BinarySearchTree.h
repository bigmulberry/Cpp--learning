#pragma once

namespace Key //set
{
template<class K>
struct BSTreeNode
{
	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
	K _key;

	BSTreeNode(const K& key)
	{
		_key = key;
		_left = nullptr;
		_right = nullptr;
	}
};

template<class K>
class BSTree
{
public:
	typedef BSTreeNode<K> node;

	//构造
	BSTree() :_root(nullptr) 
	{
	}



	//拷贝构造和赋值重载都要深拷贝
	node* _Copy(node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}
		node* copyNode = new node(root->_key);
		copyNode->_left=_Copy(root->_left);
		copyNode->_right=_Copy(root->_right);
		return copyNode;
	}
	BSTree(const BSTree<K>& t) :_root(nullptr)
	{
		_root=_Copy(t._root);
	}

	//赋值重载
	BSTree<K>& operator=(BSTree<K> t)
	{
		Destroy(_root);
		_root = nullptr;
		swap(_root, t._root);
		return *this;
	}
	 
	//析构
	~BSTree() 
	{
		Destroy(_root);
		_root = nullptr;
	}

	void Destroy(node* root)//消除以root为根的子树
	{
		if (root == nullptr)
			return;
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	//插入结点
	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new node(key);
			return true;
		}

		node* cur = _root;
		node* parent = _root;
		while (cur)
		{
			if (key>cur->_key)//往右走
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)//往左走
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		cur = new node(key);
		if (parent->_key > key)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		return true;
	}

	//插入结点 递归版本
	bool _insertR(const K& key,node*& root)
	{
		if (root == nullptr)
		{
			root = new node(key);
			return true;
		}
		if (key < root->_key)
		{
			
			return _insertR(key, root->_left);
			
		}
		else if (key > root->_key)
		{		
			return _insertR(key, root->_right);
		}
		else
		{
			return false;
		}
	}

	bool insertR(const K& key)
	{
			return _insertR(key, _root);
	}

	void _Inorder(node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_Inorder(root->_left);
		cout << root->_key<<" ";
		_Inorder(root->_right);
	}

	void Inorder()
	{
		if (_root == nullptr)
		{
			cout << "void tree\n";
		}
		else
		{
			_Inorder(_root);
			cout << endl;
		}
	}

	node* Find(const K& key)
	{
		node* cur = _root;
		while (cur)
		{
			if (cur->_key < key)//往右找
			{
				cur = cur->_right;
			}
			else  if (cur->_key > key)
			{
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
			
		}
		return nullptr;
	}

	node* _FindR(const K& key,node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}
		if (root->_key > key)
		{
			return _FindR(key, root->_left);
		}
		else if (root->_key < key)
		{
			return _FindR(key, root->_right);
		}
		else
		{
			return root;
		}
	}

	node* FindR(const K& key)
	{
		return _FindR(key, _root);
	}


	bool Erase(const K& key)
	{
		node* cur = _root;
		node* parent = nullptr;
		while (cur)
		{
			if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				//找到删除点位cur和其父结点parent

				//删除的三种情况
				//有一边为空，那么就用另一边 的孩子直接连上去(parent不为nullptr)
				if (cur->_left == nullptr)//左为空
				{
					if (cur == _root)
					{
						_root = cur->_right;
					}
					else
					{
						if (parent->_left == cur)
						{
							parent->_left = cur->_right;
						}
						else
						{
							parent->_right = cur->_right;
						}
					}


					delete cur;
					cur = nullptr;
				}
				else if (cur->_right == nullptr)//左不为空 右为空
				{
					if (cur == _root)
					{
						_root = cur->_left;
					}
					else
					{
						if (parent->_left == cur)
						{
							parent->_left = cur->_left;
						}
						else
						{
							parent->_right = cur->_left;
						}
					}

					delete cur;
					cur = nullptr;
				}
				//两边都不为空，需要使用替换删除
				else
				{

					////方法一：找到置换节点，直接删除
					//找左子树中的最大结点顶上
					//node* Maxleft = cur->_left;
					//node* MaxParent = cur;
					//while (Maxleft->_right)
					//{
					//	MaxParent = Maxleft;
					//	Maxleft = Maxleft->_right;
					//}//循环结束 Maxleft 找到了左子树的最大点
					//cur->_key = Maxleft->_key;
					////删除掉Maxleft这个位置的结点
					//if (MaxParent->_right == Maxleft)//左子树最大结点为右大娃
					//{
					//	MaxParent->_right = Maxleft->_left;
					//}
					//else//左子树的最大结点为左子树的根
					//{
					//	MaxParent->_left = Maxleft->_left;
					//}
					//delete Maxleft;
					//Maxleft = nullptr;


					//调用递归删除
					node* Maxleft = cur->_left;
					while (Maxleft->_right)
					{
						Maxleft = Maxleft->_right;
					}//循环结束 Maxleft 找到了左子树的最大点

					K tmp = Maxleft->_key;
					Erase(Maxleft->_key);
					cur->_key = tmp;
				}
				return true;
			}
		}
		return false;
	}


	bool _EraseR(const K& key, node*& root)
	{
		if (root == nullptr)
		{
			return false;
		}
		if (root->_key == key)
		{
			if (root->_right == nullptr)
			{
				node* del = root;
				root = root->_left;
				delete del;
			}
			else if (root->_left == nullptr)
			{

				node* del = root;
				root = root->_right;
				delete del;
			}
			else
			{
				//迭代删除替换节点
				/*node* maxParent = root;
				node* maxleft = maxParent->_left;
				
				while (maxleft->_right)
				{
					maxParent = maxleft;
					maxleft = maxleft->_right;
				}
				K tmp = maxleft->_key;
				root->_key = tmp;
				if (maxParent->_right == maxleft)
				{
					maxParent->_right = maxleft->_left;
				}
				else
				{
					maxParent->_left = maxleft->_left;
				}
				delete maxleft;*/



				//递归删除
				node* maxleft = root->_left;

				while (maxleft->_right)
				{
					maxleft = maxleft->_right;
				}
				K tmp = maxleft->_key;
				_EraseR(tmp, root->_left);
				root->_key = tmp;

			}
			return true;
		}
		else if (root->_key > key)
		{
			return _EraseR(key, root->_left);
		} 
		else
		{
			return _EraseR(key, root->_right);
		}
	}

	bool EraseR(const K& key)
	{
		return _EraseR(key, _root);
	}

private:
	node* _root;
};

}
  

namespace KV //map
{
	template<class K,class V>
	struct BSTreeNode
	{
		BSTreeNode<K,V>* _left;
		BSTreeNode<K,V>* _right;
		K _key;
		V _value;
		BSTreeNode(const K& key,const V& value)
		{
			_key = key;
			_value = value;
			_left = nullptr;
			_right = nullptr;
		}
	};

	template<class K,class V>
	class BSTree
	{
	public:
		typedef BSTreeNode<K,V> node;

		//构造
		BSTree() :_root(nullptr)
		{
		}

		//拷贝构造和赋值重载都要深拷贝

		BSTree(const BSTree<K,V>& t) :_root(nullptr)
		{
			_root = _Copy(t._root);
		}

		//赋值重载
		BSTree<K,V>& operator=(BSTree<K,V> t)
		{
			Destroy(_root);
			_root = nullptr;
			swap(_root, t._root);
			return *this;
		}

		//析构
		~BSTree()
		{
			Destroy(_root);
			_root = nullptr;
		}

		bool insertR(const K& key,const V& value)
		{
			return _insertR(key,value, _root);
		}

		void Inorder()
		{
			if (_root == nullptr)
			{
				cout << "void tree\n";
			}
			else
			{
				_Inorder(_root);
				cout << endl;
			}
		}


		node* FindR(const K& key)
		{
			return _FindR(key, _root);
		}


		bool EraseR(const K& key)
		{
			return _EraseR(key, _root);
		}

	private:

		node* _Copy(node* root)
		{
			if (root == nullptr)
			{
				return nullptr;
			}
			node* copyNode = new node(root->_key, root->_value);
			copyNode->_left = _Copy(root->_left);
			copyNode->_right = _Copy(root->_right);
			return copyNode;
		}

		//插入结点 递归版本
		bool _insertR(const K& key, const V& value, node*& root)
		{
			if (root == nullptr)
			{
				root = new node(key, value);
				return true;
			}
			if (key < root->_key)
			{

				return _insertR(key, value, root->_left);

			}
			else if (key > root->_key)
			{
				return _insertR(key, value, root->_right);
			}
			else
			{
				return false;
			}
		}

		bool _EraseR(const K& key, node*& root)
		{
			if (root == nullptr)
			{
				return false;
			}
			if (root->_key == key)
			{
				if (root->_right == nullptr)
				{
					node* del = root;
					root = root->_left;
					delete del;
				}
				else if (root->_left == nullptr)
				{

					node* del = root;
					root = root->_right;
					delete del;
				}
				else
				{
					//递归删除
					node* maxleft = root->_left;

					while (maxleft->_right)
					{
						maxleft = maxleft->_right;
					}
					K ktmp = maxleft->_key;
					V vtmp = maxleft->_value;
					_EraseR(ktmp, root->_left);
					root->_key = ktmp;
					root->_value = vtmp;
				}
				return true;
			}
			else if (root->_key > key)
			{
				return _EraseR(key, root->_left);
			}
			else
			{
				return _EraseR(key, root->_right);
			}
		}

		node* _FindR(const K& key, node* root)
		{
			if (root == nullptr)
			{
				return nullptr;
			}
			if (root->_key > key)
			{
				return _FindR(key, root->_left);
			}
			else if (root->_key < key)
			{
				return _FindR(key, root->_right);
			}
			else
			{
				return root;
			}
		}

		void _Inorder(node* root)
		{
			if (root == nullptr)
			{
				return;
			}
			_Inorder(root->_left);
			cout << root->_key << "-" << root->_value << endl;
			_Inorder(root->_right);
		}

		void Destroy(node* root)//消除以root为根的子树
		{
			if (root == nullptr)
				return;
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
		}

		node* _root;
	};

}