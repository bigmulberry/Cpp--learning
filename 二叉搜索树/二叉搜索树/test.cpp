#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

#include "BinarySearchTree.h"


void Test1()
{
	Key::BSTree<int> b1;
	int a[] = { 5,3,7,1,4,6,8,0,2,9 };
	for (auto e : a)
	{
		b1.insert(e);
	}
	b1.Inorder();

	for (auto e : a)
	{
		b1.Erase(e);
	}
	b1.Inorder();

	for (auto e : a)
	{
		b1.insertR(e);
	}
	b1.Inorder();

	for (auto e : a)
	{
		b1.EraseR(e);
	}
	b1.Inorder();
}

void Test2()
{
	Key::BSTree<int> b1;
	int a[] = { 5,3,7,1,4,6,8,0,2,9 };
	for (auto e : a)
	{
		b1.insert(e);
	}
	Key::BSTree<int> b2=b1;
	b2.Inorder();
	Key::BSTree<int> b3;
	b3 = b2;
	b3.Inorder();

}

void Test3()
{
	KV::BSTree<int,char> b1;
	int a[] = { 5,3,7,1,4,6,8,0,2,9 };
	char b[] = { 'f','d','h','b','e','g','i','a','c','j'};
	for (int i=0;i<10;++i)
	{
		b1.insertR(a[i],b[i]);
	}

	b1.Inorder();

}

void Test4()
{
	KV::BSTree<string, int> cnt;
	string fruitbox[] = { "apple","apple","orange" ,"apple" ,"banana","banana" ,"orange" ,"watermelon"};
	for (const auto& str : fruitbox)
	{
		//找水果
		KV::BSTreeNode<string,int>* fruit=cnt.FindR(str);
		//找不到插入<str,1>
		if (fruit == nullptr)
		{
			cnt.insertR(str, 1);
		}
		else
		{
			fruit->_value++;
		}
	}
	cnt.Inorder();
}


//class A
//{
//public:
//	void foo()
//	{
//		cout << "A::foo" << endl;;
//	}
//	virtual void bar()
//	{
//		cout << "A::bar" << endl;
//	}
//};
//
//class B:public A
//{
//public:
//	void foo()
//	{
//		cout << "B::foo" << endl;;
//	}
//	virtual void bar()
//	{
//		cout << "B::bar" << endl;
//	}
//};

//int main()
//{
//	//Test3();
//	//Test4();
//	A a;
//	B b;
//	A* pa = (A*)&b;
//	pa->bar();
//	pa->foo();
//
//	return 0;
//}


#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	if (n == 1 || n == 2)
	{
		cout << -1 << endl;
		return 0;
	}
	if (n % 2 == 1)
	{
		cout << 2 << endl;
		return 0;
	}
	vector<vector<int>> arr(n);
	for (int i = 0; i < n; ++i)
	{
		arr[i].resize(2 * i + 1, 0);
		arr[i][0] = 1;
		arr[i][2 * i] = 1;
		if (i >= 1)
		{
			arr[i][1] = i;
			arr[i][2 * i - 1] = i;
		}
	}
	for (int i = 2; i < n; i++)
	{

		for (int j = 2; j < 2 * i - 1; ++j)
		{

			arr[i][j] = arr[i - 1][j - 2] + arr[i - 1][j - 1] + arr[i - 1][j];
			//if (i == n - 1 && arr[i][j] % 2 == 0)
			//{
			//	cout << j + 1 << endl;
			//	return 0;
			//}
			
		}
		
	}


	for (int i = 0; i < n; i++)
	{
		cout << "n:" << i + 1<<"  ";
		for (int j = 0; j < 2 * i + 1; ++j)
		{
			cout << arr[i][j] << ' ';
		}
		cout << endl;
	}

	cout << -1 << endl;


	return 0;
}


