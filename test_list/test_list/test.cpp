#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

//****************************************************************************************
//基本使用
//****************************************************************************************

//void test_list1()
//{
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//
//	//遍历方式1 迭代器
//	list<int>::iterator it = lt.begin();
//	while (it != lt.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	//遍历方式1 范围for
//	for (auto e : lt)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	//构造
//	list<int> lt2(4, 100);
//	list<int> lt3(lt.begin(),lt.end());
//	//利用数组和迭代器构造链表
//	int array[] = { 16,2,32,12 };
//	list<int> lt4(array,array+sizeof(array)/sizeof(int));//数组的原生指针可以都当作天然的迭代器使用 
//
//
//	//升序 < 默认
//	//降序 >
//	vector<int> v(array, array + 4);
//	sort(v.begin(),v.end()-1);//默认升序 
//	sort(v.begin(),v.end(),greater<int>());//降序
//	//sort底层使用快速排序，要求容器迭代器必须是 随机迭代器
//	//比如：快排要三数取中优化，不支持随机访问，效率就不够了
//	//sort(lt.begin(),lt.end());//不支持,因为<algorithm>的sort支持随机迭代器，list的双向迭代器无法胜任
//	lt4.sort();//使用list的排序库函数，但不常用，效率太低
//	lt4.sort(greater<int>());
//
//	//常见的容器迭代器分为三种：
//	//单向 ++            ：单链表forward_list
//	//双向 ++/--         :list
//	//随机 ++/--/+/-     :vector/string
//	//
//
//}
//
//template <class T>
//void PrintContainer(const T& t)
//{
//	typename T::const_iterator it = t.begin();
//	for (const auto& e : t)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//
//void test_list2()
//{
//	//list<int> lt1;
//	//cout << lt1.max_size() << endl;//4G大小，每个单元存放12字节
//	 
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//	PrintContainer(lt);
//
//	//assign(),赋予新值，原来的值就抛弃了
//	//1.assign(size_t n,value_type val);
//	//2.assign(前后迭代器)
//	list<int> lt2(5,100);
//	PrintContainer(lt2);
//	lt2.assign(lt.begin(), lt.end());
//	PrintContainer(lt2);
//
//	//insert
//	//1.传入pos，val ，返回指向第一个新插入元素的迭代器。
//	//2.传入pos，n个，val，无返回值
//	//3.传入pos，头尾迭代器，无返回值
//	list<int>::iterator pos = find(lt.begin(), lt.end(), 3);
//	lt.insert(pos, 30);
//	PrintContainer(lt);
//	//vector使用insert/erase会导致迭代器失效
//	//list进行insert后，迭代器不会产生1.意义变了/2.野指针的情况，所以不会失效
//	cout << *pos << endl;
//	//erase
//	//1.传入pos位置，删掉pos位置，返回指向后一个元素的迭代器
//	//2.传入区间，删掉区间，返回指向区间后一个元素的迭代器
//	lt.erase(pos);
//	PrintContainer(lt);
//	cout << *pos << endl;
//	//list进行erase后，迭代器原先指向的位置已被删除，成为野指针，再次访问必然崩溃。所以list的erase会导致迭代器失效
//
//}
//
//void test_list3()
//{
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//
//	//splice 接合  将内容转移，被转移的容器将会清除掉被转移的元素
//	auto pos = find(lt.begin(), lt.end(), 3);
//	list<int> lt2;
//	lt2.splice(lt2.begin(), lt,lt.begin(), pos);
//	PrintContainer(lt2);
//	PrintContainer(lt);
//
//	//remove
//	lt2.remove(6);//不存在元素 也无所谓
//	PrintContainer(lt2);
//	lt2.remove(2);
//	PrintContainer(lt2);
//
//}
//
//
//void test_list4()
//{
//	//unique 去重连续的元素 常常和sort连用 ，可以把连续的元素放在一起
//	//但是一般不建议使用链表的sort，效率不高 
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(2);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(2);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.unique();
//	PrintContainer(lt);
//	lt.reverse();
//	PrintContainer(lt);
//	//list拥有自己的swap ，效率更高（交换指针）
//
//}


//int main()
//{
//	//test_list1();
//	//test_list2();
//	//test_list3();
//	test_list4();
//	return 0;
//}

//*********************************************************************************************************
// 自己实现list
//*********************************************************************************************************
#include <assert.h>
#include "list.h"

//测试函数
template <class T>
void Print(const sjl::list<T>& lt)
{
	typename sjl::list<T>::const_iterator it = lt.begin();
	while (it != lt.end())
	{
		//*it += 1;//提供的list为const，但还是可以修改
		cout <<*it << " ";
		it++;
	}
	cout << endl;
}
void test1_mylist()
{
	sjl::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	Print(lt);

	sjl::list<string> lt2;
	lt2.push_back("hello");
	lt2.push_back("world");

	Print(lt2);
}



int main()
{

	//test1_mylist();
	//sjl::test2_mylist();
	sjl::test3_mylist();

	return 0;
}



































