#pragma once

namespace sjl
{
	//单结点*****************************************************************
	template <class T>
	class _list_node
	{
	public:
		T _val;
		_list_node<T>* _prev;
		_list_node<T>* _next;

		_list_node(const T& t=T()):_val(t),_next(nullptr),_prev(nullptr)
		{
			 
		}
 	};

	//迭代器类***************************************************************
	//原生的指针不能满足迭代器的定义
	//通过类封装原生指针，重载运算符满足
	//为防止产生冗余代码，在template的T后新设Ref和Ptr，分别表示T的引用和指针 
	template <class T,class Ref,class Ptr>
	class _list_iterator
	{
		
	public:
		typedef _list_node<T> node;
		typedef _list_iterator<T,Ref,Ptr> self;//自我类型定义为self 方便使用

		node* _pnode;

		//构造函数
		_list_iterator(node* pnode):_pnode(pnode)
		{}

		//拷贝构造，赋值重载，析构 我们不写，编译器默认生成的就可以使用

		//解引用重载
		//T& operator*();
		//const T& operator*();
		Ref operator*()
		{
			return _pnode->_val;
		}

		//->重载
		Ptr operator->()
		{
			return &_pnode->_val;
		}
		//!=重载
		bool operator!=(const self& s)const
		{
			return _pnode != s._pnode;
		}

		//==重载
		bool operator==(const self& s)const
		{
			return _pnode == s._pnode;
		}

		//前置++ 重载
		self& operator++()
		{
			_pnode = _pnode->_next;
			return *this;
		}

		//后置++ 重载
		self operator++(int)
		{
			self tmp = *this;
			_pnode = _pnode->_next;
			return tmp;
		}

		//前置-- 重载
		self& operator--()
		{
			_pnode = _pnode->_prev;
			return *this;
		}
		
		//后置-- 重载
		self operator--(int)
		{
			self tmp = *this;
			_pnode = _pnode->_prev;
			return tmp;
		}

		self operator-(int n)
		{
			node* tmp = _pnode;
			while(n>0)
			{
				tmp=tmp->_prev;
				n--;
			}
			return tmp;
		}

	};

	//因为解引用的重载函数只能有一个
	//为了返回const类型，需拥有const_iterator 一般会重新设计一个类
	//但是新创建一个类会产生大量冗余的代码
	//template <class T>
	//class _list_const_iterator
	//{

	//public:
	//	typedef _list_node<T> node;
	//	typedef _list_const_iterator<T> self;//自我类型定义为self 方便使用
	//	node* _pnode;
	//	//构造函数
	//	_list_const_iterator<T>(node* pnode) : _pnode(pnode)
	//	{}

	//	//拷贝构造，赋值重载，析构 我们不写，编译器默认生成的就可以使用

	//	//解引用重载
	//	const T& operator*()const
	//	{
	//		return _pnode->_val;
	//	}

	//	//!=重载
	//	bool operator!=(const self& s)const
	//	{
	//		return _pnode != s._pnode;
	//	}

	//	//==重载
	//	bool operator==(const self& s)const
	//	{
	//		return _pnode == s._pnode;
	//	}

	//	//前置++ 重载
	//	self& operator++()
	//	{
	//		_pnode = _pnode->_next;
	//		return *this;
	//	}

	//	//后置++ 重载
	//	self operator++(int)
	//	{
	//		self tmp = *this;
	//		_pnode = _pnode->_next;
	//		return tmp;
	//	}

	//	//前置-- 重载
	//	self& operator--()
	//	{
	//		_pnode = _pnode->_prev;
	//		return *this;
	//	}

	//	//后置-- 重载
	//	self operator--(int)
	//	{
	//		self tmp = *this;
	//		_pnode = _pnode->_prev;
	//		return tmp;
	//	}
	//};


	//list 类******************************************************************************************************
	template <class T>
	class list
	{
	public:
		typedef _list_node<T> node;
		typedef _list_iterator<T,T&,T*> iterator;
		typedef _list_iterator<T,const T&,const T*> const_iterator;
		//构造函数
		list()
		{
			_head = new node();
			_head->_next = _head;
			_head->_prev = _head;
		}

		//拷贝构造
		list(const list<T>& t)
		{
			//建立头结点
			_head = new node();
			_head->_prev = _head;
			_head->_next = _head;

			//深拷贝
			for (const auto& e : t)
			{
				push_back(e);
			}
		}

		//赋值重载
		//传统学法
		//list<T>& operator=(const list<T>& t)
		//{
		//	//防止自己给自己赋值
		//	if (this != &t)
		//	{
		//		clear();
		//		for (const auto& e : t)
		//		{
		//			push_back(e);
		//		}
		//	}
		//	return *this;
		//}

		//现代写法
		list<T>& operator=(list<T> t)
		{
			//clear();

			swap(_head, t._head);//t是拷贝过来的临时变量，再和_head交换后，会在函数时自动销毁，无需提前clear
			return *this;
		}


		//析构函数
		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}
		iterator begin()
		{     
			return iterator(_head->_next);//返回iterator类的匿名对象，结构为链表，所以需要迭代器的+-操作，而普通指针的加减是线性的
		}

		const_iterator begin()const
		{
			return const_iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}

		const_iterator end()const
		{
			return const_iterator(_head);
		}

		void insert(iterator pos, const T& x)
		{
			assert(pos._pnode);
			node* newnode = new node(x);
			node* cur = pos._pnode;
			node* pre = cur->_prev;
			pre->_next = newnode;
			newnode->_prev = pre;
			newnode->_next = cur;
			cur->_prev = newnode;
		}

		iterator erase(iterator pos)
		{
			assert(pos._pnode);
			assert(pos != end());
			node* pre_pos = pos._pnode->_prev;
			node* post_pos = pos._pnode->_next;

			pre_pos->_next = post_pos;
			post_pos->_prev = pre_pos;

			delete pos._pnode;
			return iterator(post_pos);
		}

		void push_back(const T& x)
		{
			//node* newnode = new node(x);
			//node* Tail = _head->_prev;
			//Tail->_next = newnode;
			//newnode->_prev = Tail;
			//newnode->_next = _head;
			//_head->_prev = newnode;
			insert(end(), x);
		}

		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		bool empty()  
		{
			return begin() == end();
		}

		void clear()
		{
			iterator it = begin();
			while (it!=end())
			{
				it=erase(it);
			}
		}

	private:
		node* _head;
	};

	class Date
	{
	public:
		int _year = 0;
		int _month = 1;
		int _day = 0;
	};


	//->的重载
	void test2_mylist()
	{
		list<Date> lt;
		lt.push_back(Date());
		lt.push_back(Date());
		lt.push_back(Date());
		lt.push_back(Date());

		list<Date>::iterator it = lt.begin();
		while (it != lt.end())
		{
			cout << it.operator->()->_year <<" " << it->_month << " " << it->_day <<endl;
			//->的重载返回的是T*/Date*，然后再接一个->进行访问对象的成员，但是这里编译器做了优化，省略了一个箭头
			++it;
			
		}
	}

	template <class T>
	void PrintList(const list<T>& lt)
	{
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test3_mylist()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		list<int>::iterator pos= lt.begin();
		lt.insert(pos, 10);
		lt.insert(lt.erase(pos),4);
		PrintList(lt);

		list<int>copy(lt);
		PrintList(copy);

		list<int> lt2;
		lt2.push_back(10);
		lt2.push_back(20);
		copy = lt2;
		PrintList(copy);


		lt.clear();
	}
}  
