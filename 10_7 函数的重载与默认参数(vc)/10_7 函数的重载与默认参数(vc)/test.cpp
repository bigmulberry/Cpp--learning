#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

 //C++允许同一函数名定义多个函数，这些函数的参数类型和个数可以不相同，
//而且至少要有一个不相同，如果都相同的话就会报重复定义的链接错误了。使一个函数名可以多用。

//函数重载的要求：——同名不同参，与返回类型无关
//重载函数的参数个数、参数类型、参数顺序 三者中必须至少有一种不同（不然会产生调用疑惑）。
//函数的返回值类型可以相同也可以不同。
//单纯返回值类型不同的两个同名函数不能构成函数重载，会报链接错误。


int max_num(int a, int b)
{
	return a > b ? a:b;
}

//float max_num(int a, int b)
//{
//	return a > b ? a : b;
//}

//函数的重载一般都是功能相近，或者功能类似的函数进行重载，
//不能把一些功能相差很大，或者完全不相关的函数叫同一个名字，语法上是没有错误，但是违背函数重载设计的初衷




//函数的默认参数

//举例，找出数组中的最大最小值，封装成函数
//void get_min_max(int src[], int arr_len, int* max_v, int* min_v)
//{
//	if (arr_len < 0 || !max_v || !min_v) return;
//
//	*min_v = src[0];
//	*max_v = src[0];
//	for (int i = 1; i < arr_len; ++i)
//	{
//		if (*min_v > src[i]) *min_v = src[i];
//		if (*max_v < src[i]) *max_v = src[i];
//	}
//}
//
//int main()
//{
//	int src[] = { 66, 33, 55, 22, 88, 89, 19 };
//	int a, b;
//	get_min_max(src, sizeof(src) / sizeof(int), &a, &b);
//	cout << "最大数 = " << a << ", 最小数 = " << b << endl;
//
//	return 0;
//}

//想让这个函数更灵活一些，有的时候想求最大值，有的时候想求最小值，有的时候同时求最大值和最小值。
void get_min_max(int src[], int arr_len, int* max_v=NULL, int* min_v=NULL)
{
	if (arr_len < 0 || (!max_v && !min_v)) return;

	int min_num = src[0];
	int max_num = src[0];
	for (int i = 1; i < arr_len; ++i)
	{
		if (min_num > src[i]) min_num = src[i];
		if (max_num < src[i]) max_num = src[i];
	}
	if (max_v)
		*max_v = max_num;
	if (min_v)
		*min_v = min_num;

}

//函数的默认参数对函数重载所造成的歧义：
int get_min_max(int src[], int arr_len)
{
	return src[0];
}

int main()
{
	int src[] = { 66, 33, 55, 22, 88, 89, 19 };
	int a, b;
	get_min_max(src, sizeof(src) / sizeof(int), &a, &b);
	cout << "最大数 = " << a << ", 最小数 = " << b << endl;

	int max_v=0;
	get_min_max(src, sizeof(src) / sizeof(int), &max_v);
	cout << "max_v=" << max_v << endl;

	int min_v = 0;
	get_min_max(src, sizeof(src) / sizeof(int),NULL ,&min_v);
	cout << "min_v=" << min_v << endl;

	//与函数重载造成的歧义
	//get_min_max(src, sizeof(src) / sizeof(int));//默认参数全为NULL的情况产生了冲突

	return 0;
}

//函数默认参数的注意事项：
//①、在函数声明的时候指定，如果没有函数的声明则可以放在函数的定义中，但是声明和定义只能选一个；
//②、从第一个有默认值的参数开始，后面的所有参数必须都有默认值才行；(带默认值的参数都放在函数参数列表尾部)
//③、调用的时候如果要自定义非第一个默认值的参数，那么前面所有的参数都要明确的写上默认值才行；（min_v之前的max_v要指定为NULL）
//④、从使用角度来说函数的默认值比重载更方便，从函数内部实现角度来说比函数的重载更复杂。



