#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include "RBTree.h"
#include <cstdlib>
void testRBTree()
{
	RBTree< int, int > t;
	int arr[] = { 21,18,32,4,26,9,17,12,30,29 };
	//int n = 10000;
	srand(time(nullptr));
	cout << "-------------插入结点------------" << endl;
	for (int i=0;i<10;++i)
	{
		int e = arr[i];
		t.Insert(make_pair(e, e*100));
	}
	t.InOrder();
	cout << "是否为红黑树：" << t.isBalance() << endl;
	cout << "-------------删除结点------------" << endl;
	for (auto e : arr)
	{
		t.Erase(e);
		cout << "是否为红黑树：" << t.isBalance() << endl;
	}

}


int main()
{
	testRBTree();
	return 0;
}